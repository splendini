
%{
#include <stdio.h>
#include "uex.h"
#include "uex_language.h"

#define YYSTYPE ue_p

#include "uex_parser.tab.h"

/*
http://stackoverflow.com/questions/9656786/flex-newline-scanning-for-bison
http://stackoverflow.com/questions/20458469/flex-bison-eof-propagation-from-stdin-vs-a-file
*/

int yylex();

extern int lineNumber;
extern bool isEOF;
bool endParsingOnNewLine = true;
int errorLineCol = 0;

%}

/*
An interactive scanner is one that only looks ahead if it must.
Looking one extra character ahead, is a bit faster than only looking
ahead when necessary. Scanners that always look ahead give dreadful
interactive performance; for example, a newline, is not recognized
until another newline is entered

avoid: implicit declaration of function ‘fileno’
fileno conforms to POSIX
*/
%option always-interactive

/* to avoid warning: ‘yyunput’ defined but not used */
%option nounput
/* to avoid warning: ‘input’ defined but not used */
%option noinput

/* Suppress the default implementations. */
%option noyyalloc noyyrealloc noyyfree

punct [!"#%&'()*+,-./:;<=>?@[\]^_`{|}~]
ws [ \t]

%%
{ws}+						{ 	errorLineCol += strlen(yytext); }

\n  		               	{	errorLineCol = 0;
								if (endParsingOnNewLine) {
									/* this fakes an EOF */
									return 0;
								} else { /* eat \n */
									lineNumber += 1;
								}
							 }

\%.*						/* comment */

("-"?[0-9])[0-9]*          	{	errorLineCol += strlen(yytext);
								yylval = ue_StringToInteger(yytext);
								return TINTEGER; }

("."[0-9]+)|("-"?[0-9]+"."[0-9]*)	{
								/* the rule only works when the local
									is such that the decimal point is
									a deciaml point and not a comma */
								errorLineCol += strlen(yytext);
								yylval = ue_StringToReal(yytext);
								return TREAL; }

\"([^\\\"\n]|(\\.))*\"    	{	errorLineCol += strlen(yytext);
								/* remove trailing " */
								yytext[yyleng-1] = '\0';
								/* +1 remove initial " */
								yylval = ue_mk_String(yytext+1);
								return TSTRING; }

\==						 	{	errorLineCol += 2;
								return TEQUAL; }

\!=						 	{	errorLineCol += 2;
								return TNEQUAL; }

\>=						 	{	errorLineCol += 2;
								return TGEQ; }

\<=						 	{	errorLineCol += 2;
								return TLEQ; }

\&&						 	{	errorLineCol += 2;
								return TAND; }

[||]					 	{	errorLineCol += 2;
								return TOR; }

\===					 	{	errorLineCol += 3;
								return TISSAME; }

\=!=					 	{	errorLineCol += 3;
								return TISNSAME; }

{punct}						{	errorLineCol += 1;
								return yytext[0]; }

[a-zA-Z$]([a-zA-Z$]|[0-9])*    	{
								/* should allow more characters */
								errorLineCol += strlen(yytext);
								yylval = ue_mk_Symbol(yytext);
								return TSYMBOL; }

"\\pi"|"\\e"|"\\-inf"|"\\inf"|"\\nan"				    	{
								/* should allow more characters */
								errorLineCol += strlen(yytext);
								yylval = ue_mk_Symbol(yytext);
								return TSYMBOL; }

.							{	errorLineCol++;
								printf("Look at line number %i column %i. No expression can start with: \"%s\".\n", lineNumber, errorLineCol, yytext);
								return TJUNK;   }

%%

int yywrap() {
	isEOF = true;
	return 1;
}

/* Provide our own implementations. */
void * yyalloc (size_t bytes) {
	return ue_Malloc(bytes);
}
     
void * yyrealloc (void * ptr, size_t bytes) {
	return ue_Realloc(ptr, bytes);
}
     
void yyfree (void * ptr) {      
	ue_Free(ptr);
}
     

