
#include "uex.h"
#include "uex_language.h"
#include "environment.h"
#include "evaluator.h"
#include "special_forms.h"

ue_p sState;

ue_p ue_Eval(const ue_p u, ue_p env) {

	bool qf, qr, qet, listable;
	size_t length, arity, i, j;
	ue_p uex, tmp, uitemp;
	ue_p ueval, ui, type;
	ue_p param, args;
	ue_p fparameters, fenv, frameName;
	ue_p p, a;

	uex = u;

tailcall:

	assert(ue_is_Environment(env));

	switch( ue_get_type(uex) ) {
		case STRING:
		case NUMBER:
		case TENSOR:
		case FUNCTION:
		case DATA:
			return uex;

		case SYMBOL: {
			tmp = lookupVariableValue(uex, env);
			return tmp;
		}

		case LUMP: {
			type = ue_get_LumpType(uex);


			/* in a tailcall we need to reset qf and qr
			*  such that attributes are not propagated */
			qf = false;
			qr = false;
			listable = false;

			/* attributes are attached to symbols */
			if (ue_is_Symbol(type)) {
				qf = (bool) ue_has_SymbolAttribute(type, QUOTEFIRST);
				qr = (bool) ue_has_SymbolAttribute(type, QUOTEREST);
				listable = (bool) ue_has_SymbolAttribute(type, LISTABLE);
			}

			type = ue_Eval(type, env);

			if (ue_is_Function(type)) {
				qf = (bool) ue_has_FunctionAttribute(type, QUOTEFIRST);
				qr = (bool) ue_has_FunctionAttribute(type, QUOTEREST);
				listable = (bool) ue_has_FunctionAttribute(type, LISTABLE);
			}

			arity = ue_get_LumpLength(uex);
			ueval = ue_mk_LumpShell(type, arity);

			for (i = 1; i <= arity; i++) {
				ui = ue_get_LumpElem(uex, i);
				qet = (ue_get_Type(ui) == sUnquote);
				if (((i != 1 || !qf) && (i == 1 || !qr)) || qet) {
					ui = ue_Eval(ui, env);
				}
				ue_set_LumpElem(ueval, i, ui);
			}


			/**/
			/* Listable */
			/**/
			if (listable && (arity > 0)) {
				length = 0;
				for (i = 1; i <= arity; i++) {
					tmp = ue_get_LumpElem(ueval, i);
					if (ue_is_List(tmp)) {
						size_t len = ue_get_LumpLength(tmp);
						if (length == 0) length = len;
						if (length != len) {
							/* error - could warn, return sFailed; */
							listable = false; 
						}	
					}
				}
				if (length == 0) listable = false;
			}

			if (listable && (arity > 0)) {
				tmp = ue_mk_LumpShell(sList, length);
				for (i = 1; i <= length; i++ ) {
					/* type may have lost it's attribute after eval */
					ui = ue_mk_LumpShell(ue_get_LumpType(uex), arity);
					for (j = 1; j <= arity; j++) {
						uitemp = ue_get_LumpElem(ueval, j);
						if (ue_is_List(uitemp)) {
							uitemp = ue_get_LumpElem(uitemp, i);
						}
						ue_set_LumpElem(ui, j, uitemp);
					}
					/* build up the uex and then do one tailcall;
					 * an alternative is to 
					 * ui = ue_Eval(ui, env); 
					 * and after the loop return tmp; */
					ue_set_LumpElem(tmp, i, ui);
				}
				uex = tmp;
				goto tailcall;
			}


			/**/
			/* apply */
			/**/

			/* apply primitives */
			if (ue_is_SystemFunction(type)) {
				ue_p state = ue_mk_State(ueval, env);
				tmp = ue_get_SystemFunctionFP(type)(state);
				if (ue_is_State(tmp)) {
					uex = ue_get_State_ue(tmp);
					env = ue_get_State_env(tmp);
					goto tailcall;
				} else {
					return tmp;
				}
			}


			/* apply function */
			if (ue_is_PureFunction(type)) {
				assert(arity == ue_get_LumpLength(ueval));	
				fparameters = getPureFunctionParameters(type);

				if (ue_get_Type(fparameters) == sVariadic) {
						arity = 1;
					ueval = ue_mk_Lump(sList, 1, ueval);
				} else if (arity != ue_length(fparameters)) {
						return u;
				}

				uex = getPureFunctionBody(type);
				args = sEmptyList;
				param = sEmptyList;
				for (i = 1; i <= arity; i++) {
					p = ue_get_LumpElem(fparameters, i);
					assert(ue_is_Symbol(p));
					param = cons( p, param);
					a = ue_get_LumpElem(ueval, i);
					args = cons( a, args);
				}

				fenv = ue_get_PureFunctionEnv(type);
				if (fenv == sDynamicScope) {
					fenv = env;
				}

				frameName = getPureFunctionFrameName(type);
				env = extendEnvironment(frameName, param, args, fenv);

				goto tailcall;
			}

			return ueval;
		}

		default:
			printf("\nue_Eval: can not evaluate this.\n");
            return sFailed;

	}
}

void ue_init_evaluator() {
	sState 			= ue_mk_Symbol("State");
}

