#include <time.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>


/*ue lib*/
#include "symboltable.h"
#include "uex.h"
#include "uex_math.h"
#include "utils.h"

/*ue lang*/
#include "environment.h"
#include "evaluator.h"
#include "optimizations.h"
#include "part.h"
#include "special_forms.h"
#include "uex_language.h"

/* should all of these be _WIN32 */
#ifdef WIN32
	#include <windows.h>
#else
	#include <sys/time.h>
#endif

ue_p sDynamicScope;
ue_p sEndOfFile;
ue_p sFalse;
ue_p sListable;
ue_p sNone;
ue_p sQuoteAll;
ue_p sQuoteFirst;
ue_p sQuoteRest;
ue_p sQuoteAllCoercion;
ue_p sQuoteFirstCoercion;
ue_p sQuoteRestCoercion;
ue_p sStaticScope;
ue_p sTrue;
ue_p sVariadic;


#define initSymbolAndSystemFunction(name) ue_p s##name, sf##name

/* symbols and related system functions */
initSymbolAndSystemFunction( And);
initSymbolAndSystemFunction( Apply);
initSymbolAndSystemFunction( Attributes);
initSymbolAndSystemFunction( AttributesAdd);
initSymbolAndSystemFunction( AttributesRemove);
initSymbolAndSystemFunction( Block);
initSymbolAndSystemFunction( ByteCount);
initSymbolAndSystemFunction( Clear);
initSymbolAndSystemFunction( Close);
initSymbolAndSystemFunction( Coerce);
initSymbolAndSystemFunction( Compare);
initSymbolAndSystemFunction( Context);
initSymbolAndSystemFunction( ConstantArray);
initSymbolAndSystemFunction( Environment);
initSymbolAndSystemFunction( Equal);
initSymbolAndSystemFunction( Evaluate);
initSymbolAndSystemFunction( Exit);
initSymbolAndSystemFunction( Function);
initSymbolAndSystemFunction( Greater);
initSymbolAndSystemFunction( GreaterEqual);
initSymbolAndSystemFunction( Hash);
initSymbolAndSystemFunction( If);
initSymbolAndSystemFunction( IsEven);
initSymbolAndSystemFunction( IsInterpretedFunction);
initSymbolAndSystemFunction( IsList);
initSymbolAndSystemFunction( IsLump);
initSymbolAndSystemFunction( IsNotSame);
initSymbolAndSystemFunction( IsNumber);
initSymbolAndSystemFunction( IsNumberExact);
initSymbolAndSystemFunction( IsNumberInexact);
initSymbolAndSystemFunction( IsOdd);
initSymbolAndSystemFunction( IsSame);
initSymbolAndSystemFunction( IsStream);
initSymbolAndSystemFunction( IsString);
initSymbolAndSystemFunction( IsSymbol);
initSymbolAndSystemFunction( IsTensor);
initSymbolAndSystemFunction( IsTensorExact);
initSymbolAndSystemFunction( IsTensorInexact);
initSymbolAndSystemFunction( Join);
initSymbolAndSystemFunction( Length);
initSymbolAndSystemFunction( Less);
initSymbolAndSystemFunction( LessEqual);
initSymbolAndSystemFunction( Let);
initSymbolAndSystemFunction( LibraryFunction);
initSymbolAndSystemFunction( Load);
initSymbolAndSystemFunction( LoadLibraryFunction);
initSymbolAndSystemFunction( LookupData);
initSymbolAndSystemFunction( LookupDataAdd);
initSymbolAndSystemFunction( LookupDataDefaultValue);
initSymbolAndSystemFunction( LookupDataFind);
initSymbolAndSystemFunction( LookupDataKeyValues);
initSymbolAndSystemFunction( Loop);
initSymbolAndSystemFunction( Map);
initSymbolAndSystemFunction( MemoryInUse);
initSymbolAndSystemFunction( Not);
initSymbolAndSystemFunction( NotEqual);
initSymbolAndSystemFunction( OpenAppend);
initSymbolAndSystemFunction( OpenRead);
initSymbolAndSystemFunction( OpenWrite);
initSymbolAndSystemFunction( OptimizeExpression);
initSymbolAndSystemFunction( Order);
initSymbolAndSystemFunction( Outer);
initSymbolAndSystemFunction( Or);
initSymbolAndSystemFunction( Parse);
initSymbolAndSystemFunction( Part);
initSymbolAndSystemFunction( Print);
initSymbolAndSystemFunction( Quote);
initSymbolAndSystemFunction( Range);
initSymbolAndSystemFunction( ReadLine);
initSymbolAndSystemFunction( Replace);
initSymbolAndSystemFunction( Sequence);
initSymbolAndSystemFunction( Set);
initSymbolAndSystemFunction( SetGlobal);
initSymbolAndSystemFunction( Sort);
initSymbolAndSystemFunction( SymbolTable);
initSymbolAndSystemFunction( Table);
initSymbolAndSystemFunction( Tensor);
initSymbolAndSystemFunction( TensorData);
initSymbolAndSystemFunction( Timing);
initSymbolAndSystemFunction( Transpose);
initSymbolAndSystemFunction( Type);
initSymbolAndSystemFunction( Unique);
initSymbolAndSystemFunction( Unquote);
initSymbolAndSystemFunction( Which);
initSymbolAndSystemFunction( Write);

initSymbolAndSystemFunction( Abs);
initSymbolAndSystemFunction( ArcCos);
initSymbolAndSystemFunction( ArcCosh);
initSymbolAndSystemFunction( ArcSin);
initSymbolAndSystemFunction( ArcSinh);
initSymbolAndSystemFunction( ArcTan);
initSymbolAndSystemFunction( ArcTanh);
initSymbolAndSystemFunction( Ceiling);
initSymbolAndSystemFunction( Cos);
initSymbolAndSystemFunction( Cosh);
initSymbolAndSystemFunction( Erf);
initSymbolAndSystemFunction( Erfc);
initSymbolAndSystemFunction( Exp);
initSymbolAndSystemFunction( Floor);
initSymbolAndSystemFunction( Gamma);
initSymbolAndSystemFunction( Log);
initSymbolAndSystemFunction( Log2);
initSymbolAndSystemFunction( Log10);
initSymbolAndSystemFunction( Remainder);
initSymbolAndSystemFunction( Round);
initSymbolAndSystemFunction( Sign);
initSymbolAndSystemFunction( Sin);
initSymbolAndSystemFunction( Sinh);
initSymbolAndSystemFunction( Tan);
initSymbolAndSystemFunction( Tanh);
initSymbolAndSystemFunction( Truncate);



/* special functions return either an processed universal expression (ue)
 * or a state object. If a state object is returned the evaluator will
 * re-evaluate the content of the state object. To create a state object
 * ue_State_update_ue and/or ue_State_update_env should be used. If the 
 * state object ever gets additional content then this will be auto updated -
 * in contrast to ue_mk_State where the additional would need to be added
 * at every call of ue_mk_State */

/* to avoid re-eval after return form special function 
 * where Type(XY(as,sdf,df)) === XY => False, since
 * XY is sXY and not the hostfunction (hXY), this can be avoided
 * with 
 * static ue_p pfXY(const ue_p state) {
 *  u = ue_get_State_ue(state);
 *  ur = ue_mk_LumpShell(ue_get_LumpType(u),...);
 *  return ur;
 * }
 * */

extern ue_p ue_Parse( FILE *file, bool eatNewLine);

static ue_p pfParse(const ue_p state); 

/**/
/* helper functions */
/**/

static
ue_p not(const ue_p u) {
	if ( u == sTrue) {
		return sFalse;
	} else if ( u == sFalse) {
		return sTrue;
	}
	return u;
}

#define BOOL(pfname, ok, not_ok)\
static ue_p pfname(const ue_p state) {\
	ue_p ur, ui;\
	ue_p u = ue_get_State_ue(state);\
	ue_p env = ue_get_State_env(state);\
	size_t len = ue_get_LumpLength(u);\
	size_t i;\
\
\
	if (len == 0) return ok;\
\
	ui = ue_get_LumpElem(u, 1);\
	if ((len == 1) || (ui == not_ok)) {\
		return ui;\
	} else {\
		ur = ue_mk_Lump(ue_get_LumpType(u), 1, ui);\
	}\
\
	for (i = 2; i <= len; i++) {\
		ui = ue_get_LumpElem(u, i);\
		ui = ue_Eval(ui, env);\
		if (ui == not_ok) return not_ok;\
		if (ui != ok) ue_LumpAppendTo(ur, ui);\
	}\
\
	len = ue_get_LumpLength(ur);\
\
	if (len == 0) return ok;\
	if (len == 1) return ue_get_LumpElem(ur, 1);\
\
	return ur;\
\
}\

#define cmpEqual(a, b) ((a) == (b)) 
#define cmpGreater(a, b) ((a) > (b)) 
#define cmpGreaterEqual(a, b) ((a) >= (b)) 
#define cmpLess(a, b) ((a) < (b)) 
#define cmpLessEqual(a, b) ((a) <= (b)) 
#define cmpNotEqual(a, b) ((a) != (b)) 

/* TODO: ten_t and mixed input */
#define CMP_OP(name)\
static ue_p ue_##name(const ue_p u1In, const ue_p u2In) {\
	ue_p u1 = u1In, u2 = u2In;\
\
	if (ue_is_Symbol(u1)) u1 = ue_Coerce(u1, MR);\
	if (ue_is_Symbol(u2)) u2 = ue_Coerce(u2, MR);\
\
	if (ue_is_Number(u1) && ue_is_Number(u2)) {\
		if (cmp##name( num_cmp(ue_get_Number(u1), ue_get_Number(u2)), 0))\
			return sTrue;\
		else\
			return sFalse;\
	}\
\
	if (ue_is_List(u1) && ue_is_List(u2) && ue_length(u1) == ue_length(u2)) {\
		ue_p res = ue_mk_LumpShell(sList, ue_length(u1));\
		for(size_t i = 1; i <= ue_length(u1); i++) {\
			ue_set_LumpElem(res, i, ue_##name(ue_get_LumpElem(u1, i), ue_get_LumpElem(u2, i)));\
		}\
		return res;\
	}\
\
	return ue_mk_Lump(sf##name, 2, u1In, u2In);\
}\
\
static ue_p pf##name(const ue_p state) {\
	ue_p u1, u2;\
	ue_p u = ue_get_State_ue(state);\
\
	if (ue_get_LumpLength(u) != 2)\
		return u;\
\
	u1 = ue_get_LumpElem(u, 1);\
	u2 = ue_get_LumpElem(u, 2);\
\
	return ue_##name(u1, u2);\
\
}\

/* should this be _WIN32 */
#ifdef WIN32
static double get_time() {
    LARGE_INTEGER t, f;
	double tmp;
    QueryPerformanceCounter(&t);
    QueryPerformanceFrequency(&f);
	tmp = (double) f.QuadPart;
	tmp = (double) (t.QuadPart) / tmp;
	return tmp;
}
#else
static double get_time() {
	/* this is C99 */
    /*struct timeval t = (struct timeval) {0, 0};*/
	/*@-type@*/
    struct timeval t = {0, 0};
	/*@=type*/
	/*@-unrecog@*/
    gettimeofday(&t, NULL);
	/*@=unrecog@*/
    return t.tv_sec + (t.tv_usec * 1e-6);
}
#endif

static ue_p mkApplyState(ue_p f, ue_p g, ue_p state) {
	size_t i, len;
	ue_p u, gi;

	assert(ue_is_State(state));

	len = ue_get_LumpLength(g);
	u = ue_mk_LumpShell(f, len);
	for (i = 1; i <= len; i++) {
		gi = ue_get_LumpElem(g, i);
		ue_set_LumpElem(u, i, gi);
	}

	return ue_State_update_ue(state, u);
}

#define CHANGE_ATTRIBUTE(fun, ac)\
static ue_p fun##_##ac##_Attribute(ue_p f, ue_p attrIn) {\
	size_t i, len;\
	ue_p attr = attrIn;\
\
	assert(ue_is_##fun(f));\
\
	if (!ue_is_List(attr)) {\
		attr = ue_mk_Lump(sList, 1, attr);\
	}\
\
	len = ue_get_LumpLength(attr);\
	if (len == 0) {\
		ue_set_##fun##Attributes(f, NOATTR);\
		return f;\
	}\
\
	for (i = 1; i <= len; i++) {\
		ue_p thisAttr = ue_get_LumpElem(attr, i);\
		if (thisAttr == sListable) {\
			ue_##ac##_##fun##Attribute(f, LISTABLE);\
			continue;\
		} else if (thisAttr == sQuoteFirst) {\
			ue_##ac##_##fun##Attribute(f, QUOTEFIRST);\
			continue;\
		} else if (thisAttr == sQuoteRest) {\
			ue_##ac##_##fun##Attribute(f, QUOTEREST);\
			continue;\
		} else if (thisAttr == sQuoteAll) {\
			ue_##ac##_##fun##Attribute(f, QUOTEALL);\
			continue;\
		} else if (thisAttr == sQuoteFirstCoercion) {\
			ue_##ac##_##fun##Attribute(f, QUOTEFIRSTCOERCION);\
			continue;\
		} else if (thisAttr == sQuoteRestCoercion) {\
			ue_##ac##_##fun##Attribute(f, QUOTERESTCOERCION);\
			continue;\
		} else if (thisAttr == sQuoteAllCoercion) {\
			ue_##ac##_##fun##Attribute(f, QUOTEALLCOERCION);\
			continue;\
		} else {\
			return sFailed;\
		}\
	}\
\
	return f;\
}\

/* Symbol_Add_Attribute */
CHANGE_ATTRIBUTE(Symbol, add)
/* Symbol_Remove_Attribute */
CHANGE_ATTRIBUTE(Symbol, remove)

/* Function_Add_Attribute */
CHANGE_ATTRIBUTE(Function, add)
/* Function_Remove_Attribute */
CHANGE_ATTRIBUTE(Function, remove)

static ue_p ue_open(const ue_p state, const char *mode) {
	ue_p tmp;
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 1)
		return u;

	tmp = ue_get_LumpElem(u, 1);
	if (!ue_is_String(tmp))
		return u;

	return ue_mk_Stream(ue_get_String(tmp), mode);
}

static bool splitBindingList(ue_p bindings, ue_p *args, ue_p *vals) {
	bool error = false;
	size_t i, len;
	ue_p tmp;

	/* maybe too general? */
	if (!ue_is_Lump(bindings))
		return error;

	len = ue_get_LumpLength(bindings);

	*args = ue_mk_LumpShell(sList, len);
	*vals = ue_mk_LumpShell(sList, len);

	for (i = 1; i <= len; i++) {
		tmp = ue_get_LumpElem(bindings, i);
		/* maybe too general? */
		if (!ue_is_Lump(tmp) || (ue_get_LumpLength(tmp) < 2))
			return error;

		ue_set_LumpElem(*args, i, ue_get_LumpElem(tmp, 1));
		ue_set_LumpElem(*vals, i, ue_get_LumpElem(tmp, 2));

		if (!ue_is_Symbol( ue_get_LumpElem(*args, i)))
			return error;
	}

	return !error;
}

/*
 * host functions
 *
 * */

#define ATTRIBUTE_FUN(op, ops)\
static ue_p pfAttributes##op(const ue_p state) {\
	ue_p f, attr;\
	ue_p u = ue_get_State_ue(state);\
\
	if (ue_get_LumpLength(u) != 2) return u;\
\
	f = ue_get_LumpElem(u, 1);\
	attr = ue_get_LumpElem(u, 2);\
\
	if (ue_is_Function(f) && (Function_##ops##_Attribute(f, attr) == f)) {\
		return f;\
	} else if (ue_is_Symbol(f) && (Symbol_##ops##_Attribute(f, attr) == f)) {\
		return f;\
	}\
\
	return u;\
}\

/*pfAttributesAdd*/
/*pfAttributesRemove*/
ATTRIBUTE_FUN(Add, add)
ATTRIBUTE_FUN(Remove, remove)

BOOL(pfAnd, sTrue, sFalse)


/* apply(f, g(...)) -> f(...) */
static ue_p pfApply(const ue_p state) {
	size_t len;
	ue_p f, g;
	ue_p u = ue_get_State_ue(state);

	len = ue_get_LumpLength(u);
	if (len != 2)
		return u;

	f = ue_get_LumpElem(u, 1);
	g = ue_get_LumpElem(u, 2);

	if (ue_is_Tensor(g)) {
		ue_p temp = ue_TensorApply(f, g);
		if (ue_is_Tensor(temp)) return temp;
		return u;
	}

	if (!ue_is_Lump(g))
		return u;

	return mkApplyState(f, g, state);
}

static ue_p pfAttributes(const ue_p state) {
	size_t len;
	ue_p f, res;
	bool lstb;
	bool qf, qr;
	bool cqf, cqr;
	ue_p u = ue_get_State_ue(state);

	len = ue_get_LumpLength(u);
	if (len != 1) return u;

	f = ue_get_LumpElem(u, 1);

	if (ue_is_Symbol(f)) {
		lstb = ue_has_SymbolAttribute(f, LISTABLE);
		qf = ue_has_SymbolAttribute(f, QUOTEFIRST);
		qr = ue_has_SymbolAttribute(f, QUOTEREST);
		cqf = ue_has_SymbolAttribute(f, QUOTEFIRSTCOERCION);
		cqr = ue_has_SymbolAttribute(f, QUOTERESTCOERCION);
	} else if (ue_is_Function(f)) {
		lstb = ue_has_FunctionAttribute(f, LISTABLE);
		qf = ue_has_FunctionAttribute(f, QUOTEFIRST);
		qr = ue_has_FunctionAttribute(f, QUOTEREST);
		cqf = ue_has_FunctionAttribute(f, QUOTEFIRSTCOERCION);
		cqr = ue_has_FunctionAttribute(f, QUOTERESTCOERCION);
	} else {
		return u;
	}

	res = ue_mk_Lump(sList, 0);

	if (lstb) ue_LumpAppendTo(res, sListable);

	if (qf && qr) {
		ue_LumpAppendTo(res, sQuoteAll);
	} else if (qf) {
		ue_LumpAppendTo(res, sQuoteFirst);
	} else if (qr) {
		ue_LumpAppendTo(res, sQuoteRest);
	}

	if (cqf && cqr) {
		ue_LumpAppendTo(res, sQuoteAllCoercion);
	} else if (cqf) {
		ue_LumpAppendTo(res, sQuoteFirstCoercion);
	} else if (cqr) {
		ue_LumpAppendTo(res, sQuoteRestCoercion);
	}

	return res;
}

static ue_p pfBlock(const ue_p state) {
	ue_p body, bindings, args, vals;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);
	ue_p res;

	if (ue_get_LumpLength(u) != 2)
		return u;

	bindings = ue_get_LumpElem(u, 1);
	body = ue_get_LumpElem(u, 2);

	if (!splitBindingList(bindings, &args, &vals))
		return u;

	shadowVariableValue(args, vals, env);

	res = ue_Eval( body, env);

	unshadowVariableValue(args, env);

	return res;
}

static ue_p pfByteCount(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	if (ue_get_LumpLength(u) != 1)
		return u;

	u = ue_get_LumpElem(u, 1);
	return ue_ByteCount(u);
}

static ue_p pfClear(const ue_p state) {
	size_t i;
	ue_p var;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);
	size_t len = ue_get_LumpLength(u);
	
	if (len == 0)
		return sNull;

	for (i = 1; i <= len - 1; i++) {
		var = ue_get_LumpElem(u, i);
		(void) clearVariable(var, env);
	}

	var = ue_get_LumpElem(u, len);
	return clearVariable(var, env);
}


/* close(stream(path)) -> path */
static ue_p pfClose(const ue_p state) {
	ue_p stream;
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 1)
		return u;

	stream = ue_get_LumpElem(u, 1);

	if (!ue_is_Stream(stream))
		return u;

	return ue_StreamClose(stream);
}

static ue_p pfCoerce(const ue_p state) {
	ue_p arg, prec;
	ntype toType;
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 2)
		return u;

	arg = ue_get_LumpElem(u, 1);
	prec = ue_get_LumpElem(u, 2);

	toType = ue_CCoerceType(prec);
	if (!numtype_is_valid(toType)) {
		return u;
	}

	return ue_Coerce(arg, toType);
}

static ue_p pfCompare(const ue_p state) {
	long int r;
	ue_p u = ue_get_State_ue(state);
	
	if (ue_get_LumpLength(u) != 2)
		return u;

	r = ue_cmp( ue_get_LumpElem(u, 1), ue_get_LumpElem(u, 2));
	return ue_mk_mi_si(r);
}

/* Complex(a, b) */
static ue_p pfComplex(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p re, im;

	if (2 != ue_get_LumpLength(u))
		return u;

	re = ue_get_LumpElem(u, 1);
	im = ue_get_LumpElem(u, 2);
	return ue_mk_Complex(re, im);
}

/* Context() */
static ue_p pfContext(const ue_p state) {
	ue_p env = ue_get_State_env(state);
	return frameContext( firstFrame(env));
}

static ue_p pfConstantArray(const ue_p state) {
	ue_p u = ue_get_State_ue(state);

	if (2 != ue_get_LumpLength(u))
		return u;

	return ue_ConstantArray_Tensor(u);
}


/* pfEqual */
CMP_OP(Equal)


/* evaluate(u) */
static ue_p pfEvaluate(const ue_p state) {
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 1)
		return u;

	return ue_State_update_ue(state, ue_get_LumpElem(u, 1));
}

/* environment() */
static ue_p pfEnvironment(const ue_p state) {
	ue_p env = ue_get_State_env(state);
	/*return ue_cp(env);*/
	return env;
}

/* exit(u) */
static ue_p pfExit(const ue_p state) {
	int r = EXIT_SUCCESS;
	ue_p u = ue_get_State_ue(state);
	if (ue_get_LumpLength(u) != 0)
		r = EXIT_FAILURE;
	exit(r);
}

static ue_p pfOptimizeExpression(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);
	ue_p u1;

	if (ue_get_LumpLength(u) != 1)
		return u;

	u1 = ue_get_LumpElem(u, 1);

	return ue_InlineFunction(u1, env);
}

static ue_p sfInterpretedFunction;

/* function(args, body) -> function({args}, body)*/
static ue_p pfInterpretedFunction(const ue_p state) {
	size_t i, len;
	ue_p ui, args, body, pfun;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);
	ue_p scope = sStaticScope;
	ue_p frameName;
	bool variadicQ, ok;

	len = ue_get_LumpLength(u);

	if ( len < 2 || len > 3 )
		return u;

	args = ue_get_LumpElem(u, 1);
	body = ue_get_LumpElem(u, 2);

	variadicQ = (ue_get_Type(args) == sVariadic);
	if ( !(ue_is_List(args) || variadicQ))
		args = ue_mk_Lump(sList, 1, args);

	for (i = 1; i <= ue_get_LumpLength(args); i++) {
		ui = ue_get_LumpElem(args, i);
		if ( !ue_is_Symbol(ui)) {
			/* TODO: message */
			return sFailed;
		}
	}

	if (len == 3)
		scope = ue_get_LumpElem(u, 3);

	ok = ue_mk_UniqueSymbol(&frameName, sFrame);
	if (!ok) {
		/* TODO: message */
		return sFailed;
	}

	pfun = ue_mk_Lump(sfInterpretedFunction, 4, args, body, scope, frameName);

	if (scope == sDynamicScope) {
		env = sDynamicScope;
	}

	pfun = ue_mk_PureFunction(pfun, env);

	return pfun;
}


/* pfGreater */
CMP_OP(Greater)

/* pfGreaterEqual */
CMP_OP(GreaterEqual)

/* Hash(u) -> MInt */
static ue_p pfHash(const ue_p state) {
	ue_p u1;
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 1)
		return u;

	u1 = ue_get_LumpElem(u, 1);
	return ue_Hash(u1);
}


/* isSame(u1, u2) -> true/false */
static ue_p pfIsSame(const ue_p state) {
	ue_p u1, ui;
	size_t l, i = 2;
	bool sameQ = true;
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) < 2)
		return sTrue;

	u1 = ue_get_LumpElem(u, 1);
	l = ue_get_LumpLength(u);

	while (i <= l && sameQ) {
		ui = ue_get_LumpElem(u, i);
		sameQ = ue_is_Same(u1, ui);
		u1 = ui;
		i++;
	}
	if(!sameQ) return sFalse;

	return sTrue;
}

static ue_p pfIsNotSame(const ue_p state) {
	ue_p same = pfIsSame(state);
	return not(same);
}

#define IS_XY(name)\
static ue_p pfIs##name(const ue_p state) {\
	ue_p u1;\
	ue_p u = ue_get_State_ue(state);\
\
	if (ue_get_LumpLength(u) != 1)\
		return sFalse;\
\
	u1 = ue_get_LumpElem(u, 1);\
\
	if (ue_is_##name(u1))\
		return sTrue;\
	else\
		return sFalse;\
}\

/*pfIsEven*/
IS_XY(Even)
/*pfIsInterpretedFunction*/
#define ue_is_InterpretedFunction	ue_is_Function
IS_XY(InterpretedFunction)
/*pfIsList*/
IS_XY(List)
/*pfIsLump*/
IS_XY(Lump)
/*pfIsNumber*/
IS_XY(Number)
/*pfIsNumberExact*/
IS_XY(NumberExact)
/*pfIsNumberInexact*/
IS_XY(NumberInexact)
/*pfIsOdd*/
IS_XY(Odd)
/*pfIsStream*/
IS_XY(Stream)
/*pfIsString*/
IS_XY(String)
/*pfIsSymbol*/
IS_XY(Symbol)
/*pfIsTensor*/
IS_XY(Tensor)
/*pfIsTensorExact*/
IS_XY(TensorExact)
/*pfIsTensorInexact*/
IS_XY(TensorInexact)


/* if(cond, true, false, unknown) */
static ue_p pfIf(const ue_p state) {
	size_t len;
	ue_p cond, tmp;
	ue_p u = ue_get_State_ue(state);
	
	len = ue_get_LumpLength(u);
	if (len < 2 || len > 4)
		return u;

	cond = ue_get_LumpElem(u, 1);
	if (ue_is_Same(sTrue, cond)) {
		tmp = ue_get_LumpElem(u, 2);
		tmp = ue_State_update_ue(state, tmp);
		return tmp;
	} else if (ue_is_Same( sFalse, cond) ) {
		if (len > 2) {
			tmp = ue_get_LumpElem(u, 3);
			tmp = ue_State_update_ue(state, tmp);
			return tmp;
		}
		return sNull;
	} else if ( len == 4 ) {
		tmp = ue_get_LumpElem(u, 4);
		tmp = ue_State_update_ue(state, tmp);
		return tmp;
	} else {
		return u;
	}
}

/* join(str1, str2, ...) */

static ue_p pfJoin(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	return ue_Join(u);
}

/* length(u) */
static ue_p pfLength(const ue_p state) {
	ue_p tmp;
	ue_p u = ue_get_State_ue(state);

	tmp = ue_get_LumpElem( u, 1);
	if (ue_is_String(tmp)) {
		tmp = ue_get_StringCharacters(tmp);
	} else {
		tmp = ue_Length( tmp);
	}

	return tmp;
}

/* pfLess less */
CMP_OP(Less)

/* pfLessEqual lessEqual */
CMP_OP(LessEqual)


/*
 * let(list(list(a1, v1), ..), body)) ->	   
 * function(list(a1, a2,,), body)(v1, v2, ..) 
 *
 * let(list(), body) -> function(list(), body)
 *
 * let(list(list(a11, v11),..), list(list(a21, v21),..), body) -> 
 * function(list(a11,a1..), function(list(a21,a2..),body)(v21,v2..))(v11,v1..)
 *
 */
/* TODO: could be implemented as a syntax form */
static ue_p pfLet(const ue_p state) {
	ue_p fun, body, bindings, tmp, args, vals;
	ue_p u = ue_get_State_ue(state);
	size_t i, j, l, len = ue_get_LumpLength(u);

	if (len < 2)
		return u;

	body = ue_get_LumpElem(u, len);

	for (i = len - 1; i > 0; i--) {
		bindings = ue_get_LumpElem(u, i);
		if (!splitBindingList(bindings, &args, &vals))
			return u;
		fun = ue_mk_Lump(sfInterpretedFunction, 3, args, body, sStaticScope);
		l = ue_length(vals);
		body = ue_mk_LumpShell(fun, l);
		for (j = 1; j <= l; j++) {
			ue_set_LumpElem(body, j, ue_get_LumpElem(vals, j));
		} 
	}

	tmp = ue_State_update_ue(state, body);
	return tmp;
}

static ue_p pfLibraryFunction(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p ueLibraryFunction, ueLibraryName, ueSymbolName;
	const char *libraryName, *symbolName;
	void *handle;
	/* termux wants a const char * not a char * */
	const char *error;
	ue_p (*fp)(const ue_p state);

	if (ue_get_LumpLength(u) != 2)
		return u;

	ueSymbolName = ue_get_LumpElem(u, 1);
	if (!ue_is_String(ueSymbolName)) return u;
	symbolName = ue_get_String(ueSymbolName);

	ueLibraryName = ue_get_LumpElem(u, 2);
	if (!ue_is_String(ueLibraryName)) return u;
	libraryName = ue_get_String(ueLibraryName);

	handle = dlopen(libraryName, RTLD_LAZY);
	if (!handle) {
		fprintf(stderr, "%s\n", dlerror());
 		return sFailed;
	}

	dlerror();    /* Clear any existing error */

	/* Writing: cosine = (double (*)(double)) dlsym(handle, "cos");
	  would seem more natural, but the C99 standard leaves
	  casting from "void *" to a function pointer undefined.
	  The assignment used below is the POSIX.1-2003 (Technical
	  Corrigendum 1) workaround; see the Rationale for the
	  POSIX specification of dlsym(). */

	*(void **) (&fp) = dlsym(handle, symbolName);

	error = dlerror();
	if (error != NULL)  {
		fprintf(stderr, "%s\n", error);
		return sFailed;
	}

	ueLibraryFunction = ue_mk_Lump(sfLibraryFunction,
		2, ueSymbolName, ueLibraryName);
	return ue_mk_SystemFunction(ueLibraryFunction, fp);
}

/* load("file") */
/* load multi line ue */
static ue_p pfLoad(const ue_p state) {
	ue_p tmp, instream;
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 1)
		return u;

	instream = ue_open(state, "r");
	if (!ue_is_InputStream(instream))
		return u;

	tmp = ue_mk_Lump(sParse, 2, instream, sTrue);
	tmp = ue_State_update_ue(state, tmp);

	tmp = pfParse(tmp);
	tmp = ue_State_update_ue(state, tmp);

	ue_StreamClose(instream);

	return tmp;
}

static ue_p pfLoadLibraryFunction(const ue_p state) {
	return pfLibraryFunction(state);
}

static ue_p pfLookupData(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p keyvalues, defaultValue, keys, values;

	if (ue_get_LumpLength(u) != 2)
		return u;

	keyvalues = ue_get_LumpElem(u, 1);
	defaultValue = ue_get_LumpElem(u, 2);

	if (ue_get_LumpLength(keyvalues) != 2)
		return u;

	keys = ue_get_LumpElem(keyvalues, 1);
	values = ue_get_LumpElem(keyvalues, 2);

	if (!(ue_is_Lump(keys) && ue_is_Lump(values)))
		return u;

	return ue_mk_LookupData(keys, values, defaultValue);
}

static ue_p pfLookupDataAdd(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p data, kv, keys, values, res;
	bool single = false;

	if (ue_get_LumpLength(u) != 2)
		return u;

	data = ue_get_LumpElem(u, 1);
	kv = ue_get_LumpElem(u, 2);

	if (!ue_is_Data(data))
		return u;

	if (!(ue_is_List(kv) && (ue_get_LumpLength(kv) == 2)))
		return u;

	keys = ue_get_LumpElem(kv, 1);
	values = ue_get_LumpElem(kv, 2);

	if (!ue_is_List(keys))
		keys = ue_mk_Lump(sList, 1, keys);

	if (!ue_is_List(values)) {
		values = ue_mk_Lump(sList, 1, values);
		single = true;
	}

	if (ue_get_LumpLength(keys) == ue_get_LumpLength(values)) {
		res = ue_LookupDataAdd(data, keys, values);
		if (single) res = ue_get_LumpElem(res, 1);
		return res;
	}

	return sFailed;
}

static ue_p pfLookupDataDefaultValue(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p data;

	if (ue_get_LumpLength(u) != 1)
		return u;

	data = ue_get_LumpElem(u, 1);

	if (!ue_is_Data(data))
		return u;

	return ue_LookupDataDefaultValue(data);
}

static ue_p pfLookupDataFind(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p data, key;
	bool single = false;

	if (ue_get_LumpLength(u) != 2)
		return u;

	data = ue_get_LumpElem(u, 1);
	if (!ue_is_Data(data))
		return u;

	key = ue_get_LumpElem(u, 2);
	if (!ue_is_List(key)) {
		key = ue_mk_Lump(sList, 1, key);
		single = true;
	}

	if (single)
		return ue_get_LumpElem(ue_LookupDataFind(data, key), 1);

	return ue_LookupDataFind(data, key);
}

static ue_p pfLookupDataKeyValues(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p data;

	if (ue_get_LumpLength(u) != 1)
		return u;

	data = ue_get_LumpElem(u, 1);

	if (!ue_is_Data(data))
		return u;

	return ue_LookupDataKeyValues(data);
}

#define ITERATOR(name, store)\
static ue_p pf##name(const ue_p state) {\
	size_t len, iterLen;\
	size_t to = 1, i;\
	ue_p uefrom, ueto, uestep;\
	ue_p body, iterData, val, var = sNull, ele = sNull;\
	ue_p u = ue_get_State_ue(state);\
	ue_p env = ue_get_State_env(state);\
	num_t nt, nf, ns, ni;\
	ue_p res = sNull;\
	ue_p tmp;\
	ue_p vars, vals;\
	ue_p context, shadowed;\
\
	len = ue_get_LumpLength(u);\
	if (2 != len)\
		return u;\
\
	body = ue_get_LumpElem(u, 1);\
\
	iterData = ue_get_LumpElem(u, 2);\
\
	if( !(ue_is_List(iterData))) {\
		iterData = ue_mk_Lump(sList, 1, iterData);\
	}\
\
	iterLen = ue_get_LumpLength( iterData);\
	uefrom = ueto = uestep = ue_mk_mi_si(1);\
\
	if (iterLen == 1) {\
		ueto = ue_get_LumpElem( iterData, 1);	\
		ueto = ue_Eval( ueto, env);				\
	}\
\
	if (iterLen == 2) {\
		var = ue_get_LumpElem( iterData, 1);	\
		ueto = ue_get_LumpElem( iterData, 2);	\
		ueto = ue_Eval( ueto, env);				\
		if (ue_is_List(ueto)) {\
			ele = ueto;\
			ueto = ue_Length(ele);\
		}\
	}\
\
	if (iterLen == 3) {\
		var = ue_get_LumpElem( iterData, 1);	\
		uefrom = ue_get_LumpElem( iterData, 2);	\
		uefrom = ue_Eval( uefrom, env);			\
		ueto = ue_get_LumpElem( iterData, 3);	\
		ueto = ue_Eval( ueto, env);				\
	}\
\
	if (iterLen == 4) {\
		var = ue_get_LumpElem( iterData, 1);	\
		uefrom = ue_get_LumpElem( iterData, 2);	\
		uefrom = ue_Eval( uefrom, env);			\
		ueto = ue_get_LumpElem( iterData, 3);	\
		ueto = ue_Eval( ueto, env);				\
		uestep = ue_get_LumpElem( iterData, 4);	\
		uestep = ue_Eval( uestep, env);			\
	}\
\
	if (!(ue_is_Number(uefrom) && ue_is_Number(ueto) && ue_is_Number(uestep))) {\
		return u;\
	}\
\
	nt = ue_get_Number(ueto);\
	nf = ue_get_Number(uefrom);\
	ns = ue_get_Number(uestep);\
	to = num_iterateSteps(nf, nt, ns);\
\
	if (store) {\
		res = ue_mk_LumpShell(sList, to);\
	}\
\
	if (!ue_is_Symbol(var)) {\
		return u;\
	}\
\
	/* loop */\
	if (var == sNull) {\
		for (i = 0; i < to; i++) {\
			if (store) {\
				tmp = ue_Eval( body, env);\
				ue_set_LumpElem(res, i + 1, tmp);\
			} else {\
				(void) ue_Eval( body, env);\
			}\
		}\
	} else {\
		nf = ue_get_Number(uefrom);\
		ns = ue_get_Number(uestep);\
		vars = ue_mk_Lump(sList, 1, var);\
		vals = ue_mk_Lump(sList, 1, sNull);\
		context = frameContext( firstFrame(env));\
		shadowed = shadowVariableValue(vars, vals, env);\
		for (i = 0; i < to; i++) {\
			if (ele == sNull) {\
				/* ni = i*ns + nf seems more accurate then ni =nf; ni += ns*/\
				ni = num_mk_mi_si(i);\
				num_mul(&ni, ni, ns);\
				num_add(&ni, ni, nf);\
				val = ue_mk_Number(ni);\
			} else {\
				val = ue_get_LumpElem(ele, i + 1);\
			}\
			defineVariableValue(context, var, val, env);\
			if (store) {\
				tmp = ue_Eval( body, env);\
				ue_set_LumpElem(res, i + 1, tmp);\
			} else {\
				(void) ue_Eval( body, env);\
			}\
		}\
		unshadowVariableValue(shadowed, env);\
	}\
\
	return res;\
}\

/* Loop( body, to) */
/* Loop( body, {to}) */
/* Loop( body, {var, to}) */
/* Loop( body, {var, from, to}) */
/* Loop( body, {var, from, to, step}) */
/* TODO: Loop( body, {var, {item1, item2,...}}) */
/* pfLoop */
ITERATOR(Loop, false)
/* pfTable */
ITERATOR(Table, true)

/*
 * Map(f, {l11, l12, .., l1c}, {l21, .., l2c}, .., {lr1, .., lrc}) ->
 *	List( 
 * 		f(l11, l21, .., lr1),
 * 		f(l12, l22, .., lr2),
 * 		..
 * 		f(l1c, l2c, .., lrc)
 * 	)
 *
 *
 * could possibly have Map(f, h1(l11, l12, ..), h2(l21, ..), ..) -> 
 * 	h1( f(l11, ..), ..)
 * 
 **/
/* TODO: Scan */
static ue_p pfMap(const ue_p state) {
	size_t len, clen, rlen, r, c;
	ue_p f, l1, lr, tmp, tmp2, res;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);

	len = ue_get_LumpLength(u);
	if (len < 2) return u;

	f = ue_get_LumpElem(u, 1);
	l1 = ue_get_LumpElem(u, 2);

	if (!ue_is_Lump(l1)) return u;
	clen = ue_get_LumpLength(l1);

	for (r = 3; r <= len; r++) {
		lr = ue_get_LumpElem(u, r);
		if (!ue_is_Lump(lr) || ue_get_LumpLength(lr) != clen)
			return u;
	}

	rlen = len - 1;
	res = ue_mk_LumpShell(sList, clen);

	for (c = 1; c <= clen; c++) {
		tmp = ue_mk_LumpShell(f, rlen);
		for (r = 1; r <= rlen; r++) {
			lr = ue_get_LumpElem(u, r + 1); 
			ue_set_LumpElem(tmp, r, ue_get_LumpElem(lr, c));
		}
		tmp2 = ue_Eval(tmp, env);
		ue_set_LumpElem(res, c, tmp2);
	}

	return res;
}

static ue_p pfMemoryInUse(const ue_p state) {
	return ue_MemoryInUse();
}

static ue_p pfNot(const ue_p state) {
	ue_p u, b, tmp;
	u = ue_get_State_ue(state);
	if (ue_get_LumpLength(u) != 1)
		return u;
	b = ue_get_LumpElem(u, 1);
	tmp = not(b);
	if (tmp == b) return u;
	return tmp;
}

/* pfNotEqual */
CMP_OP(NotEqual)

static ue_p pfOpenAppend(const ue_p state) {
	return ue_open(state, "a");
}

static ue_p pfOpenRead(const ue_p state) {
	return ue_open(state, "r");
}

static ue_p pfOpenWrite(const ue_p state) {
	return ue_open(state, "w");
}

static ue_p pfOrder(const ue_p state) {
	ue_p u = ue_get_State_ue(state);\
	ue_p u1, u2;

	if (ue_get_LumpLength(u) != 2)
		return u;

	u1 = ue_get_LumpElem(u, 1);
	u2 = ue_get_LumpElem(u, 2);

	return ue_mk_mi_si(ue_cmp(u1, u2));
}

/* Or(u1, u2, ..) -> True/False */
BOOL(pfOr, sFalse, sTrue)

static ue_p pfOuter(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	size_t len = ue_get_LumpLength(u);
	size_t i;
	ue_p lst, ui, res;


	if (len < 1)
		return u;

	lst = ue_mk_LumpShell(sList, len - 1);
	for (i = 2; i <= len; i++) {
		ui = ue_get_LumpElem(u, i);
		if (ue_is_EmptyList(ui)) return u;
		ue_set_LumpElem(lst, i - 1, ui);
	}

	if (ue_get_LumpElem(u, 1) == sList) {
		res = ue_OuterList(lst);
		if (res != lst) return res;
	}

	return u;
}

/* parse() ->u2; default input stream is stdin */
/* parse(inputstream) -> ue */
/* parse(string) -> ue */
/* TODO eat newline should be an option */
/* parse(inputstream/string, bool eatnewline) -> ue*/
/* ue is not evaluated */
static ue_p pfParse(const ue_p state) {
	ue_p u1, u2, res;
	FILE *f = NULL;
	ue_p u = ue_get_State_ue(state);
	bool eatNewLine = true;
	bool close_file_p = false;
	size_t len = ue_get_LumpLength(u);

	if (len > 2)
		return u;
	
	if (len == 0) {
		f = stdin;
		eatNewLine = false;
	}

	if (len > 0) {
		u1 = ue_get_LumpElem(u, 1);
	
		if (ue_is_String(u1)) {
			char newline = '\n';
			size_t size = ue_get_StringLength(u1);
			close_file_p = true;
			/* f = fmemopen( ue_get_String(u1), l, "r"); */
			/* http://compgroups.net/comp.lang.c/fmemopen-portable-version/712598 */
			f = tmpfile();
			if (f == NULL) return u;
		    fwrite( ue_get_String(u1), sizeof(char), size, f);
		    fwrite( &newline, sizeof(char), 1, f);
		    rewind(f);
	
		} else if (ue_is_InputStream(u1)) {
			f = ue_get_StreamFP(u1);
			if (f == stdin)
				eatNewLine = false;
		} else {
			return u;
		}
	}

	if (len > 1) {
		u2 = ue_get_LumpElem(u, 2);
		if (u2 == sTrue)
			eatNewLine = true;
		else if (u2 == sFalse)
			eatNewLine = false;
		else
			return u;
	}

	if (f == NULL) {
		/*TODO: message  */
		return u;
	}

	res = ue_Parse(f, eatNewLine);

	if (close_file_p)
		fclose(f);

	return res;
}

/* part( t(.., ei,...), i) -> ei*/
/* part( "string", i) -> ei*/
/* part( tensor, i) -> ei*/

/* part( t(.., ei,...), MIntTensor({i})) -> {ei}*/
/* part( "string", MIntTensor({i})) -> {ei}*/
/* part( tensor, MIntTensor({i})) -> Tensor({ei})*/

/* part( t(.., ei,...), splice( from, to, ?steps), ..) */
/* part( "string", splice( from, to, ?steps), ..) */
/* part( tensor, splice( from, to, ?steps), ..) */



static ue_p pfPart(const ue_p state) {
	size_t len;
	ue_p u1, res, spec;
	ue_p u = ue_get_State_ue(state);
	part_spec_type_t spectype;

	assert(ue_is_Lump(u));

	len = ue_get_LumpLength(u);
	if (len < 1) return u;

	u1 = ue_get_LumpElem(u, 1);

	if (len < 2) return u1;

	part_specification_analysis(u, &spectype, &spec);

	if (spectype == SINGLE) {
		res = ue_part_single(u1, ue_get_Tensor(spec));
	} else if (spectype == SCATTERED) {
		res = ue_part_scattered(u1, ue_get_Tensor(spec));
	} else if (spectype == STRUCTURED) {
		res = ue_part_structured(u1, spec);
	} else {
		return u;
	}

	/* returned 'unevaluated' */
	if (res == u1) return u;

	return res;

}

/* print(u) */
static ue_p pfPrint(const ue_p state) {
	FILE *f = stdout;
	size_t i, len;
	ue_p u = ue_get_State_ue(state);
	ue_p res = sNull;

	len = ue_get_LumpLength(u);

	for (i = 1; i < len; i++)
		ue_PrintRaw( f, ue_get_LumpElem(u, i), false);

	if (len) {
		res = ue_get_LumpElem(u, len);
		ue_PrintRaw( f, res, false);
	}

	fprintf(f, "\n");
	/*TODO: check return value*/
	(void) fflush(f);

	return res;
}

/* quote(u) -> u*/
static ue_p pfQuote(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	return ue_get_LumpElem(u, 1);
}

/* range(stop)				 */
/* range(start, stop)		 */
/* range(start, stop, step)	 */
static ue_p pfRange(const ue_p state) {
	ue_p u = ue_get_State_ue(state);

	return ue_Range(u);
}

/* rational(a, b) */
static ue_p pfRational(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p num, den;
	
	if (2 != ue_get_LumpLength(u))
		return u;

	num = ue_get_LumpElem(u, 1);
	den = ue_get_LumpElem(u, 2);
	return ue_mk_Rational(num, den);
}

/* http://stackoverflow.com/questions/3501338/c-read-file-line-by-line */
static ue_p pfReadLine(const ue_p state) {
	ue_p tmp, u = ue_get_State_ue(state);
	FILE *f;
    size_t len = 128, pos = 0;
	int c;

	if (ue_get_LumpLength(u) != 1)
		return u;

	u = ue_get_LumpElem(u, 1);

	if (!ue_is_InputStream(u))
		return u;

	f = ue_get_StreamFP(u);

	char *buffer = (char *) ue_Malloc(sizeof(char) * len);
	validateMemory(buffer, "pfRead");

	c = getc(f);
	while ((c != '\n') && (c != EOF)) {
		if (pos == len) {
			len += 128;
			buffer = ue_Realloc(buffer, (size_t) len);
			validateMemory(buffer, "pfRead");
		}
		buffer[pos] = c;
		pos++;
		
		c = getc(f);
	}

	if (pos == 0 && c == EOF) {
		tmp = sEndOfFile;
	} else {
		buffer[pos] = '\0';
		tmp = ue_mk_String(buffer);
	}

	ue_Free(buffer);

	return tmp;
}

static ue_p pfReplace(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p res, uin, vars, vals, bindings;
	bool ok;

	if (ue_length(u) != 2) return u;

	bindings = ue_get_LumpElem(u, 1);
	uin = ue_get_LumpElem(u, 2);

	res = ue_cp(uin);

	ok = splitBindingList(bindings, &vars, &vals);
	if (!ok) return uin;

	ok = ue_ReplaceList(&res, ue_is_Same, vars, vals);
	if(!ok || res == uin) return uin;

	return res;
}

/* sequence(u1, u2, .., un) -> un */
/* TODO: could be a syntax form */
static ue_p pfSequence(const ue_p state) {
	size_t len, i;
	ue_p u, env, tmp;

	u = ue_get_State_ue(state);
	env = ue_get_State_env(state);

	len = ue_get_LumpLength(u);

	if (len == 0) return sNull;

	for (i = 1; i < len; i++) {
		tmp = ue_get_LumpElem( u, i);
		(void) ue_Eval(tmp, env);
	}

	return ue_State_update_ue(state, ue_get_LumpElem( u, len));
}

/* scheme set! */
static ue_p pfSetGlobal(const ue_p state) {
	ue_p var, val, tmp;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);

	if (ue_get_LumpLength(u) != 2)
		return u;

	var = ue_get_LumpElem(u, 1);
	val = ue_get_LumpElem(u, 2);

	if (!ue_is_Symbol(var))
		return u;

	tmp = setVariableValue(var, val, env);
	return tmp;
}

/* set(var1, val1) -> val1 */
/* set(list(var1, var2), list(val1, val2)) -> list(val1, val2)*/
/* scheme define */



static ue_p pfSet(const ue_p state) {
	ue_p t, var, val, tmp, tmp2, context;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);
	size_t len = ue_get_LumpLength(u);


	if (len < 2 || len > 3) return u;

	var = ue_get_LumpElem(u, 1);
	val = ue_get_LumpElem(u, 2);

	if (len == 3) {
		context = ue_get_LumpElem(u, 3);
	} else {
		context = frameContext( firstFrame(env));
	}

	t = ue_get_Type(var); 

	/* TODO: should 'context' be passed on to set_part? */
	if (t == sPart) {
		tmp = set_part(var, val, env);
		return tmp;
	}

	if (t == sList && ue_is_List(val)) {
		size_t i;
		size_t len = ue_get_LumpLength(var);
		if ( len != ue_get_LumpLength(val)) {
			/* TODO: message */
			return sFailed;
		}

		tmp = ue_mk_LumpShell(sList, len);
		for (i = 1; i <= len; i++) {
			/* should be sfSet not sList */
			tmp2 = ue_mk_Lump(sList, 3, ue_get_LumpElem(var, i), ue_get_LumpElem(val, i), context);
			tmp2 = pfSet(ue_State_update_ue(state, tmp2));
			ue_set_LumpElem(tmp, i, tmp2);
		}
		return tmp;
	}

	if (!ue_is_Symbol(var))
		return u;

	tmp = defineVariableValue(context, var, val, env);
	return tmp;
}

static ue_p pfSort(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	ue_p u1, utmp;

	if (ue_get_LumpLength(u) != 1)
		return u;

	u1 = ue_get_LumpElem(u, 1);
	utmp = ue_Sort(u1);

	if (u1 == utmp) return u;

	return utmp;
}

/* SymbolTable() */
static ue_p pfSymbolTable(const ue_p state) {
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 0)
		return u;

	return ue_SymbolTable();
}

static ue_p pfTensor(const ue_p state) {
	ue_p in;
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 1)
		return u;

	in = ue_get_LumpElem(u, 1);

	return ue_mk_TensorFromList(in);
}

static ue_p pfTensorData(const ue_p state) {
	return pfTensor(state);
}

/* we compute wall clock time */
static ue_p pfTiming(const ue_p state) {
	double start, end;
	ue_p u1, res, timing;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);

	if (ue_get_LumpLength(u) != 1)
		return u;

	u1 = ue_get_LumpElem(u, 1);

	start = get_time();
	res = ue_Eval( u1, env);
	end = get_time();

	timing = ue_mk_mr_d( end - start);

	return ue_mk_Lump(sList, 2, timing, res);
}

static ue_p pfTranspose(const ue_p state) {
	size_t *dims;
	size_t i, l1, l2;
	ue_p ten, tmp2, ele;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);
	
	l1 = ue_get_LumpLength(u);
	if (l1 < 1 || l1 > 2)
		return u;

	ten = ue_get_LumpElem(u, 1);
	if (ue_is_Tensor(ten)) {
		if (l1 == 1) {
			l2 = 2;
			dims = (size_t*) ue_Malloc( l2 * sizeof(size_t));
			validateMemory(dims, "pfTranspose");
			dims[0] = 2;
			dims[1] = 1;
		} else {
			tmp2 = ue_get_LumpElem(u, 2);
			tmp2 = ue_Eval( tmp2, env);
			if (!ue_is_List(tmp2))
				return u;
			l2 = ue_get_LumpLength(tmp2);
			dims = (size_t*) ue_Malloc( l2 * sizeof(size_t));
			validateMemory(dims, "pfTranspose");
			for (i = 1; i <= l2; i++) {
				ele = ue_get_LumpElem(tmp2, i);
				if (!ue_is_mi(ele) || ue_get_mi(ele) < 1)
					return u;
				dims[i - 1] = (size_t) ue_get_mi(ele);
			}	
		}

		ten = ue_TensorTranspose(ten, l2, dims);
		return ten;
	}

	return u;
}

/* type(g(1,2,3)) -> g */
static ue_p pfType(const ue_p state) {
	ue_p tmp;
	ue_p u = ue_get_State_ue(state);

	if (ue_get_LumpLength(u) != 1)
		return u;

	tmp = ue_get_LumpElem(u, 1);
	tmp = ue_get_Type( tmp);
	return tmp;
}

static ue_p pfUnique(const ue_p state) {
	ue_p ui, res;
	ue_p u = ue_get_State_ue(state);
	bool ok = false;

	if (ue_get_LumpLength(u) != 1)
		return u;

	ui = ue_get_LumpElem(u, 1);
	if (ue_is_Symbol(ui)) {
		ok = ue_mk_UniqueSymbol(&res, ui);
	}

	if (ue_is_List(ui)) {
		ok = ue_mk_UniqueSymbolList(&res, ui);
	}

	if (ok) return res;

	return u;
}


/* unquote(u) -> u*/
static ue_p pfUnquote(const ue_p state) {
	ue_p u = ue_get_State_ue(state);
	return ue_get_LumpElem(u, 1);
}

static ue_p pfWhich(const ue_p state) {
	size_t len, i, j, rlen;
	ue_p tmp, res = sNull;
	ue_p u = ue_get_State_ue(state);
	ue_p env = ue_get_State_env(state);

	len = ue_get_LumpLength(u);

	if (!(len % 2 == 0)) {
		/* message */
		return u;
	}

	for (i = 1; i < len; i = i + 2) {
		tmp = ue_get_LumpElem(u, i);
		tmp = ue_Eval(tmp, env);
		if ( tmp == sTrue ) {
			res = ue_get_LumpElem(u, i + 1);
			res = ue_State_update_ue(state, res);
			break;
		}
		if ( tmp != sFalse ) {
			rlen = len - i + 1;
			res = ue_mk_LumpShell(sfWhich, rlen);
			for (j = 1; j <= rlen; j++) {
				ue_set_LumpElem(res, j, ue_get_LumpElem(u, j + i - 1));
			}
			break;
		}
	}

	return res;
}

/* write(stream, u) */
static ue_p pfWrite(const ue_p state) {
	FILE *f;
	size_t i, len;
	ue_p stream;
	ue_p u = ue_get_State_ue(state);

	len = ue_get_LumpLength(u);

	if (len < 2)
		return u;

	stream = ue_get_LumpElem(u, 1);
	if (!ue_is_OutputStream(stream)) {
		/* message */
		return u;
	}
	f = ue_get_StreamFP(stream);

	for (i = 2; i <= len; i++)
		ue_PrintRaw( f, ue_get_LumpElem(u, i), false);

	/*TODO: check return value*/
	(void) fflush(f);

	return sNull;
}

#define PF_MATH_FUNCTION(fun)\
static ue_p pf##fun(const ue_p state) {\
	ue_p u = ue_get_State_ue(state);\
	return ue_##fun(u);\
}\

PF_MATH_FUNCTION(Abs)
PF_MATH_FUNCTION(ArcCos)
PF_MATH_FUNCTION(ArcCosh)
PF_MATH_FUNCTION(ArcSin)
PF_MATH_FUNCTION(ArcSinh)
PF_MATH_FUNCTION(ArcTan)
PF_MATH_FUNCTION(ArcTanh)
PF_MATH_FUNCTION(Ceiling)
PF_MATH_FUNCTION(Cos)
PF_MATH_FUNCTION(Cosh)
PF_MATH_FUNCTION(Erf)
PF_MATH_FUNCTION(Erfc)
PF_MATH_FUNCTION(Exp)
PF_MATH_FUNCTION(Floor)
PF_MATH_FUNCTION(Gamma)
PF_MATH_FUNCTION(GCD)
PF_MATH_FUNCTION(Log)
PF_MATH_FUNCTION(Log2)
PF_MATH_FUNCTION(Log10)
PF_MATH_FUNCTION(Max)
PF_MATH_FUNCTION(Min)
PF_MATH_FUNCTION(Add)
PF_MATH_FUNCTION(Power)
PF_MATH_FUNCTION(Remainder)
PF_MATH_FUNCTION(Round)
PF_MATH_FUNCTION(Sign)
PF_MATH_FUNCTION(Sin)
PF_MATH_FUNCTION(Sinh)
PF_MATH_FUNCTION(Tan)
PF_MATH_FUNCTION(Tanh)
PF_MATH_FUNCTION(Multiply)
PF_MATH_FUNCTION(Truncate)

extern 
void ue_init_special_forms() {

	/* symbols that have no system function assocciated */
    sDynamicScope		= ue_mk_Symbol( "DynamicScope");
	sEndOfFile			= ue_mk_Symbol( "EndOfFile");
    sFalse				= ue_mk_Symbol( "False");
    sListable			= ue_mk_Symbol( "Listable");
    sNone				= ue_mk_Symbol( "None");
    sQuoteAll 			= ue_mk_Symbol( "QuoteAll");
    sQuoteFirst			= ue_mk_Symbol( "QuoteFirst");
    sQuoteRest			= ue_mk_Symbol( "QuoteRest");
    sQuoteAllCoercion	= ue_mk_Symbol( "QuoteAllCoercion");
    sQuoteFirstCoercion	= ue_mk_Symbol( "QuoteFirstCoercion");
    sQuoteRestCoercion	= ue_mk_Symbol( "QuoteRestCoercion");
    sStaticScope		= ue_mk_Symbol( "StaticScope");
    sTrue				= ue_mk_Symbol( "True");
    sVariadic			= ue_mk_Symbol( "Variadic");


	/* OS dependent variables */
	addSystemVariable( "$OperatingSystem",	ue_mk_Symbol("Unix"));
#ifdef _WIN32
	addSystemVariable( "$OperatingSystem",	ue_mk_Symbol("Windows"));
#endif /* _WIN32 */

	/* infinite loop */
	/*addSystemVariable( "environment", ue_system_env);*/
	addSystemVariable( "$Attributes",		ue_mk_Lump(sList, 7,
		sListable,
		sQuoteAll, sQuoteFirst, sQuoteRest,
		sQuoteAllCoercion, sQuoteFirstCoercion, sQuoteRestCoercion));
	addSystemVariable( "$MinMachineInteger",ue_mk_mi_si(MI_MIN));
	addSystemVariable( "$MachineEpsilon",	ue_mk_mr_d(DBL_EPSILON));
	addSystemVariable( "$MaxMachineInteger",ue_mk_mi_si(MI_MAX));
	addSystemVariable( "$StandardInput",	ue_mk_Stream_from_FILE(stdin,
												"StandardInput", "r"));
	addSystemVariable( "$StandardOutput",	ue_mk_Stream_from_FILE(stdout,
												"StandardOutput", "w"));
	addSystemVariable( "$StandardError",	ue_mk_Stream_from_FILE(stderr,
												"StandardError", "w"));
//	addSystemVariable( "$SymbolTable",		ue_SymbolTable());


/*
 * Getting the type of an unevaluated SystemFunction has to work like this:
 *
 * 1 : Type(Multiply)
 * 1=> SystemFunction
 *
 * 2 : Type(Multiply(a,b))
 * 2=> Multiply
 *
 * 3 : Type(Type(Multiply(a,b)))
 * 3=> SystemFunction
 *
 * If one calles ue_mk_Lump one can not call with sMultiply but has to call
 * with hMultiply because else it would return:
 *
 * 4 : Type(Type(Multiply(a,b)))
 * 4=> Symbol
 *
 * but that were confusing compared to 1.
 *
 * */

	/* primitive functions */
	attachSystemFunction( Apply,				NOATTR);
	attachSystemFunction( Attributes,			NOATTR);
	attachSystemFunction( AttributesAdd,			NOATTR);
	attachSystemFunction( AttributesRemove,			NOATTR);
	attachSystemFunction( ByteCount,			NOATTR);
	attachSystemFunction( Close,				NOATTR);
	attachSystemFunction( Coerce,				NOATTR);
	attachSystemFunction( Compare,				NOATTR);
	attachSystemFunction( Complex,				NOATTR);
	attachSystemFunction( Context,				NOATTR);
	attachSystemFunction( ConstantArray,			NOATTR);
	attachSystemFunction( Environment,			NOATTR);
	attachSystemFunction( Equal,				NOATTR);
	attachSystemFunction( Evaluate,				NOATTR);
	attachSystemFunction( Exit,				NOATTR);
	attachSystemFunction( Greater,				NOATTR);
	attachSystemFunction( GreaterEqual,			NOATTR);
	attachSystemFunction( Hash,				NOATTR);
	attachSystemFunction( IsInterpretedFunction,		NOATTR);
	attachSystemFunction( IsEven,				NOATTR);
	attachSystemFunction( IsList,				NOATTR);
	attachSystemFunction( IsLump,				NOATTR);
	attachSystemFunction( IsNotSame,			NOATTR);
	attachSystemFunction( IsNumber,				NOATTR);
	attachSystemFunction( IsNumberExact,			NOATTR);
	attachSystemFunction( IsNumberInexact,			NOATTR);
	attachSystemFunction( IsOdd,				NOATTR);
	attachSystemFunction( IsStream,				NOATTR);
	attachSystemFunction( IsString,				NOATTR);
	attachSystemFunction( IsSymbol,				NOATTR);
	attachSystemFunction( IsSame,				NOATTR);
	attachSystemFunction( IsTensor,				NOATTR);
	attachSystemFunction( IsTensorExact,			NOATTR);
	attachSystemFunction( IsTensorInexact,			NOATTR);
	attachSystemFunction( Join,				NOATTR);
	attachSystemFunction( Length,				NOATTR);
	attachSystemFunction( Less,				NOATTR);
	attachSystemFunction( LessEqual,			NOATTR);
	attachSystemFunction( LibraryFunction,			NOATTR);
	attachSystemFunction( Load,				NOATTR);
	attachSystemFunction( LoadLibraryFunction,		NOATTR);
	attachSystemFunction( LookupData,			NOATTR);
	attachSystemFunction( LookupDataAdd,			NOATTR);
	attachSystemFunction( LookupDataDefaultValue,		NOATTR);
	attachSystemFunction( LookupDataFind,			NOATTR);
	attachSystemFunction( LookupDataKeyValues,		NOATTR);
	attachSystemFunction( Max,				NOATTR);
	attachSystemFunction( Map,				NOATTR);
	attachSystemFunction( MemoryInUse,			NOATTR);
	attachSystemFunction( Min,				NOATTR);
	attachSystemFunction( Not,				NOATTR);
	attachSystemFunction( NotEqual,				NOATTR);
	attachSystemFunction( OpenAppend,			NOATTR);
	attachSystemFunction( OpenRead,				NOATTR);
	attachSystemFunction( OpenWrite,			NOATTR);
	attachSystemFunction( Order,				NOATTR);
	attachSystemFunction( Outer,				NOATTR);
	attachSystemFunction( Parse,				NOATTR);
	attachSystemFunction( Part,				NOATTR);
	attachSystemFunction( Print,				NOATTR);
	attachSystemFunction( Range,				NOATTR);
	attachSystemFunction( Rational,				NOATTR);
	attachSystemFunction( ReadLine,				NOATTR);
	attachSystemFunction( Replace,				NOATTR);
	attachSystemFunction( Sort,				NOATTR);
	attachSystemFunction( SymbolTable,			NOATTR);
	attachSystemFunction( Tensor,				NOATTR);
	attachSystemFunction( TensorData,			NOATTR);
	attachSystemFunction( Transpose,			NOATTR);
	attachSystemFunction( Type,				NOATTR);
	attachSystemFunction( Unique,				NOATTR);
	attachSystemFunction( Unquote,				NOATTR);
	attachSystemFunction( Write,				NOATTR);

	/* math funcntions */
	attachSystemFunction( Abs,				LISTABLE);
	attachSystemFunction( Add,				NOATTR);
	attachSystemFunction( ArcCos,				LISTABLE);
	attachSystemFunction( ArcCosh,				LISTABLE);
	attachSystemFunction( ArcSin,				LISTABLE);
	attachSystemFunction( ArcSinh,				LISTABLE);
	attachSystemFunction( ArcTan,				LISTABLE);
	attachSystemFunction( ArcTanh,				LISTABLE);
	attachSystemFunction( Ceiling,				LISTABLE);
	attachSystemFunction( Cos,				LISTABLE);
	attachSystemFunction( Cosh,				LISTABLE);
	attachSystemFunction( Erf,				LISTABLE);
	attachSystemFunction( Erfc,				LISTABLE);
	attachSystemFunction( Exp,				LISTABLE);
	attachSystemFunction( Floor,				LISTABLE);
	attachSystemFunction( Gamma,				LISTABLE);
	attachSystemFunction( GCD,				NOATTR);
	attachSystemFunction( Log,				LISTABLE);
	attachSystemFunction( Log2,				LISTABLE);
	attachSystemFunction( Log10,				LISTABLE);
	attachSystemFunction( Multiply,				NOATTR);
	attachSystemFunction( Power,				NOATTR);
	attachSystemFunction( Remainder,			NOATTR);
	attachSystemFunction( Round,				LISTABLE);
	attachSystemFunction( Sign,				LISTABLE);
	attachSystemFunction( Sin,				LISTABLE);
	attachSystemFunction( Sinh,				LISTABLE);
	attachSystemFunction( Tan,				LISTABLE);
	attachSystemFunction( Tanh,				LISTABLE);
	attachSystemFunction( Truncate,				LISTABLE);


	attachSystemFunction( And,				QUOTEREST);
	attachSystemFunction( Block,				QUOTEALL);
	attachSystemFunction( Clear,				QUOTEALL);
	attachSystemFunction( If,				QUOTEREST);
	attachSystemFunction( Let,				QUOTEALL);
	attachSystemFunction( Loop, 				QUOTEALL);
	attachSystemFunction( OptimizeExpression,		QUOTEALL);
	attachSystemFunction( Or,				QUOTEREST);
	attachSystemFunction( Quote,				QUOTEFIRST);
	attachSystemFunction( Sequence,				QUOTEALL);
	attachSystemFunction( Set,				QUOTEFIRST);
	attachSystemFunction( SetGlobal,			QUOTEFIRST);
	attachSystemFunction( Table,				QUOTEALL);
	attachSystemFunction( Timing,				QUOTEFIRST);
	attachSystemFunction( Which,				QUOTEALL);


	/* sInterpretedFunction is external */
	overwriteSystemFunction( InterpretedFunction,		QUOTEALL);

	/* sFunction does not have a pfFunction */
	sFunction			= ue_mk_Symbol( "Function");
	sfFunction = ue_mk_SystemFunction(sFunction, pfInterpretedFunction);
	ue_add_FunctionAttribute(sfFunction, QUOTEALL);
	(defineVariableValue(sSystemFrame, sFunction, sfFunction, ue_system_env));

}
