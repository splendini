%{
#include <stdio.h>
#include <stdlib.h>
#include "uex.h"
#include "uex_language.h"

#define YYSTYPE ue_p

/* changes for felx/bison need make clean; make */
/* bison -v produces *.output with conflict details */

/* stuff from flex that bison needs to know about: */
int yylex();
int yyparse();

extern FILE *yyin;

static ue_p parseValue;

extern bool isEOF;
extern int lineNumber;
extern int errorLineCol;
extern bool endParsingOnNewLine;

void yyerror(const char *s);
%}

%token TINTEGER 
%token TREAL 
%token TSTRING 
%token TSYMBOL 
%token '(' ')' '{' '}' '[' ']'
%token ',' ';' 
%token TJUNK

%left ';'
%right '=' 
%left TOR TAND '!'
%left TISNSAME TISSAME
%left TLEQ '<'
%left TGEQ '>'
%left TNEQUAL TEQUAL
%left '+' 
%left '*' 
%right '^' 
%nonassoc '(' '[' /* prevent shift/reduce conflicts */

%start input 

%%

input: ue 						{	parseValue = $1; }
	| /* empty */				{	parseValue = sNull; }
	| error						{	parseValue = sFailed;}
	;

ue	: TINTEGER                   	
    | TREAL                      	
    | TSYMBOL                    	
    | TSTRING                    	
	| ue '(' list ')'			{	ue_set_LumpType($3, $1); $$ = $3; }
	| '(' ue ')'				{	$$ = $2; }
	| '{' list '}'				{	$$ = $2; }
	| ue '[' ue ']'				{	$$ = ue_mk_Lump(sPart, 2, $1, $3); }
	| ue '^' ue					{	$$ = ue_mk_Lump(sPower, 2, $1, $3); }
	| ue '*' ue					{	$$ = ue_mk_Lump(sMultiply, 2, $1, $3); }
	| ue '+' ue					{	$$ = ue_mk_Lump(sAdd, 2, $1, $3); }
	| ue TEQUAL ue				{	$$ = ue_mk_Lump(sEqual, 2, $1, $3); }
	| ue TNEQUAL ue				{	$$ = ue_mk_Lump(sNotEqual, 2, $1, $3); }
	| ue '>' ue					{	$$ = ue_mk_Lump(sGreater, 2, $1, $3); }
	| ue TGEQ ue				{	$$ = ue_mk_Lump(sGreaterEqual, 2, $1, $3); }
	| ue '<' ue					{	$$ = ue_mk_Lump(sLess, 2, $1, $3); }
	| ue TLEQ ue				{	$$ = ue_mk_Lump(sLessEqual, 2, $1, $3); }
	| ue TISSAME ue				{	$$ = ue_mk_Lump(sIsSame, 2, $1, $3); }
	| ue TISNSAME ue			{	$$ = ue_mk_Lump(sIsNotSame, 2, $1, $3); }
	| '!' ue 					{	$$ = ue_mk_Lump(sNot, 1, $2); }
	| ue TAND ue				{	$$ = ue_mk_Lump(sAnd, 2, $1, $3); }
	| ue TOR ue					{	$$ = ue_mk_Lump(sOr, 2, $1, $3); }
	| ue '=' ue					{	$$ = ue_mk_Lump(sSet, 2, $1, $3); }
	| ue ';' ue					{	$$ = ue_mk_Lump(sSequence, 2, $1, $3); }
	| ue ';' 					{	$$ = ue_mk_Lump(sSequence, 2, $1, sNull); }
    ;

list: /* empty */				{	$$ = ue_mk_Lump(sList, 0); }
	| member
	;

member: ue						{	$$ = ue_mk_Lump(sList, 1, $1); }
	| member ',' ue				{	$$ = ue_LumpAppendTo($1, $3); }
	;

%%

void yyerror(const char *s) {
	printf("%s:\n on line %d in column %d.\n", s, lineNumber, errorLineCol);
	errorLineCol = 0;
}

ue_p ue_Parse( FILE *file, bool eatNewLine) {
	int err;
	unsigned int oldLN;

	oldLN = lineNumber;
	if (file) {
		lineNumber = 1;
		yyin = file;
	} else /* error */ {
		printf("ue_Parse: file not valid.");
		return sNull;
	}

	endParsingOnNewLine = !eatNewLine;

	err = yyparse();
	lineNumber = oldLN;
	if (err != 0) {
		return sNull;
	} else {
		return parseValue;
	}
}
