
#include "uex.h"
#include "uex_language.h"
#include "evaluator.h"
#include "environment.h"
#include "special_forms.h"

ue_p	ue_system_env;
ue_p	ue_global_env;

void ue_init_language() {
	/* environment needs to be done before special forms */
	ue_init_environment();
	ue_init_evaluator();
	ue_init_special_forms();
	ue_global_env = extendEnvironment(sGlobalFrame, sEmptyList, sEmptyList, ue_system_env);
}

