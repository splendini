
#include "environment.h"
#include "evaluator.h"
#include "part.h"

void
part_specification_analysis(ue_p part, part_spec_type_t *spectype, ue_p *spec) {
	size_t len, i;
	ue_p ui, temp;

	*spec = sFailed;
	*spectype = NOTVALID;

	assert(ue_is_Lump(part));

	len = ue_get_LumpLength(part);
	if (len < 2) return;

	ui = ue_get_LumpElem(part, 2);

	/* scattered */
	if (ue_is_List(ui) && len == 2) {
		ui = ue_mk_TensorFromList(ui);
	}
	if (ue_is_Tensor(ui) && (ue_get_TensorRank(ui) == 2) &&\
		 (ue_get_TensorType(ui) == MI)) {
		*spectype = SCATTERED;
		*spec = ui;
		return;
	}

	/* single */
	if (ue_is_Number(ui)) {
		*spectype = SINGLE;
		ui = ue_mk_LumpShell(sList, len - 1);
		for (i = 2; i <= len; i++) {
			temp = ue_get_LumpElem(part, i);
			if (!ue_is_Number(temp)) {
				*spectype = NOTVALID;
				break;
			}
			ue_set_LumpElem(ui, i - 1, temp);
		}
		if (*spectype == SINGLE) {
			ui = ue_mk_TensorFromList(ui);
			if (ue_is_Tensor(ui) && (ue_get_TensorRank(ui) == 1) &&\
				 (ue_get_TensorType(ui) == MI)) {
				*spectype = SINGLE;
				*spec = ui;
				return;
			}
		}
	}

	/* structured */
	ui = ue_mk_LumpShell(sList, len - 1);
	for (i = 2; i <= len; i++) {
		temp = ue_get_LumpElem(part, i);
		if (ue_is_Tensor(temp) || ue_is_List(temp) ||
				(temp == sAll) || ue_is_Number(temp)) {
			*spectype = STRUCTURED;
		} else {
			*spectype = NOTVALID;
			break;
		}
		ue_set_LumpElem(ui, i - 1, temp);
	}
	*spec = ui;

}


/*
static ue_p part_lump(ue_p u, size_t from, size_t to, size_t steps) {
	size_t len, i;
	ue_p res;

	assert(ue_is_Lump(u));

	len = ue_get_LumpLength(u);
	if ((from == to) && (to <= len))
			return ue_cp(ue_get_LumpElem(u, from));

	if ((from < to) && (to <= len)) {
		res = ue_get_LumpType(u);
		len = to - from + 1;
		res = ue_mk_LumpShell(res, len);
		for (i = 1; i <= len; i++) {
			ue_set_LumpElem(res, i, ue_get_LumpElem(u, from + i - 1));
		}
		return res;
	}

	return sFailed;
}
*/

static ue_p ue_set_part_lump(const ue_p list, const ue_p val) {
	size_t i;
	long mi;
	ue_p u, pos;

	u = ue_get_LumpElem(list, 1);
	pos = ue_get_LumpElem(list, 2);

	assert(ue_is_mi(pos));

	mi = ue_get_mi(pos);
	i = ue_get_LumpLength(u);
	assert(i < (size_t) MI_MAX);

	if (mi < 0) {
		mi = ((long) i) + mi + 1;
	}

	if ((mi < 0) || (mi > (long) i)) {
		return sFailed;
	}
	assert(mi >= 0);
	ue_set_LumpElem(u, (size_t) mi, val);

	return val;
}

static ue_p ue_set_part_tensor(const ue_p part, const ue_p v) {
	ue_p spec, part1;
	part_spec_type_t spectype;
	ue_p res = sFailed;

	part1 = ue_get_LumpElem(part, 1);

	assert(ue_is_Tensor(part1));

	part_specification_analysis(part, &spectype, &spec);

	if (spectype == SINGLE && ue_is_Number(v)) {
		res = ue_set_part_single(&part1, ue_get_Tensor(spec), ue_get_Number(v));
	} else if (spectype == SCATTERED) {
		res = ue_set_part_scattered(&part1, ue_get_Tensor(spec), v);
	} else if (spectype == STRUCTURED) {
		res = ue_set_part_structured(&part1, spec, v);
	}

	if (res != sFailed) {
		return v;
	} else {
		return res;
	}
}


ue_p set_part(const ue_p part, const ue_p val, ue_p env) {
	size_t i, len;
	ue_p var, varval, tmp, list;

	if (!ue_is_Lump(part))
		return sFailed;

	len = ue_get_LumpLength(part);
	if ((ue_get_LumpLength(part) < 2))
		return sFailed;

	list = ue_mk_LumpShell(sList, len);

	var = ue_get_LumpElem(part, 1);
	if (!ue_is_Symbol(var)) {
		return sFailed;
	}

	varval = lookupVariableValue(var, env);
	ue_set_LumpElem(list, 1, varval);
	for (i = 2; i <= len; i++) {
		/* Part(Tensor, {number}, {2,3}) we want List(number)
		*  to evaluate to List(1) for example */
		tmp = ue_Eval(ue_get_LumpElem(part, i), env);
		ue_set_LumpElem(list, i, tmp);
	}

	switch(ue_get_type(varval)) {
		case SYMBOL:
		case STRING:
		case NUMBER:
		case FUNCTION:
		case DATA:			return sFailed;

		case TENSOR:		return ue_set_part_tensor(list, val);
		case LUMP:			return ue_set_part_lump(list, val);

		default:
			noCase("set_part");
			return sFailed;
	}
}

