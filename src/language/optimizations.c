#include "uex.h"
#include "environment.h"

ue_p ue_InlineFunction( const ue_p u, ue_p env) {
	size_t i;
	ue_p type, result, tmp;

	switch ( ue_get_type(u)) {
		case STRING:
		case NUMBER:
		case TENSOR:
		case FUNCTION:
		case DATA:
			return u;

		case SYMBOL: {
			tmp = systemFunctionLookup(u, env);
			return tmp;
		}

		case LUMP: {
			size_t len = ue_get_LumpLength(u);
			type = ue_InlineFunction(ue_get_Type(u), env);
			result = ue_mk_LumpShell(type, len);
			for (i = 1; i <= len; i++) {
				tmp = ue_InlineFunction(ue_get_LumpElem(u, i), env);
				ue_set_LumpElem(result, i, tmp);
			}
			return result;
		}

		default:
			printf("\nue_InlineFunction: unkown expression type.\n");
            return sFailed;

	}

}

