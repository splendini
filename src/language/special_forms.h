#ifndef SPECIAL_FORMS_H 
#define SPECIAL_FORMS_H

/* pure symbols */
extern ue_p sDynamicScope;
extern ue_p sEndOfFile;
extern ue_p sFalse;
extern ue_p sListable;
extern ue_p sNone;
extern ue_p sQuoteAll;
extern ue_p sQuoteFirst;
extern ue_p sQuoteRest;
extern ue_p sQuoteAllCoercion;
extern ue_p sQuoteFirstCoercion;
extern ue_p sQuoteRestCoercion;
extern ue_p sStaticScope;
extern ue_p sTrue;
extern ue_p sVariadic;


#define initExternSymbolAndSystemFunction(name) extern ue_p s##name, sf##name

/* symbols and related system functions */
initExternSymbolAndSystemFunction( And);
initExternSymbolAndSystemFunction( Apply);
initExternSymbolAndSystemFunction( Attributes);
initExternSymbolAndSystemFunction( AttributesAdd);
initExternSymbolAndSystemFunction( AttributesRemove);
initExternSymbolAndSystemFunction( Block);
initExternSymbolAndSystemFunction( ByteCount);
initExternSymbolAndSystemFunction( Clear);
initExternSymbolAndSystemFunction( Close);
initExternSymbolAndSystemFunction( Coerce);
initExternSymbolAndSystemFunction( Compare);
initExternSymbolAndSystemFunction( Context);
initExternSymbolAndSystemFunction( ConstantArray);
initExternSymbolAndSystemFunction( Environment);
initExternSymbolAndSystemFunction( Equal);
initExternSymbolAndSystemFunction( Evaluate);
initExternSymbolAndSystemFunction( Exit);
initExternSymbolAndSystemFunction( Function);
initExternSymbolAndSystemFunction( Greater);
initExternSymbolAndSystemFunction( GreaterEqual);
initExternSymbolAndSystemFunction( Hash);
initExternSymbolAndSystemFunction( If);
initExternSymbolAndSystemFunction( IsEven);
initExternSymbolAndSystemFunction( IsInterpretedFunction);
initExternSymbolAndSystemFunction( IsList);
initExternSymbolAndSystemFunction( IsLump);
initExternSymbolAndSystemFunction( IsNotSame);
initExternSymbolAndSystemFunction( IsNumber);
initExternSymbolAndSystemFunction( IsNumberExact);
initExternSymbolAndSystemFunction( IsNumberInexact);
initExternSymbolAndSystemFunction( IsOdd);
initExternSymbolAndSystemFunction( IsSame);
initExternSymbolAndSystemFunction( IsStream);
initExternSymbolAndSystemFunction( IsString);
initExternSymbolAndSystemFunction( IsSymbol);
initExternSymbolAndSystemFunction( IsTensor);
initExternSymbolAndSystemFunction( IsTensorExact);
initExternSymbolAndSystemFunction( IsTensorInexact);
initExternSymbolAndSystemFunction( Join);
initExternSymbolAndSystemFunction( Length);
initExternSymbolAndSystemFunction( Less);
initExternSymbolAndSystemFunction( LessEqual);
initExternSymbolAndSystemFunction( Let);
initExternSymbolAndSystemFunction( LibraryFunction);
initExternSymbolAndSystemFunction( Load);
initExternSymbolAndSystemFunction( LoadLibraryFunction);
initExternSymbolAndSystemFunction( LookupData);
initExternSymbolAndSystemFunction( LookupDataAdd);
initExternSymbolAndSystemFunction( LookupDataDefaultValue);
initExternSymbolAndSystemFunction( LookupDataFind);
initExternSymbolAndSystemFunction( LookupDataKeyValues);
initExternSymbolAndSystemFunction( Loop);
initExternSymbolAndSystemFunction( Map);
initExternSymbolAndSystemFunction( MemoryInUse);
initExternSymbolAndSystemFunction( Not);
initExternSymbolAndSystemFunction( NotEqual);
initExternSymbolAndSystemFunction( OpenAppend);
initExternSymbolAndSystemFunction( OpenRead);
initExternSymbolAndSystemFunction( OpenWrite);
initExternSymbolAndSystemFunction( OptimizeExpression);
initExternSymbolAndSystemFunction( Order);
initExternSymbolAndSystemFunction( Outer);
initExternSymbolAndSystemFunction( Or);
initExternSymbolAndSystemFunction( Parse);
initExternSymbolAndSystemFunction( Part);
initExternSymbolAndSystemFunction( Print);
initExternSymbolAndSystemFunction( Quote);
initExternSymbolAndSystemFunction( Range);
initExternSymbolAndSystemFunction( ReadLine);
initExternSymbolAndSystemFunction( Replace);
initExternSymbolAndSystemFunction( Sequence);
initExternSymbolAndSystemFunction( Set);
initExternSymbolAndSystemFunction( SetGlobal);
initExternSymbolAndSystemFunction( Sort);
initExternSymbolAndSystemFunction( SymbolTable);
initExternSymbolAndSystemFunction( Table);
initExternSymbolAndSystemFunction( Tensor);
initExternSymbolAndSystemFunction( TensorData);
initExternSymbolAndSystemFunction( Timing);
initExternSymbolAndSystemFunction( Transpose);
initExternSymbolAndSystemFunction( Type);
initExternSymbolAndSystemFunction( Unique);
initExternSymbolAndSystemFunction( Unquote);
initExternSymbolAndSystemFunction( Which);
initExternSymbolAndSystemFunction( Write);


/* math functions */
initExternSymbolAndSystemFunction( Abs);
initExternSymbolAndSystemFunction( ArcCos);
initExternSymbolAndSystemFunction( ArcCosh);
initExternSymbolAndSystemFunction( ArcSin);
initExternSymbolAndSystemFunction( ArcSinh);
initExternSymbolAndSystemFunction( ArcTan);
initExternSymbolAndSystemFunction( ArcTanh);
initExternSymbolAndSystemFunction( Ceiling);
initExternSymbolAndSystemFunction( Cos);
initExternSymbolAndSystemFunction( Cosh);
initExternSymbolAndSystemFunction( Erf);
initExternSymbolAndSystemFunction( Erfc);
initExternSymbolAndSystemFunction( Exp);
initExternSymbolAndSystemFunction( Floor);
initExternSymbolAndSystemFunction( Gamma);
initExternSymbolAndSystemFunction( Log);
initExternSymbolAndSystemFunction( Log2);
initExternSymbolAndSystemFunction( Log10);
initExternSymbolAndSystemFunction( Remainder);
initExternSymbolAndSystemFunction( Round);
initExternSymbolAndSystemFunction( Sign);
initExternSymbolAndSystemFunction( Sin);
initExternSymbolAndSystemFunction( Sinh);
initExternSymbolAndSystemFunction( Tan);
initExternSymbolAndSystemFunction( Tanh);
initExternSymbolAndSystemFunction( Truncate);


#define getPureFunctionParameters(f)	(assert(ue_is_PureFunction(f)),\
										ue_get_LumpElem(ue_get_FunctionUex(f), 1))

#define getPureFunctionBody(f)			(assert(ue_is_PureFunction(f)),\
										ue_get_LumpElem(ue_get_FunctionUex(f),2))

#define getPureFunctionScope(f)			(assert(ue_is_PureFunction(f)),\
										ue_get_LumpElem(ue_get_FunctionUex(f),3))

#define getPureFunctionFrameName(f)		(assert(ue_is_PureFunction(f)),\
										ue_get_LumpElem(ue_get_FunctionUex(f),4))

void	ue_init_special_forms	( );

#endif
