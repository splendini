
#ifndef UEX_LANGUAGE_H
#define UEX_LANGUAGE_H

#include "evaluator.h"
#include "special_forms.h"

extern ue_p	ue_system_env;
extern ue_p	ue_global_env;

ue_p	ue_Parse			(FILE *file, bool eatNewLine);
int		ylex				(); 
int		yyparse				(); 
void	ue_init_language	();

#endif

