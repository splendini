#ifndef PART_H 
#define PART_H

#include "uex.h"

typedef enum {
	NOTVALID		=	0, 
	SCATTERED		=	1,
	SINGLE			=	2,
	STRUCTURED		=	3
} part_spec_type_t;

void
part_specification_analysis(ue_p part, part_spec_type_t *spectype, ue_p *spec);

ue_p set_part(const ue_p part, const ue_p val, ue_p env);

#endif
