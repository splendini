
#ifndef EVALUATOR_H
#define EVALUATOR_H

extern ue_p sState;

/* ue_mk_State should not be used outisde; if an additional
 * field, like a continuation, is added then all calls to ue_mk_State
 * will need to be updated */
#define ue_mk_State(u, env)			(ue_mk_Lump(sState, 2, u, env))
#define ue_is_State(u)				(ue_is_Lump(u) && (ue_get_LumpType(u) == sState))
#define ue_get_State_ue(s)			(ue_get_LumpElem(s, 1))
#define ue_get_State_env(s)			(ue_get_LumpElem(s, 2))
#define ue_State_update_ue(s, u)	(ue_mk_State(u, ue_get_LumpElem(s, 2)))
#define ue_State_update_env(s, env)	(ue_mk_State(ue_get_LumpElem(s, 1), env))

ue_p ue_Eval	(const ue_p u, ue_p env);

void ue_init_evaluator	();

#endif

