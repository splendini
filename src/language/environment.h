#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "uex.h"

extern ue_p sFrame;
extern ue_p sSystemFrame;
extern ue_p sGlobalFrame;
extern ue_p sCleared;

#define emptyEnvironment			sEmptyList
#define ue_mk_Environment()			emptyEnvironment
#define ue_is_Environment(env)		ue_is_List(env)
#define isEmptyEnvironment(env)		ue_is_EmptyList(env)
#define enclosingEnvironment(env)	cdr(env)
#define firstFrame(env)				car(env)

#define mkFrame(name, vars, vals)	ue_mk_Lump(name, 2, vars, vals)
#define frameVariables(frame)		ue_get_LumpElem(frame, 1)
#define frameValues(frame)			ue_get_LumpElem(frame, 2)
#define frameContext(frame)			ue_get_LumpType(frame)

ue_p	extendEnvironment	(ue_p name, ue_p vars, ue_p vals, ue_p baseEnv);

ue_p	defineVariableValue	(ue_p context, ue_p var, ue_p val, ue_p env);
ue_p	setVariableValue	(ue_p var, ue_p val, ue_p env);

bool	isVariableDefined	(ue_p var, ue_p env); 
ue_p	lookupVariableValue	(ue_p var, ue_p env);
ue_p	clearVariable		(ue_p var, ue_p env);

ue_p	systemFunctionLookup(ue_p var, ue_p env);

ue_p	shadowVariableValue	(ue_p vars, ue_p vals, ue_p env);
void	unshadowVariableValue(ue_p vars, ue_p env);


/* 
 * if ue_mk_Symbol is called with a string it has seen before
 * then the reference to the corresponding symbol is returend.
 * */

#define stringify(a) #a

#define addSystemVariable(s, v)		do {\
					ue_p funSym = ue_mk_Symbol(s);\
					(defineVariableValue( sSystemFrame, funSym, v, ue_system_env));\
					} while (0)

#define attachSystemFunction(name, a)		do {\
					s##name = ue_mk_Symbol(stringify(name));\
					sf##name = ue_mk_SystemFunction(s##name, &pf##name);\
					ue_add_FunctionAttribute(sf##name, a);\
					(defineVariableValue( sSystemFrame, s##name, sf##name, ue_system_env));\
					} while (0)

#define overwriteSystemFunction(name, a)		do {\
					sf##name = s##name;\
					s##name = ue_mk_SystemFunction(sf##name, &pf##name);\
					ue_add_FunctionAttribute(s##name, a);\
					(defineVariableValue( sSystemFrame, sf##name, s##name, ue_system_env));\
					} while (0)


void ue_init_environment     ();

#endif

