
#include "environment.h"
#include "uex_language.h"

ue_p sFrame;
ue_p sSystemFrame;
ue_p sGlobalFrame;
ue_p sCleared;


static
void addBindingToFrame_cons( ue_p var, ue_p val, ue_p frame) {
	ue_p tmp;

	tmp = cons(var, frameVariables(frame));
	ue_set_LumpElem(frame, 1, tmp);

	tmp = cons(val, frameValues(frame));
	ue_set_LumpElem(frame, 2, tmp);
}

static
void addBindingToFrame( ue_p var, ue_p val, ue_p frame) {
	addBindingToFrame_cons(var, val, frame);
}

static
ue_p extendEnvironment_cons(ue_p name, ue_p vars, ue_p vals, ue_p baseEnv) {
	ue_p tmp;

	assert(ue_is_Lump(vars));
	assert(ue_is_Lump(vals));
	assert(ue_get_LumpLength(vars) == ue_get_LumpLength(vals));

	tmp = mkFrame(name, vars, vals);
	tmp = cons(tmp, baseEnv);
	return tmp;
}

extern
ue_p systemFunctionLookup( ue_p var, ue_p env) {
	ue_p frame, vars, vals, tmp;

	if (!ue_is_Symbol(var))
		return var;

	while (!isEmptyEnvironment(env)) {
		frame = firstFrame(env);
		if (frameContext(frame) == sSystemFrame) {;
			vars = frameVariables(frame);
			vals = frameValues(frame);
			while (!ue_is_EmptyList(vars)) {
				tmp = car(vars);
				if (ue_is_SameSymbol(var, tmp)) {
					tmp = car(vals);
					return tmp;
				}
				vars = cdr(vars);
				vals = cdr(vals);
			}
			if (ue_is_EmptyList(vars)) {
				return var;
			}
		}
		env = enclosingEnvironment(env);
	}
	return var;
}

static
ue_p lookupVariableValue_cons( ue_p var, ue_p env) {
	ue_p frame, vars, vals, tmp;

	if (!ue_is_Symbol(var))
		return var;

	while (!isEmptyEnvironment(env)) {
		frame = firstFrame(env);
		vars = frameVariables(frame);
		vals = frameValues(frame);
		while (!ue_is_EmptyList(vars)) {
			tmp = car(vars);
			if (ue_is_SameSymbol(var, tmp)) {
				tmp = car(vals);
				return tmp;
			}
			vars = cdr(vars);
			vals = cdr(vals);
		}
		env = enclosingEnvironment(env);
	}
	return var;
}

static
ue_p setVariableValue_cons(ue_p var, ue_p val, ue_p env) {
	ue_p frame, vars, vals, tmp;

	if (!ue_is_Symbol(var))
		return sFailed;

	while (!isEmptyEnvironment(env)) {
		frame = firstFrame(env);
		vars = frameVariables(frame);
		vals = frameValues(frame);

		while (!ue_is_EmptyList(vars)) {
			tmp = car(vars);
			if (ue_is_SameSymbol(var, tmp)) {
				ue_set_LumpElem(vals, 1, val);
				return val;
			}
			vars = cdr(vars);
			vals = cdr(vals);
		}
		env = enclosingEnvironment(env);
	}

	/* set does not add a binding if it does not find the variable */
	return sFailed;
}

/* define convertes a symbol to a variable */
static inline
ue_p defineVariableValueBase_cons(ue_p var, ue_p val, ue_p env) {
	ue_p frame, vars, vals, tmp;

	if (!ue_is_Symbol(var))
		return sFailed;

	frame = firstFrame(env);
	vars = frameVariables(frame);
	vals = frameValues(frame);

	while (!ue_is_EmptyList(vars)) {
		tmp = car(vars);
		if (ue_is_SameSymbol(var, tmp)) {
			ue_set_LumpElem(vals, 1, val);
			return val;
		}
		vars = cdr(vars);
		vals = cdr(vals);
	}

	addBindingToFrame(var, val, frame);
	return val;
}

static
ue_p defineVariableValue_cons(ue_p context, ue_p var, ue_p val, ue_p env) {
	ue_p frame;

	if (!ue_is_Symbol(var))
		return sFailed;

	while (!isEmptyEnvironment(env)) {
		frame = firstFrame(env);

		if (frameContext(frame) == context) {
			return defineVariableValueBase_cons(var, val, env);
		}

		env = enclosingEnvironment(env);
	}

	return sFailed;
}

static
ue_p clearVariableValue_cons(ue_p var, ue_p env) {
	ue_p frame, vars, vals, tmp;

	if (!ue_is_Symbol(var))
		return sFailed;

	frame = firstFrame(env);
	vars = frameVariables(frame);
	vals = frameValues(frame);

	while (!ue_is_EmptyList(vars)) {
		tmp = car(vars);
		if (ue_is_SameSymbol(var, tmp)) {
			ue_set_LumpElem(vars, 1, sCleared);
			ue_set_LumpElem(vals, 1, sCleared);
			return sNull;
		}
		vars = cdr(vars);
		vals = cdr(vals);
	}

	return sNull;
}

extern
ue_p extendEnvironment(ue_p name, ue_p vars, ue_p vals, ue_p baseEnv) {
	return extendEnvironment_cons(name, vars, vals, baseEnv);
}

extern
ue_p lookupVariableValue( ue_p var, ue_p env) {
	return lookupVariableValue_cons( var, env);
} 

extern
ue_p defineVariableValue(ue_p fname, ue_p var, ue_p val, ue_p env) {
	return defineVariableValue_cons(fname, var, val, env);
}

static
ue_p clearVariableValue(ue_p var, ue_p env) {
	return clearVariableValue_cons(var, env);
}

extern
ue_p clearVariable(ue_p var, ue_p env) {
	if (ue_is_Symbol(var)) {
		ue_set_SymbolAttributes(var, NOATTR);
		return clearVariableValue(var, env);
	}
	/*TODO: message*/
	return sNull;
}

extern
ue_p setVariableValue(ue_p var, ue_p val, ue_p env) {
	return setVariableValue_cons(var, val, env);
}

extern
bool isVariableDefined(ue_p var, ue_p env) {
	return lookupVariableValue(var, env) != var;
}

extern
ue_p shadowVariableValue( ue_p svars, ue_p svals, ue_p env) {
	size_t i, len;
	ue_p frame, vars, vals;
	ue_p thisVar, thisVal;
	ue_p oldvals, oldvars;
	ue_p shadow;

	assert(ue_is_Lump(svars));
	assert(ue_is_Lump(svals));
	assert(ue_is_Environment(env));

	frame = firstFrame(env);

	len = ue_get_LumpLength(svars);
	assert(len == ue_get_LumpLength(svals));

	oldvals = sEmptyList;
	oldvars = sEmptyList;

	for (i = 1; i <= len; i++) {
		vars = frameVariables(frame);
		vals = frameValues(frame);

		thisVar = ue_get_LumpElem(svars, i);
		thisVal = ue_get_LumpElem(svals, i);

		oldvars = cons(thisVar, oldvars);

		while (!ue_is_EmptyList(vars)) {
			if (ue_is_SameSymbol(car(vars), thisVar)) {
				oldvals = cons(car(vals), oldvals);
				break;
			} else {
				vars = cdr(vars);
				vals = cdr(vals);
			}
		}

		if (ue_is_EmptyList(vars)) {
			oldvals = cons(sCleared, oldvals);
		}

		addBindingToFrame(thisVar, thisVal, frame);
	}

	shadow = ue_mk_Lump(sList, 2, oldvars, oldvals);

	return shadow;
}

extern
void unshadowVariableValue(ue_p shadowed, ue_p env) {
	ue_p var, val;
	ue_p oldvars, oldvals;

	assert(ue_is_Environment(env));
	assert(ue_is_Lump(shadowed) && ue_length(shadowed) == 2);

	oldvars = ue_get_LumpElem(shadowed, 1);
	oldvals = ue_get_LumpElem(shadowed, 2);

	while (!ue_is_EmptyList(oldvals)) {
		var = car(oldvars);
		val = car(oldvals);
		if (ue_is_Same(val, sCleared)) {
			/* var was not in the frame when it was shadowed so we clear it */
 			(void) clearVariableValue(var, env);
		} else {
			/* reinstantiate a value that was shadowed */
			defineVariableValueBase_cons(var, val, env);
		}
		oldvars = cdr(oldvars);
		oldvals = cdr(oldvals);
	}
}

extern
void ue_init_environment() {

	sFrame = ue_mk_Symbol("Frame");
	sSystemFrame = ue_mk_Symbol("SystemFrame");
	sGlobalFrame = ue_mk_Symbol("GlobalFrame");

	sCleared = ue_mk_Symbol("$Cleared");

	/* create the empty system environment */
	ue_system_env = extendEnvironment(sSystemFrame, sEmptyList, sEmptyList, sEmptyList);
}

