
#include "bignum.h"
#include "utils.h"

static
void * gmp_gc_malloc(size_t alloc_size) {
	return ue_Malloc(alloc_size);
}

static
void * gmp_gc_realloc(void *ptr, /*@unused@*/ size_t old_size, size_t new_size) {
	return ue_Realloc(ptr, new_size);
}

static
void gmp_gc_free(void *ptr, /*@unused@*/ size_t size) {
	ue_Free(ptr);
}

static
void init_gmp_memory_function() {
	mp_set_memory_functions(gmp_gc_malloc, gmp_gc_realloc, gmp_gc_free);
}

#define MINMAX(fun, CMPOP)\
void \
bnz_##fun(mpz_t rop, const mpz_t op1, const mpz_t op2) {\
	if (bnz_cmp(op1, op2) CMPOP 0 ) {\
		*rop = *op1;\
	} else {\
		*rop = *op2;\
	}\
}\

MINMAX(min, <)
MINMAX(max, >)

extern
void ue_init_bignum() {
	init_gmp_memory_function();
	ue_init_memory_function();
}
