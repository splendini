
#include "basenum.h"

int cmp_bi_br(const bi_t a, const br_t b) {
	br_t tmp;
	bnf_init_set_z(tmp, a);
	return bnf_cmp(tmp, b);
}

int cmp_br_bi(const br_t a, const bi_t b) {
	br_t tmp;
	bnf_init_set_z(tmp, b);
	return bnf_cmp(a, tmp);
}

/*@-fixedformalarray@*/
extern
void add_mi_bi(bi_t r, const mi_t a, const bi_t b) {
	bnz_init_set_si(r, a);
	bnz_add(r, r, b);
}

extern
void add_mi_br(br_t r, const mi_t a, const br_t b) {
	bnf_init_set_si(r, a);
	bnf_add(r, r, b);
}

extern
void add_bi_br(br_t r, const bi_t a, const br_t b) {
	bnf_init_set_z(r, a);
	bnf_add(r, r, b);
}

extern 
mi_t mi_gcd(const mi_t a, const mi_t b) {
	mi_t u, v, r;
	abs_mi_mi(u, a);
	abs_mi_mi(v, b);
    while ( v != 0) {
        r = u % v;
        u = v;
        v = r;
    }
    return u;
}

extern 
void gcd_bi_mi(bi_t r, const bi_t a, const mi_t b) {
	mi_t mn;

	abs_mi_mi(mn, b);
	assert(mn >= 0);

	bnz_init(r);
	(void) bnz_gcd_ui(r, a, (unsigned long int) mn);
}



extern
void mul_mi_bi(bi_t r, const mi_t a, const bi_t b) {
	bnz_init_set_si(r, a);
	bnz_mul(r, r, b);
}

extern
void mul_mi_br(br_t r, const mi_t a, const br_t b) {
	bnf_init_set_si(r, a);
	bnf_mul(r, r, b);
}

extern
void mul_bi_br(br_t r, const bi_t a, const br_t b) {
	bnf_init_set_z(r, a);
	bnf_mul(r, r, b);
}

void pow_mi_mi(bi_t r, const mi_t b, const mi_t e) {
	assert(e >= 0);
	bnz_init_set_si(r, b);
	bnz_pow_ui(r, r, e);
}

void pow_mi_br(br_t r, const mi_t b, const br_t e) {
	assert(b >= 0);
	bnf_init(r);
	bnf_ui_pow(r, b, e);
}


extern
void rem_mi_bi(bi_t r, const mi_t a, const bi_t b) {
	bnz_init_set_si(r, a);
	bnz_rem(r, r, b);
}

extern
void rem_mi_br(br_t r, const mi_t a, const br_t b) {
	bnf_init_set_si(r, a);
	bnf_rem(r, r, b);
}

extern
void rem_bi_mi(bi_t r, const bi_t a, const mi_t b) {
	bnz_init_set_si(r, b);
	bnz_rem(r, a, r);
}

extern
void rem_br_mi(br_t r, const br_t a, const mi_t b) {
	bnf_init_set_si(r, b);
	bnf_rem(r, a, r);
}

extern
void rem_bi_br(br_t r, const bi_t a, const br_t b) {
	bnf_init_set_z(r, a);
	bnf_rem(r, r, b);
}

extern
void rem_br_bi(br_t r, const br_t a, const bi_t b) {
	bnf_init_set_z(r, b);
	bnf_rem(r, a, r);
}

/*@=fixedformalarray@*/


