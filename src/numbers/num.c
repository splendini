
#include "num.h"

num_t num_not_valid;

num_t num_m_one_mi;
num_t num_zero_mi;
num_t num_p_one_mi;

num_t num_m_inf_mr;
num_t num_m_one_mr;
num_t num_zero_mr;
num_t num_p_one_mr;
num_t num_p_e_mr;
num_t num_p_pi_mr;
num_t num_p_inf_mr;

num_t num_nan_mr;

#define num_set_type_notype(e)	(((e).nt) = NOTYPE)
#define num_set_type_mi(e)		(((e).nt) = MI)
#define num_set_type_mr(e)		(((e).nt) = MR)
#define num_set_type_bi(e)		(((e).nt) = BI)
#define num_set_type_br(e)		(((e).nt) = BR)

/* http://stackoverflow.com/a/1514309 */
/* http://c-faq.com/misc/sd26.html */
/* http://stackoverflow.com/a/20956705 - try with gcc5 */
/* man fetestexcept */
/* https://www.cs.utah.edu/~regehr/papers/overflow12.pdf */
/*

#include <fenv.h>
#include "num.h"
#define add_is_overflow(a, b)   ((b > 0) && (a > MI_MAX - b))
#define add_is_underflow(a, b)  ((b < 0) && (a < MI_MIN - b))
#define add_is_ou_flow(a, b)    (add_is_overflow(a, b) || add_is_underflow(a, b))

void num_add_mi_mi2(num_t *r, const num_t n1, const num_t n2) {
    mi_t a, b;
    num_t res;
    a = num_get_mi(n1);
    b = num_get_mi(n2);
    res.nt = MI;
    num_set_mi(res, a + b);
    if (fetestexcept(FE_OVERFLOW | FE_UNDERFLOW)) {
        res = num_zero_mi;
    }
    *r = res;
}

void num_add_mi_mi1(num_t *r, const num_t n1, const num_t n2) {
    mi_t a, b;
    num_t res;
    a = num_get_mi(n1);
    b = num_get_mi(n2);
    if (!add_is_ou_flow(a, b)) {
        res.nt = MI;
        num_set_mi(res, a + b);
    } else {
        res = num_zero_mi;
    }
    *r = res;
}

int main () {
    num_t j = num_zero_mi;
    num_t one = num_p_one_mi;
    long i, b = 100000000;
    j = num_coerce(j, BI);
    one = num_coerce(one, BI);
    feclearexcept(FE_OVERFLOW | FE_UNDERFLOW);
    for (i = 0; i < b; i++) {
        num_add_mi_mi(&j, j, one);
        *//*num_add(&j, j, one);*//*
    }
    num_print_raw(stdout, j);
    printf("\n");
    return 0;
}

gcc -DNDEBUG -O2 test2.c num.c basenum.c -lgmp -lmpfr -lm

check NDEBUG works

time ./a.out

- much quicker without the if ou_flow
- __bultin_add_overflow about factor 3 faster
- big int about factor 1/2 slower
- fetestexcept(FE_OVERFLOW | FE_UNDERFLOW) seems slower than ou_flow (x2 times)

*/
/* ? ((a ^ b) >= 0) && ((s ^ b) < 0)) ? */
#define add_is_overflow(a, b)   ((b > 0) && (a > MI_MAX - b)) 
#define add_is_underflow(a, b)  ((b < 0) && (a < MI_MIN - b)) 
#define add_is_ou_flow(a, b)    (add_is_overflow(a, b) || add_is_underflow(a, b)) 

#define mul_is_overflow(a, b)   ((b > 0) && (a > MI_MAX / b)) 
/* crashes for a=1 and b=-1 ?*/
/*#define mul_is_underflow(a, b)  ((b < 0) && (a < MI_MIN / b))*/
#define mul_is_underflow(a, b)  ((b < 0) && (a > MI_MAX / -b)) 
#define mul_is_ou_flow(a, b)    (mul_is_overflow(a, b) || mul_is_underflow(a, b)) 


/* these need C99: */
num_t num_not_valid = {.nt = -1};

num_t num_m_one_mi = {.nt = MI, {.mit = -1}};
num_t num_zero_mi  = {.nt = MI, {.mit =  0}};
num_t num_p_one_mi = {.nt = MI, {.mit =  1}};

num_t num_m_inf_mr = {.nt = MR, {.mrt = -INFINITY}};
num_t num_m_one_mr = {.nt = MR, {.mrt = -1.}};
num_t num_zero_mr  = {.nt = MR, {.mrt =  0.}};
num_t num_p_one_mr = {.nt = MR, {.mrt =  1.}};
num_t num_p_e_mr   = {.nt = MR, {.mrt =  2.71828182845904523536}};
num_t num_p_pi_mr  = {.nt = MR, {.mrt =  3.14159265358979323846}};
num_t num_p_inf_mr = {.nt = MR, {.mrt =  INFINITY}};

bool num_is_zero(num_t n) {
	return (bool) (num_cmp(n, num_zero_mi) == 0);
}

/* The NAN macro is a GNU extension */
/*num_t num_nan_mr   = {.nt = MR, {.mrt = NAN}};*/
num_t num_nan_mr   = {.nt = MR, {.mrt = 0./0.}};


/* mpfr does not have a mpfr_const_e */

extern
num_t num_p_pi_br(void) {
	num_t res;
	num_set_type_br(res);
	(void) bnf_init(num_get_br(res));
	(void) bnf_pi(num_get_br(res));
	return res;
}



extern
num_t num_mk_mi_si(const long i) { 
	num_t res;
	num_set_type_mi(res);
	num_set_mi(res, i);
	return res;
}

extern
num_t num_mk_mr_d(const double d) { 
	num_t res;
	num_set_type_mr(res);
	num_set_mr(res, d);
	return res;
}

extern
num_t num_mk_bi_str(char const *str) {
	num_t res;
	num_set_type_bi(res);
	(void) bnz_init_set_str(num_get_bi(res), str, 10);
	return res;
}

static
num_t num_mk_bi_si(const long a) {
	num_t res;
	num_set_type_bi(res);
	bnz_init_set_si(num_get_bi(res), a);
	return res;
}

extern
num_t num_mk_bi_bi(const bi_t a) {
	num_t res;
	num_set_type_bi(res);
	bnz_init_set(num_get_bi(res), a);
	return res;
}

extern
num_t num_mk_br_str(char const *str) {
	num_t res;
	num_set_type_br(res);
	(void) bnf_init_set_str(num_get_br(res), str, 10);
	return res;
}

static
num_t num_mk_br_mr(const mr_t a) {
	num_t res;
	assert(sizeof(mr_t) == sizeof(double));
	num_set_type_br(res);
	(void) bnf_init_set_d(num_get_br(res), (const double) a);
	return res;
}

static
num_t num_mk_br_bi(const bi_t a) {
	num_t res;
	num_set_type_br(res);
	(void) bnf_init_set_z(num_get_br(res), a);
	return res;
}

extern
num_t num_mk_br_br(const br_t a) {
	num_t res;
	num_set_type_br(res);
	(void) bnf_init_set(num_get_br(res), a);
	return res;
}

/* num_cp_mi in *.h */
/* num_cp_mr in *.h */
extern
num_t num_cp_bi(const num_t n) {
	num_t res;
	assert(num_is_bi(n));
	num_set_type_bi(res);
	bnz_init_set(num_get_bi(res), num_get_bi(n));
	return res;
}

extern
num_t num_cp_br(const num_t n) {
	num_t res;
	assert(num_is_br(n));
	num_set_type_br(res);
	bnf_init_set(num_get_br(res), num_get_br(n));
	return res;
}

extern
num_t num_cp(const num_t n) {
	assert(num_is_valid(n));
	switch(num_get_type(n)) {
		case MI:	return num_cp_mi(n);
		case MR:	return num_cp_mr(n);
		case BI:	return num_cp_bi(n);
		case BR:	return num_cp_br(n);
	
		default:
			noCase("num_cp");
			return num_mk_mi_si(0);
	}
}

#define NUM_COERCE_FUN(to, t)\
static \
num_t num_coerce_##to##_##t(const num_t n1) {\
	num_t res;\
	num_set_type_##to(res);\
	coerce_##to##_##t(num_get_##to(res), num_get_##t(n1));\
	return res;\
}\

static inline
num_t num_identity(const num_t n) {
	return n;
}

NUM_COERCE_FUN(mr, mi)
#define num_coerce_mr_mr	num_identity
NUM_COERCE_FUN(mr, bi)
NUM_COERCE_FUN(mr, br)

NUM_COERCE_FUN(bi, mi)
#define num_coerce_bi_bi	num_identity

NUM_COERCE_FUN(br, mi)
NUM_COERCE_FUN(br, bi)
#define num_coerce_br_br	num_identity


static
num_t (*coerceNum[NUMCOUNT][NUMCOUNT])(num_t n) = {
	{num_identity, num_coerce_mr_mi, num_coerce_bi_mi, num_coerce_br_mi},
	{num_identity, num_identity, num_identity, num_identity},
	{num_identity, num_identity, num_identity, num_coerce_br_bi},
	{num_identity, num_identity, num_identity, num_identity}};

extern
num_t num_coerce(const num_t n, const ntype toType) {
	return (*coerceNum[num_get_type(n)][toType])(n);
}





/* http://realtimecollisiondetection.net/blog/?p=89 */
/* fmax is only part of C99 */
/*
#define absTol DBL_EPSILON
#define relTol DBL_EPSILON
extern
bool isEq_mr(mr_t a, mr_t b) {
	if (fabs(a - b) <= fmax(absTol, relTol * fmax(fabs(a), fabs(b))))
		return true;
	return false;
}

static
int num_cmp_double_double(double d1, double d2) {
	double a = d1 - d2;  
	if (a > DBL_EPSILON) return 1;
	if (a <  - DBL_EPSILON) return -1;
	return 0;
}
*/


#define NUM_CMP(t1, t2)\
static \
int num_cmp_##t1##_##t2(const num_t n1, const num_t n2) {\
	assert(num_is_##t1(n1) && num_is_##t2(n2));\
	return cmp_##t1##_##t2(num_get_##t1(n1), num_get_##t2(n2));\
}\

NUM_CMP(mi, mi)
NUM_CMP(mi, mr)
NUM_CMP(mi, bi)
NUM_CMP(mi, br)

NUM_CMP(mr, mi)
NUM_CMP(mr, mr)
NUM_CMP(mr, bi)
NUM_CMP(mr, br)

NUM_CMP(bi, mi)
NUM_CMP(bi, mr)
NUM_CMP(bi, bi)
NUM_CMP(bi, br)

NUM_CMP(br, mi)
NUM_CMP(br, mr)
NUM_CMP(br, bi)
NUM_CMP(br, br)

static
int (*cmpNum[NUMCOUNT][NUMCOUNT])(const num_t n1, const num_t n2) = {
	{num_cmp_mi_mi, num_cmp_mi_mr, num_cmp_mi_bi, num_cmp_mi_br},
	{num_cmp_mr_mi, num_cmp_mr_mr, num_cmp_mr_bi, num_cmp_mr_br},
	{num_cmp_bi_mi, num_cmp_bi_mr, num_cmp_bi_bi, num_cmp_bi_br},
	{num_cmp_br_mi, num_cmp_br_mr, num_cmp_br_bi, num_cmp_br_br}
};

extern
int num_cmp(const num_t n1, const num_t n2) {
	return (*cmpNum[num_get_type(n1)][num_get_type(n2)])(n1, n2);
}

extern
bool num_is_same(const num_t n1, const num_t n2) {
	ntype nt1 = num_get_type(n1);
	ntype nt2 = num_get_type(n2);
	assert(num_is_valid(n1) && num_is_valid(n2));
	if (nt1 != nt2) return false;
	return (*cmpNum[nt1][nt2])(n1, n2) == 0; 
}

extern
void num_print_raw(FILE *f, const num_t n) {

	assert(num_is_valid(n));

	switch(num_get_type(n)) {
		case MI:
			print_raw_mi(f, num_get_mi(n));
   			break;

		case MR:
			print_raw_mr(f, num_get_mr(n));
			break;

		case BI:
			/*@-sysunrecog@*/
			print_raw_bi(f, num_get_bi(n));
			/*@=sysunrecog@*/
   			break;

		case BR:
			print_raw_br(f, num_get_br(n));
			break;

		default:
			noCase("num_print_raw");
			break;
	}
}


static
void num_noop_arg1(num_t *r, const num_t n1) {
	num_set_type_notype(*r);
}

static
void num_noop_arg2(num_t *r, const num_t n1, const num_t n2) {
	num_set_type_notype(*r);
}



#define NUM_ARG1_FUN(fun, t)\
static \
void num_##fun##_##t##_##t(num_t *r, const num_t n1) {\
	num_t res;\
	num_set_type_##t(res);\
	fun##_##t##_##t(num_get_##t(res), num_get_##t(n1));\
	*r = res;\
}\

#define NUM_ARG1_FP_DISPATCH(fun) \
static \
void (* fun##Num[NUMCOUNT])(num_t *r, const num_t n1) = {\
	num_##fun##_mi_mi,\
	num_##fun##_mr_mr,\
	num_##fun##_bi_bi,\
	num_##fun##_br_br\
};\
\
extern \
void num_##fun(num_t *r, const num_t n) {\
	assert(num_is_valid(n));\
	(* fun##Num[num_get_type(n)])(r, n);\
}\


/*abs*/
NUM_ARG1_FUN(abs, mi)
NUM_ARG1_FUN(abs, mr)
NUM_ARG1_FUN(abs, bi)
NUM_ARG1_FUN(abs, br)
NUM_ARG1_FP_DISPATCH(abs)

/*acos*/
#define num_acos_mi_mi	num_noop_arg1
NUM_ARG1_FUN(acos, mr)
#define num_acos_bi_bi	num_noop_arg1
NUM_ARG1_FUN(acos, br)
NUM_ARG1_FP_DISPATCH(acos)

/*acosh*/
#define num_acosh_mi_mi	num_noop_arg1
NUM_ARG1_FUN(acosh, mr)
#define num_acosh_bi_bi	num_noop_arg1
NUM_ARG1_FUN(acosh, br)
NUM_ARG1_FP_DISPATCH(acosh)

/*asin*/
#define num_asin_mi_mi	num_noop_arg1
NUM_ARG1_FUN(asin, mr)
#define num_asin_bi_bi	num_noop_arg1
NUM_ARG1_FUN(asin, br)
NUM_ARG1_FP_DISPATCH(asin)

/*asinh*/
#define num_asinh_mi_mi	num_noop_arg1
NUM_ARG1_FUN(asinh, mr)
#define num_asinh_bi_bi	num_noop_arg1
NUM_ARG1_FUN(asinh, br)
NUM_ARG1_FP_DISPATCH(asinh)

/*atan*/
#define num_atan_mi_mi	num_noop_arg1
NUM_ARG1_FUN(atan, mr)
#define num_atan_bi_bi	num_noop_arg1
NUM_ARG1_FUN(atan, br)
NUM_ARG1_FP_DISPATCH(atan)

/*atanh*/
#define num_atanh_mi_mi	num_noop_arg1
NUM_ARG1_FUN(atanh, mr)
#define num_atanh_bi_bi	num_noop_arg1
NUM_ARG1_FUN(atanh, br)
NUM_ARG1_FP_DISPATCH(atanh)

/*ceil*/
NUM_ARG1_FUN(ceil, mi)
NUM_ARG1_FUN(ceil, mr)
NUM_ARG1_FUN(ceil, bi)
NUM_ARG1_FUN(ceil, br)
NUM_ARG1_FP_DISPATCH(ceil)

/*cos*/
#define num_cos_mi_mi	num_noop_arg1
NUM_ARG1_FUN(cos, mr)
#define num_cos_bi_bi	num_noop_arg1
NUM_ARG1_FUN(cos, br)
NUM_ARG1_FP_DISPATCH(cos)

/*cosh*/
#define num_cosh_mi_mi	num_noop_arg1
NUM_ARG1_FUN(cosh, mr)
#define num_cosh_bi_bi	num_noop_arg1
NUM_ARG1_FUN(cosh, br)
NUM_ARG1_FP_DISPATCH(cosh)

/*erf*/
#define num_erf_mi_mi	num_noop_arg1
NUM_ARG1_FUN(erf, mr)
#define num_erf_bi_bi	num_noop_arg1
NUM_ARG1_FUN(erf, br)
NUM_ARG1_FP_DISPATCH(erf)

/*erfc*/
#define num_erfc_mi_mi	num_noop_arg1
NUM_ARG1_FUN(erfc, mr)
#define num_erfc_bi_bi	num_noop_arg1
NUM_ARG1_FUN(erfc, br)
NUM_ARG1_FP_DISPATCH(erfc)

/*exp*/
#define num_exp_mi_mi	num_noop_arg1
NUM_ARG1_FUN(exp, mr)
#define num_exp_bi_bi	num_noop_arg1
NUM_ARG1_FUN(exp, br)
NUM_ARG1_FP_DISPATCH(exp)

/*floor*/
NUM_ARG1_FUN(floor, mi)
NUM_ARG1_FUN(floor, mr)
NUM_ARG1_FUN(floor, bi)
NUM_ARG1_FUN(floor, br)
NUM_ARG1_FP_DISPATCH(floor)

/*gamma*/
#define num_gamma_mi_mi	num_noop_arg1
/*not very accurate*/
/*NUM_ARG1_FUN(gamma, mr)*/
#define num_gamma_mr_mr	num_noop_arg1
#define num_gamma_bi_bi	num_noop_arg1
NUM_ARG1_FUN(gamma, br)
NUM_ARG1_FP_DISPATCH(gamma)

/*log*/
#define num_log_mi_mi	num_noop_arg1
NUM_ARG1_FUN(log, mr)
#define num_log_bi_bi	num_noop_arg1
NUM_ARG1_FUN(log, br)
NUM_ARG1_FP_DISPATCH(log)

/*log2*/
#define num_log2_mi_mi	num_noop_arg1
NUM_ARG1_FUN(log2, mr)
#define num_log2_bi_bi	num_noop_arg1
NUM_ARG1_FUN(log2, br)
NUM_ARG1_FP_DISPATCH(log2)

/*log10*/
#define num_log10_mi_mi	num_noop_arg1
NUM_ARG1_FUN(log10, mr)
#define num_log10_bi_bi	num_noop_arg1
NUM_ARG1_FUN(log10, br)
NUM_ARG1_FP_DISPATCH(log10)

/*round*/
NUM_ARG1_FUN(round, mi)
NUM_ARG1_FUN(round, mr)
NUM_ARG1_FUN(round, bi)
NUM_ARG1_FUN(round, br)
NUM_ARG1_FP_DISPATCH(round)

/*sign*/
NUM_ARG1_FUN(sign, mi)
NUM_ARG1_FUN(sign, mr)
NUM_ARG1_FUN(sign, bi)
NUM_ARG1_FUN(sign, br)
NUM_ARG1_FP_DISPATCH(sign)

/*sin*/
#define num_sin_mi_mi	num_noop_arg1
NUM_ARG1_FUN(sin, mr)
#define num_sin_bi_bi	num_noop_arg1
NUM_ARG1_FUN(sin, br)
NUM_ARG1_FP_DISPATCH(sin)

/*sinh*/
#define num_sinh_mi_mi	num_noop_arg1
NUM_ARG1_FUN(sinh, mr)
#define num_sinh_bi_bi	num_noop_arg1
NUM_ARG1_FUN(sinh, br)
NUM_ARG1_FP_DISPATCH(sinh)

/*tan*/
#define num_tan_mi_mi	num_noop_arg1
NUM_ARG1_FUN(tan, mr)
#define num_tan_bi_bi	num_noop_arg1
NUM_ARG1_FUN(tan, br)
NUM_ARG1_FP_DISPATCH(tan)

/*tanh*/
#define num_tanh_mi_mi	num_noop_arg1
NUM_ARG1_FUN(tanh, mr)
#define num_tanh_bi_bi	num_noop_arg1
NUM_ARG1_FUN(tanh, br)
NUM_ARG1_FP_DISPATCH(tanh)

/*trunc*/
NUM_ARG1_FUN(trunc, mi)
NUM_ARG1_FUN(trunc, mr)
NUM_ARG1_FUN(trunc, bi)
NUM_ARG1_FUN(trunc, br)
NUM_ARG1_FP_DISPATCH(trunc)


/* bool special functions */
#define NUM_ARG1_BOOL_FUN(fun, t)\
static \
bool num_##fun##_##t(const num_t n1) {\
	return fun##_##t(num_get_##t(n1));\
}\

#define NUM_ARG1_BOOL_FP_DISPATCH(fun) \
static \
bool (* fun##Num[NUMCOUNT])(const num_t r1) = {\
	num_##fun##_mi,\
	num_##fun##_mr,\
	num_##fun##_bi,\
	num_##fun##_br\
};\
\
extern \
bool num_##fun(const num_t n) {\
	assert(num_is_valid(n));\
	return (* fun##Num[num_get_type(n)])(n);\
}\


/*is_even*/
static
bool is_even_mi(const mi_t a) {
	bi_t tmp;
	bnz_init_set_si(tmp, a);
	return (bnz_is_even(tmp) != 0);
}
#define is_even_mr(a)	(false)
#define is_even_bi(a)	(bnz_is_even(a) != 0)
#define is_even_br(a)	(false)

NUM_ARG1_BOOL_FUN(is_even, mi)
NUM_ARG1_BOOL_FUN(is_even, mr)
NUM_ARG1_BOOL_FUN(is_even, bi)
NUM_ARG1_BOOL_FUN(is_even, br)
NUM_ARG1_BOOL_FP_DISPATCH(is_even)

/*is_odd*/
static
bool is_odd_mi(const mi_t a) {
	bi_t tmp;
	bnz_init_set_si(tmp, a);
	return (bnz_is_odd(tmp) != 0);
}
#define is_odd_mr(a)	(false)
#define is_odd_bi(a)	(bnz_is_odd(a) != 0)
#define is_odd_br(a)	(false)

NUM_ARG1_BOOL_FUN(is_odd, mi)
NUM_ARG1_BOOL_FUN(is_odd, mr)
NUM_ARG1_BOOL_FUN(is_odd, bi)
NUM_ARG1_BOOL_FUN(is_odd, br)
NUM_ARG1_BOOL_FP_DISPATCH(is_odd)


#define NUM_FUN_ARG2(fun, t)\
static \
void num_##fun##_##t##_##t(num_t *r, const num_t n1, const num_t n2) {\
	num_t res;\
	num_set_type_##t(res);\
	fun##_##t##_##t(num_get_##t(res), num_get_##t(n1), num_get_##t(n2));\
	*r = res;\
}\

#define target_type(mi, mr)		mr

#define NUM_FUN_ARG2_COERCE(fun, rt, t1, t2)\
static \
void num_##fun##_##t1##_##t2(num_t *r, const num_t n1, const num_t n2) {\
	num_##fun##_##rt##_##rt(r, num_coerce_##rt##_##t1(n1), num_coerce_##rt##_##t2(n2));\
}\

#define NUM_FUN_ARG2_FP_DISPATCH(fun)\
static \
void (* fun##Num[NUMCOUNT][NUMCOUNT])(num_t *r, const num_t n1, const num_t n2) = {\
	{num_##fun##_mi_mi, num_##fun##_mi_mr, num_##fun##_mi_bi, num_##fun##_mi_br},\
	{num_##fun##_mr_mi, num_##fun##_mr_mr, num_##fun##_mr_bi, num_##fun##_mr_br},\
	{num_##fun##_bi_mi, num_##fun##_bi_mr, num_##fun##_bi_bi, num_##fun##_bi_br},\
	{num_##fun##_br_mi, num_##fun##_br_mr, num_##fun##_br_bi, num_##fun##_br_br}\
};\
\
extern \
void num_##fun(num_t *r, const num_t n1, const num_t n2) {\
	(* fun##Num[num_get_type(n1)][num_get_type(n2)])(r, n1, n2);\
}\
\


 
/* add scalar scalar */
static void num_add_bi_bi(num_t *r, const num_t n1, const num_t n2);

/* mi */
static
void num_add_mi_mi(num_t *r, const num_t n1, const num_t n2) {
	mi_t a, b;
	num_t res;
	a = num_get_mi(n1);
	b = num_get_mi(n2);
	if (add_is_ou_flow(a, b)) {
		num_t bn1 = num_mk_bi_si(a);
		num_t bn2 = num_mk_bi_si(b);
		num_add_bi_bi(&res, bn1, bn2);
	} else {
		num_set_type_mi(res);
		num_set_mi(res, a + b);
	}
	*r = res;
}

NUM_FUN_ARG2(add, mr)
NUM_FUN_ARG2(add, bi)
NUM_FUN_ARG2(add, br)

NUM_FUN_ARG2_COERCE(add, mr, mi, mr)
NUM_FUN_ARG2_COERCE(add, bi, mi, bi)
NUM_FUN_ARG2_COERCE(add, br, mi, br)

NUM_FUN_ARG2_COERCE(add, mr, mr, mi)
NUM_FUN_ARG2_COERCE(add, mr, mr, bi)
NUM_FUN_ARG2_COERCE(add, mr, mr, br)

NUM_FUN_ARG2_COERCE(add, bi, bi, mi)
NUM_FUN_ARG2_COERCE(add, mr, bi, mr)
NUM_FUN_ARG2_COERCE(add, br, bi, br)

NUM_FUN_ARG2_COERCE(add, br, br, mi)
NUM_FUN_ARG2_COERCE(add, mr, br, mr)
NUM_FUN_ARG2_COERCE(add, br, br, bi)

static
void (*add[NUMCOUNT][NUMCOUNT])(num_t *r, const num_t n1, const num_t n2) = {
	{num_add_mi_mi, num_add_mi_mr, num_add_mi_bi, num_add_mi_br},
	{num_add_mr_mi, num_add_mr_mr, num_add_mr_bi, num_add_mr_br},
	{num_add_bi_mi, num_add_bi_mr, num_add_bi_bi, num_add_bi_br},
	{num_add_br_mi, num_add_br_mr, num_add_br_bi, num_add_br_br}
};

extern
void num_add(num_t *r, const num_t n1, const num_t n2) {
	(*add[num_get_type(n1)][num_get_type(n2)])(r, n1, n2);
}



NUM_FUN_ARG2(rem, mi)
NUM_FUN_ARG2(rem, mr)
NUM_FUN_ARG2(rem, bi)
NUM_FUN_ARG2(rem, br)

NUM_FUN_ARG2_COERCE(rem, mr, mi, mr)
NUM_FUN_ARG2_COERCE(rem, bi, mi, bi)
NUM_FUN_ARG2_COERCE(rem, br, mi, br)

NUM_FUN_ARG2_COERCE(rem, mr, mr, mi)
NUM_FUN_ARG2_COERCE(rem, mr, mr, bi)
NUM_FUN_ARG2_COERCE(rem, mr, mr, br)

NUM_FUN_ARG2_COERCE(rem, bi, bi, mi)
NUM_FUN_ARG2_COERCE(rem, mr, bi, mr)
NUM_FUN_ARG2_COERCE(rem, br, bi, br)

NUM_FUN_ARG2_COERCE(rem, br, br, mi)
NUM_FUN_ARG2_COERCE(rem, mr, br, mr)
NUM_FUN_ARG2_COERCE(rem, br, br, bi)

NUM_FUN_ARG2_FP_DISPATCH(rem)


static void num_mul_bi_bi(num_t *r, const num_t n1, const num_t n2);

/* mi */
static
void num_mul_mi_mi(num_t *r, const num_t n1, const num_t n2) {
	mi_t a, b;
	num_t res;
	a = num_get_mi(n1);
	b = num_get_mi(n2);
	if ( mul_is_ou_flow(a, b) ) {
		num_t bn1 = num_mk_bi_si(a);
		num_t bn2 = num_mk_bi_si(b);
		num_mul_bi_bi(&res, bn1, bn2);
	} else {
		num_set_type_mi(res);
		num_set_mi(res, a * b);
	}
	*r = res;
}

NUM_FUN_ARG2(mul, mr)
NUM_FUN_ARG2(mul, bi)
NUM_FUN_ARG2(mul, br)

NUM_FUN_ARG2_COERCE(mul, mr, mi, mr)
NUM_FUN_ARG2_COERCE(mul, bi, mi, bi)
NUM_FUN_ARG2_COERCE(mul, br, mi, br)

NUM_FUN_ARG2_COERCE(mul, mr, mr, mi)
NUM_FUN_ARG2_COERCE(mul, mr, mr, bi)
NUM_FUN_ARG2_COERCE(mul, mr, mr, br)

NUM_FUN_ARG2_COERCE(mul, bi, bi, mi)
NUM_FUN_ARG2_COERCE(mul, mr, bi, mr)
NUM_FUN_ARG2_COERCE(mul, br, bi, br)

NUM_FUN_ARG2_COERCE(mul, br, br, mi)
NUM_FUN_ARG2_COERCE(mul, mr, br, mr)
NUM_FUN_ARG2_COERCE(mul, br, br, bi)

static
void (*mul[NUMCOUNT][NUMCOUNT])(num_t *r, const num_t n1, const num_t n2) = {
	{num_mul_mi_mi, num_mul_mi_mr, num_mul_mi_bi, num_mul_mi_br},
	{num_mul_mr_mi, num_mul_mr_mr, num_mul_mr_bi, num_mul_mr_br},
	{num_mul_bi_mi, num_mul_bi_mr, num_mul_bi_bi, num_mul_bi_br},
	{num_mul_br_mi, num_mul_br_mr, num_mul_br_bi, num_mul_br_br}
};

extern
void num_mul(num_t *r, const num_t n1, const num_t n2) {
	(*mul[num_get_type(n1)][num_get_type(n2)])(r, n1, n2);
}


NUM_FUN_ARG2(div, mi)
NUM_FUN_ARG2(div, mr)
NUM_FUN_ARG2(div, bi)
NUM_FUN_ARG2(div, br)

NUM_FUN_ARG2_COERCE(div, mr, mi, mr)
NUM_FUN_ARG2_COERCE(div, bi, mi, bi)
NUM_FUN_ARG2_COERCE(div, br, mi, br)

NUM_FUN_ARG2_COERCE(div, mr, mr, mi)
NUM_FUN_ARG2_COERCE(div, mr, mr, bi)
NUM_FUN_ARG2_COERCE(div, mr, mr, br)

NUM_FUN_ARG2_COERCE(div, bi, bi, mi)
NUM_FUN_ARG2_COERCE(div, mr, bi, mr)
NUM_FUN_ARG2_COERCE(div, br, bi, br)

NUM_FUN_ARG2_COERCE(div, br, br, mi)
NUM_FUN_ARG2_COERCE(div, mr, br, mr)
NUM_FUN_ARG2_COERCE(div, br, br, bi)

static
void (*divide[NUMCOUNT][NUMCOUNT])(num_t *r, const num_t n1, const num_t n2) = {
	{num_div_mi_mi, num_div_mi_mr, num_div_mi_bi, num_div_mi_br},
	{num_div_mr_mi, num_div_mr_mr, num_div_mr_bi, num_div_mr_br},
	{num_div_bi_mi, num_div_bi_mr, num_div_bi_bi, num_div_bi_br},
	{num_div_br_mi, num_div_br_mr, num_div_br_bi, num_div_br_br}
};

extern
void num_divide(num_t *r, const num_t n1, const num_t n2) {
	(*divide[num_get_type(n1)][num_get_type(n2)])(r, n1, n2);
}


/* pow scalar scalar */
static void num_pow_bi_mi(num_t *r, const num_t base, const num_t exp);
static void num_pow_mr_mr(num_t *r, const num_t base, const num_t exp);
static void num_pow_br_br(num_t *r, const num_t base, const num_t exp);

/* mi */
static
void num_pow_mi_mi(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t b = num_mk_bi_si(num_get_mi(base));
	num_pow_bi_mi(&res, b, exp);
	if (bnz_is_si(num_get_bi(res))) {
		/*try to convert back to mi*/
		mi_t temp = bnz_get_si(num_get_bi(res));
		num_set_type_mi(res);
		num_set_mi(res, temp);
	}
	*r = res;
}

static
void num_pow_mi_mr(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t b = num_mk_mr_d((const double) num_get_mi(base));
	num_pow_mr_mr(&res, b, exp);
	*r = res;
}

static
void num_pow_mi_bi(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t b = num_mk_bi_si(num_get_mi(base));
	if (bnz_is_si(num_get_bi(exp))) {
		num_t e = num_mk_mi_si(bnz_get_si(num_get_bi(exp)));
		num_pow_bi_mi(&res, b, e);
	} else {
		/*overflow! e.g.: 2^(3^(4^5))*/
		/* TODO: message */
		/* TODO: this is not quite right, why positive and why MR */
		res = num_p_inf_mr;
	}
	*r = res;
}

static
void num_pow_mi_br(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t b = num_mk_br_mr((const mr_t) num_get_mi(base));
	num_pow_br_br(&res, b, exp);
	*r = res;
}

/* mr */
static
void num_pow_mr_mi(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t e = num_mk_mr_d((const double) num_get_mi(exp));
	num_pow_mr_mr(&res, base, e);
	*r = res;
}

static
void num_pow_mr_mr(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	/* exp should need to be mi to stay in domain */
	num_set_type_mr(res);
	num_set_mr(res, pow(num_get_mr(base), num_get_mr(exp)));
	*r = res;
}

static
void num_pow_mr_bi(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t e = num_mk_mr_d(bnz_get_d(num_get_bi(exp)));
	num_pow_mr_mr(&res, base, e);
	*r = res;
}

static
void num_pow_mr_br(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t e = num_mk_mr_d(bnf_get_d(num_get_br(exp)));
	num_pow_mr_mr(&res, base, e);
	*r = res;
}

/* bi */
static
void num_pow_bi_mi(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	assert(num_get_mi(exp) >= 0);
	num_set_type_bi(res);
	bnz_init(num_get_bi(res));
	bnz_pow_ui(num_get_bi(res), num_get_bi(base), (unsigned long int) num_get_mi(exp));
	*r = res;
}

static
void num_pow_bi_mr(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t b = num_mk_mr_d(bnz_get_d(num_get_bi(base)));
	num_pow_mr_mr(&res, b, exp);
	*r = res;
}

static
void num_pow_bi_bi(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t e;
	assert(bnz_is_si(num_get_bi(exp)));
	/* bi^bi is insane */
	e = num_mk_mi_si(bnz_get_si(num_get_bi(exp)));
	num_pow_bi_mi(&res, base, e);
	*r = res;
}

static
void num_pow_bi_br(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t b = num_mk_br_bi(num_get_bi(base));
	num_pow_br_br(&res, b, exp);
	*r = res;
}


/* br */
static
void num_pow_br_mi(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_set_type_br(res);
	bnf_init(num_get_br(res));
	bnf_pow_si(num_get_br(res), num_get_br(base), num_get_mi(exp));
	*r = res;
}

static
void num_pow_br_mr(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_t b = num_mk_mr_d(bnf_get_d(num_get_br(base)));
	assert(num_get_mr(b) >= 0);
	num_pow_mr_mr(&res, b, exp);
	*r = res;
}

static
void num_pow_br_bi(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	num_set_type_br(res);
	bnf_init(num_get_br(res));
	bnf_pow_z(num_get_br(res), num_get_br(base), num_get_bi(exp));
	*r = res;
}

static
void num_pow_br_br(num_t *r, const num_t base, const num_t exp) {
	num_t res;
	/*assert(bnf_cmp_si(num_get_br(base), 0) >= 0);*/
	num_set_type_br(res);
	bnf_init(num_get_br(res));
	bnf_pow(num_get_br(res), num_get_br(base), num_get_br(exp));
	*r = res;
}

static
void (*power[NUMCOUNT][NUMCOUNT])(num_t *r, const num_t n1, const num_t n2) = {
	{num_pow_mi_mi, num_pow_mi_mr, num_pow_mi_bi, num_pow_mi_br},
	{num_pow_mr_mi, num_pow_mr_mr, num_pow_mr_bi, num_pow_mr_br},
	{num_pow_bi_mi, num_pow_bi_mr, num_pow_bi_bi, num_pow_bi_br},
	{num_pow_br_mi, num_pow_br_mr, num_pow_br_bi, num_pow_br_br}
};

/* num_pow could 'return' void and exp < 0 checked in uexPower */
extern
void num_pow(num_t *r, const num_t n1, const num_t n2) {
	(*power[num_get_type(n1)][num_get_type(n2)])(r, n1, n2);
}

/* gcd scalar scalar */
static void num_gcd_bi_bi(num_t *r, const num_t n1, const num_t n2);
static void num_gcd_bi_mi(num_t *r, const num_t n1, const num_t n2);

/* mi */
static
void num_gcd_mi_mi(num_t *r, const num_t n1, const num_t n2) {
	num_t res;
	mi_t gcd = mi_gcd(num_get_mi(n1), num_get_mi(n2));

	num_set_type_mi(res);
	num_set_mi(res, gcd);

	*r = res;
}

static
void num_gcd_mi_bi(num_t *r, const num_t n1, const num_t n2) {
	num_gcd_bi_mi(r, n2, n1);
}

static
void num_gcd_bi_mi(num_t *r, const num_t n1, const num_t n2) {
	num_t res;
	gcd_bi_mi(num_get_bi(res), num_get_bi(n1), num_get_mi(n2));
	num_set_type_bi(res);
	*r = res;
}

static
void num_gcd_bi_bi(num_t *r, const num_t n1, const num_t n2) {
	num_t res;
	num_set_type_bi(res);
	bnz_init(num_get_bi(res));
	bnz_gcd(num_get_bi(res), num_get_bi(n1), num_get_bi(n2));
	*r = res;
}

static
void (*gcd[NUMCOUNT][NUMCOUNT])(num_t *r, const num_t n1, const num_t n2) = {
	{num_gcd_mi_mi, num_noop_arg2, num_gcd_mi_bi, num_noop_arg2},
	{num_noop_arg2, num_noop_arg2, num_noop_arg2, num_noop_arg2},
	{num_gcd_bi_mi, num_noop_arg2, num_gcd_bi_bi, num_noop_arg2},
	{num_noop_arg2, num_noop_arg2, num_noop_arg2, num_noop_arg2}
};

extern
void num_gcd(num_t *r, const num_t n1, const num_t n2) {
	(*gcd[num_get_type(n1)][num_get_type(n2)])(r, n1, n2);
}


extern
void num_sub(num_t *r, const num_t n1, const num_t n2) {
	num_t res = num_mk_mi_si(-1);
	num_mul(&res, res, n2);
	num_add(&res, n1, res);
	*r = res;
}

/* divexact scalar scalar */
static void num_divexact_bi_bi(num_t *r, const num_t n1, const num_t n2);

/* mi */
static
void num_divexact_mi_mi(num_t *r, const num_t n1, const num_t n2) {
	num_t res;
	mi_t de = num_get_mi(n1) / num_get_mi(n2);

	num_set_type_mi(res);
	num_set_mi(res, de);

	*r = res;
}

static
void num_divexact_mi_bi(num_t *r, const num_t n1, const num_t n2) {
	num_t bn1 = num_mk_bi_si(num_get_mi(n1));
	num_divexact_bi_bi(r, bn1, n2);
}

static
void num_divexact_bi_mi(num_t *r, const num_t n1, const num_t n2) {
	num_t bn2 = num_mk_bi_si(num_get_mi(n2));
	num_divexact_bi_bi(r, n1, bn2);
}

static
void num_divexact_bi_bi(num_t *r, const num_t n1, const num_t n2) {
	num_t res;
	num_set_type_bi(res);
	bnz_init(num_get_bi(res));
	bnz_divexact(num_get_bi(res), num_get_bi(n1), num_get_bi(n2));
	*r = res;
}


static
void (*divexact[NUMCOUNT][NUMCOUNT])(num_t *r, const num_t n1, const num_t n2) = {
	{num_divexact_mi_mi, num_noop_arg2, num_divexact_mi_bi, num_noop_arg2},
	{num_noop_arg2, num_noop_arg2, num_noop_arg2, num_noop_arg2},
	{num_divexact_bi_mi, num_noop_arg2, num_divexact_bi_bi, num_noop_arg2},
	{num_noop_arg2, num_noop_arg2, num_noop_arg2, num_noop_arg2}
};

extern
void num_divexact(num_t *r, const num_t n1, const num_t n2) {
	(*divexact[num_get_type(n1)][num_get_type(n2)])(r, n1, n2);
}


/* A dedicated implementation were possibly more efficient */
extern
void num_min(num_t *r, const num_t n1, const num_t n2) {
	if ((*cmpNum[num_get_type(n1)][num_get_type(n2)])(n1, n2) < 0 ) {
		*r = n1;
	} else {
		*r = n2;
	}
}

extern
void num_max(num_t *r, const num_t n1, const num_t n2) {
	if ((*cmpNum[num_get_type(n1)][num_get_type(n2)])(n1, n2) > 0 ) {
		*r = n1;
	} else {
		*r = n2;
	}
}


size_t num_size(const num_t n) {
	size_t res = sizeof(num_get_type(n));
	switch (num_get_type(n)) {
		case MI:
			return sizeof(mi_t);
		case MR:
			return sizeof(mr_t);
		case BI:
			return bnz_size(num_get_bi(n));
		case BR:
			return bnf_size(num_get_br(n));

		default:
			noCase("num_size");
			return 0;
	}
	return res;
}


size_t num_iterateSteps(const num_t start, const num_t stop, const num_t step) {
	num_t nele;

	num_sub(&nele, stop, start);
	if (num_cmp(step, num_zero_mi) == 0) return 0;
	/* divide MR: -1 / 2 gives 0; MR works around this */
	num_divide(&nele, nele, num_coerce(step, MR));
	num_add(&nele, nele, num_mk_mi_si(1));
	num_floor(&nele, nele);

	if (num_cmp(nele, num_zero_mi) <= 0) return 0;

	switch (num_get_type(nele)) {
		case MI:
			return (size_t) num_get_mi(nele);
		case MR:
			return (size_t) num_get_mr(nele);

		case BI:
		case BR:
		default:
			noCase("num_iterateSteps");
			 return 0;
	}
}

void num_common_type(ntype *nt1, const ntype nt2) {
	if (*nt1 == nt2 || *nt1 == MR) return;
	if (nt2 == MR) {*nt1 = MR; return;}
	if (*nt1 == MI) {
		if (nt2 == BI) {*nt1 = BI; return;}
		if (nt2 == BR) {*nt1 = BR; return;}
	} else if (*nt1 == BI) {
		if (nt2 == MI) return;
		if (nt2 == BR) {*nt1 = BR; return;}
	}
} 
