#ifndef BASENUM_H
#define BASENUM_H

#include <stdbool.h>
#include <math.h>

#include "bignum.h"
#include "utils.h"


typedef long int	mi_t;
typedef double		mr_t;

typedef enum {
	MI	= 0,
	MR	= 1,
	BI	= 2,
	BR	= 3
} ntype;

#define NUMCOUNT	BR + 1 
#define NOTYPE		BR + 1

typedef struct {
	ntype nt;
	union {
		mi_t mit;
		mr_t mrt;
		bi_t bit;
		br_t brt;
	} nd;
} num_t;

#define MI_MAX LONG_MAX
/* -1 * MI_MIN should be MI_MAX */
#define MI_MIN LONG_MIN + 1

#define PRECISION_DIGITS	15



#define coerce_mi_mi(a, b)	((a) = (b))
#define coerce_mi_mr(a, b)	((a) = ((mi_t) (b)))
#define coerce_mi_bi(a, b)	((a) = bnz_get_si(b))
#define coerce_mi_br(a, b)	((a) = bnf_get_si(b))

#define coerce_mr_mi(a, b)	((a) = ((mr_t) (b)))
#define coerce_mr_mr(a, b)	((a) = (b))
#define coerce_mr_bi(a, b)	((a) = bnz_get_d(b)) 
#define coerce_mr_br(a, b)	((a) = bnf_get_d(b)) 

#define coerce_bi_mi(a, b)	(bnz_init_set_si((a), (b)))
#define coerce_bi_mr(a, b)	(bnz_init_set_d((a), (b)))
#define coerce_bi_bi(a, b)	(bnz_init_set((a), (b)))
#define coerce_bi_br(a, b)	(bnz_init(a), bnf_get_z((a),(b)))

#define coerce_br_mi(a, b)	(bnf_init_set_si((a), (b)))
#define coerce_br_mr(a, b)	(bnf_init_set_d((a), (b)))
#define coerce_br_bi(a, b)	(bnf_init_set_z((a),(b)))
#define coerce_br_br(a, b)	(bnf_init_set((a), (b)))


#define cmp_m__m_(a, b) (((a) > (b)) ? 1 : ((a) < (b)) ? -1 : 0)
#define cmp_mi_mi(a, b) (cmp_m__m_(a, b))
#define cmp_mi_mr(a, b) (cmp_m__m_(a, b))
#define cmp_mi_bi(a, b) (bnz_cmp_si(b, -(a)))
#define cmp_mi_br(a, b) (bnf_cmp_si(b, -(a)))

#define cmp_mr_mi(a, b) (cmp_m__m_(a, b))
#define cmp_mr_mr(a, b) (cmp_m__m_(a, b))
#define cmp_mr_bi(a, b) (bnz_cmp_d(b, -(a)))
#define cmp_mr_br(a, b) (bnf_cmp_d(b, -(a)))

#define cmp_bi_mi(a, b) (bnz_cmp_si(a, b))
#define cmp_bi_mr(a, b) (bnz_cmp_d(a, b))
#define cmp_bi_bi(a, b) (bnz_cmp(a, b))
int cmp_bi_br(const bi_t a, const br_t b);

#define cmp_br_mi(a, b) (bnf_cmp_si(a, b))
#define cmp_br_mr(a, b) (bnf_cmp_d(a, b))
int cmp_br_bi(const br_t a, const bi_t b);
#define cmp_br_br(a, b) (bnf_cmp(a, b))


/* add scalar scalar */
/* mi */
#define add_mi_mi(r, a, b)	 ((r) = (a) + (b))
#define add_mi_mr(r, a, b)	 ((r) = (a) + (b))
void add_mi_bi(bi_t r, const mi_t a, const bi_t b);
void add_mi_br(br_t r, const mi_t a, const br_t b);
/* mr */
#define add_mr_mi(r, a, b)	((r) = (a) + (b))
#define add_mr_mr(r, a, b)	((r) = (a) + (b))
#define add_mr_bi(r, a, b)	((r) = ((a) + bnz_get_d(b))) 
#define add_mr_br(r, a, b)	((r) = ((a) + bnf_get_d(b))) 
/* bi */
#define add_bi_mi(r, a, b)	add_mi_bi((r), (b), (a))
#define add_bi_mr(r, a, b)	add_mr_bi((r), (b), (a)) 
#define add_bi_bi(r, a, b)	(bnz_init(r), bnz_add((r), (a), (b)))
void add_bi_br(br_t r, const bi_t a, const br_t b);
/* br */
#define add_br_mi(r, a, b)	add_mi_br((r), (b), (a))
#define add_br_mr(r, a, b)	add_mr_br((r), (b), (a))
#define add_br_bi(r, a, b)	add_bi_br((r), (b), (a))
#define add_br_br(r, a, b)	(bnf_init(r), bnf_add((r), (a), (b)))

/* needed for gcd_mi_mi */
mi_t mi_gcd(mi_t a, mi_t b);
#define gcd_mi_mi(r, a, b)	((r) = mi_gcd(a, b)) 
#define gcd_mi_bi(r, a, b)	(gcd_bi_mi(r, b, a)) 
void gcd_bi_mi(bi_t r, const bi_t a, const mi_t b);
#define gcd_bi_bi(r, a, b)	(bnz_init(r), bnz_gcd(r, a, b))

/* div scalar scalar */
#define div_mi_mi(r, a, b)	((r) = (a) / (b))
#define div_mr_mr(r, a, b)	((r) = (a) / (b))
#define div_bi_bi(r, a, b)	(bnz_init(r), bnz_div((r), (a), (b)))
#define div_br_br(r, a, b)	(bnf_init(r), bnf_div((r), (a), (b)))

/* mul scalar scalar */
/* mi */
#define mul_mi_mi(r, a, b)	 ((r) = (a) * (b))
#define mul_mi_mr(r, a, b)	 ((r) = (a) * (b))
void mul_mi_bi(bi_t r, const mi_t a, const bi_t b);
void mul_mi_br(br_t r, const mi_t a, const br_t b);
/* mr */
#define mul_mr_mi(r, a, b)	((r) = (a) * (b))
#define mul_mr_mr(r, a, b)	((r) = (a) * (b))
#define mul_mr_bi(r, a, b)	((r) = ((a) * bnz_get_d(b))) 
#define mul_mr_br(r, a, b)	((r) = ((a) * bnf_get_d(b))) 
/* bi */
#define mul_bi_mi(r, a, b)	mul_mi_bi((r), (b), (a))
#define mul_bi_mr(r, a, b)	mul_mr_bi((r), (b), (a)) 
#define mul_bi_bi(r, a, b)	(bnz_init(r), bnz_mul((r), (a), (b)))
void mul_bi_br(br_t r, const bi_t a, const br_t b);
/* br */
#define mul_br_mi(r, a, b)	mul_mi_br((r), (b), (a))
#define mul_br_mr(r, a, b)	mul_mr_br((r), (b), (a))
#define mul_br_bi(r, a, b)	mul_bi_br((r), (b), (a))
#define mul_br_br(r, a, b)	(bnf_init(r), bnf_mul((r), (a), (b)))

void pow_mi_mi(bi_t r, const mi_t b, const mi_t e); 
#define pow_mi_mr(r, a, b)	((r) = pow(a, b))
void pow_mi_br(br_t r, const mi_t b, const br_t e); 
#define pow_mr_mi(r, a, b)	((r) = pow(a, b))
#define pow_mr_mr(r, a, b)	((r) = pow(a, b))
#define pow_bi_mi(r, a, b)	(bnz_init(r), bnz_pow_ui(r, a, b))
#define pow_br_mi(r, a, b)	(bnf_init(r), bnf_pow_si(r, a, b))
#define pow_br_bi(r, a, b)	(bnf_init(r), bnf_pow_z(r, a, b))
#define pow_br_br(r, a, b)	(bnf_init(r), bnf_pow(r, a, b))

/* rem scalar scalar */
#define rem_mi_mi(r, a, b)	((r) = (a) % (b)) /*b must not be 0*/
#define rem_mi_mr(r, a, b)	((r) = fmod((mr_t) (a), (b)))
void rem_mi_bi(bi_t r, const mi_t a, const bi_t b);
void rem_mi_br(br_t r, const mi_t a, const br_t b);

#define rem_mr_mi(r, a, b)	((r) = fmod((a), (mr_t) (b)))
#define rem_mr_mr(r, a, b)	((r) = fmod((a), (b)))
#define rem_mr_bi(r, a, b)	((r) = fmod((a), bnz_get_d(b))) 
#define rem_mr_br(r, a, b)	((r) = fmod((a), bnf_get_d(b))) 

void rem_bi_mi(bi_t r, const bi_t a, const mi_t b);
#define rem_bi_mr(r, a, b)	((r) = fmod(bnz_get_d(a), (b))) 
#define rem_bi_bi(r, a, b)	(bnz_init(r), bnz_rem((r), (a), (b)))
void rem_bi_br(br_t r, const bi_t a, const br_t b);

void rem_br_mi(br_t r, const br_t a, const mi_t b);
#define rem_br_mr(r, a, b)	((r) = fmod(bnf_get_d(a), (b))) 
void rem_br_bi(br_t r, const br_t a, const bi_t b);
#define rem_br_br(r, a, b)	(bnf_init(r), bnf_rem((r), (a), (b)))



/* special functions */
#define abs_mi_mi(a, b)		((a) = labs(b))
#define abs_mr_mr(a, b)		((a) = fabs(b))
#define abs_bi_bi(a, b)		(bnz_init(a), bnz_abs(a, b))
#define abs_br_br(a, b)		(bnf_init(a), bnf_abs(a, b))

#define acos_mr_mr(a, b)	((a) = acos(b))
#define acos_br_br(a, b)	(bnf_init(a), bnf_acos((a), (b)))

#define acosh_mr_mr(a, b)	((a) = acosh(b))
#define acosh_br_br(a, b)	(bnf_init(a), bnf_acosh((a), (b)))

#define asin_mr_mr(a, b)	((a) = asin(b))
#define asin_br_br(a, b)	(bnf_init(a), bnf_asin((a), (b)))

#define asinh_mr_mr(a, b)	((a) = asinh(b))
#define asinh_br_br(a, b)	(bnf_init(a), bnf_asinh((a), (b)))

#define atan_mr_mr(a, b)	((a) = atan(b))
#define atan_br_br(a, b)	(bnf_init(a), bnf_atan((a), (b)))

#define atanh_mr_mr(a, b)	((a) = atanh(b))
#define atanh_br_br(a, b)	(bnf_init(a), bnf_atanh((a), (b)))

#define ceil_mi_mi(a, b)	((a) = (b))
#define ceil_mr_mr(a, b)	((a) = ceil(b))
#define ceil_bi_bi(a, b)	(bnz_init_set((a), (b)))
#define ceil_br_br(a, b)	(bnf_init(a), bnf_ceil((a), (b)))

#define cos_mr_mr(a, b)		((a) = cos(b))
#define cos_br_br(a, b)		(bnf_init(a), bnf_cos((a), (b)))

#define cosh_mr_mr(a, b)	((a) = cosh(b))
#define cosh_br_br(a, b)	(bnf_init(a), bnf_cosh((a), (b)))

#define erf_mr_mr(a, b)		((a) = erf(b))
#define erf_br_br(a, b)		(bnf_init(a), bnf_erf((a), (b)))

#define erfc_mr_mr(a, b)	((a) = erfc(b))
#define erfc_br_br(a, b)	(bnf_init(a), bnf_erfc((a), (b)))

#define exp_mr_mr(a, b)		((a) = exp(b))
#define exp_br_br(a, b)		(bnf_init(a), bnf_exp((a), (b)))

#define floor_mi_mi(a, b)	((a) = (b))
#define floor_mr_mr(a, b)	((a) = floor(b))
#define floor_bi_bi(a, b)	(bnz_init_set((a), (b)))
#define floor_br_br(a, b)	(bnf_init(a), bnf_floor((a), (b)))

/*not very accurate*/
/*#define gamma_mr_mr(a, b)	((a) = tgamma(b))*/
#define gamma_br_br(a, b)	(bnf_init(a), bnf_gamma((a), (b)))

#define log_mr_mr(a, b)		((a) = log(b))
#define log_br_br(a, b)		(bnf_init(a), bnf_log((a), (b)))

#define log2_mr_mr(a, b)	((a) = log2(b))
#define log2_br_br(a, b)	(bnf_init(a), bnf_log2((a), (b)))

#define log10_mr_mr(a, b)	((a) = log10(b))
#define log10_br_br(a, b)	(bnf_init(a), bnf_log10((a), (b)))

#define round_mi_mi(a, b)	((a) = (b))
#define round_mr_mr(a, b)	((a) = round(b))
#define round_bi_bi(a, b)	(bnz_init_set((a), (b)))
#define round_br_br(a, b)	(bnf_init(a), bnf_round((a), (b)))

#define sign_mi_mi(a, b)	((a) = (b > 0) ? 1 : ((b < 0) ? -1 : 0))
#define sign_mr_mr(a, b)	((a) = (b > 0) ? 1 : ((b < 0) ? -1 : 0))
#define sign_bi_bi(a, b)	(bnz_init(a), bnz_sign(a, b))
#define sign_br_br(a, b)	(bnf_init(a), bnf_sign(a, b))

#define sin_mr_mr(a, b)		((a) = sin(b))
#define sin_br_br(a, b)		(bnf_init(a), bnf_sin((a), (b)))

#define sinh_mr_mr(a, b)	((a) = sinh(b))
#define sinh_br_br(a, b)	(bnf_init(a), bnf_sinh((a), (b)))

#define tan_mr_mr(a, b)		((a) = tan(b))
#define tan_br_br(a, b)		(bnf_init(a), bnf_tan((a), (b)))

#define tanh_mr_mr(a, b)	((a) = tanh(b))
#define tanh_br_br(a, b)	(bnf_init(a), bnf_tanh((a), (b)))

#define trunc_mi_mi(a, b)	((a) = (b))
#define trunc_mr_mr(a, b)	((a) = trunc(b))
#define trunc_bi_bi(a, b)	(bnz_init_set((a), (b)))
#define trunc_br_br(a, b)	(bnf_init(a), bnf_trunc((a), (b)))



#define print_raw_mi(f, d)	fprintf((f), "%li", (d))
#define print_raw_mr(f, d)	fprintf((f), "%.*f", PRECISION_DIGITS, (d))
#define print_raw_bi(f, d)	bnz_fprintf((f), "%Zd", (d))
#define print_raw_br(f, d)	bnf_fprintf((f), "%.*Rf", PRECISION_DIGITS, (d), 10)

#endif

