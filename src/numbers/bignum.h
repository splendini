
#ifndef UEXBIGNUM_H
#define UEXBIGNUM_H


#include <limits.h>
#include <stdio.h>
#include <gmp.h>
#include <mpfr.h>

typedef mpz_t		bi_t;
typedef mpfr_t		br_t;

void 	ue_init_bignum		( );

#define bnz_init				mpz_init
#define bnz_init_set			mpz_init_set
#define bnz_init_set_d			mpz_init_set_d
#define bnz_init_set_si			mpz_init_set_si
#define bnz_init_set_str		mpz_init_set_str
#define bnz_set					mpz_set
#define bnz_set_f				mpz_set_f
#define bnz_set_d				mpz_set_d
#define bnz_get_d				mpz_get_d
#define bnz_set_si				mpz_set_si
#define bnz_get_si				mpz_get_si
#define bnz_cmp					mpz_cmp
#define bnz_cmp_si				mpz_cmp_si
#define bnz_cmp_d				mpz_cmp_d
#define bnz_add					mpz_add
#define bnz_mul					mpz_mul
#define bnz_div					mpz_divexact
#define bnz_pow_ui				mpz_pow_ui
#define bnz_is_si				mpz_fits_slong_p
#define bnz_gcd					mpz_gcd
#define bnz_gcd_ui				mpz_gcd_ui
#define bnz_mod					mpz_mod
#define bnz_rem					mpz_tdiv_r
#define bnz_divexact			mpz_divexact
#define bnz_is_odd				mpz_odd_p
#define bnz_is_even				mpz_even_p
#define bnz_sign(a, b)			mpz_init_set_si(a, mpz_sgn(b))
void bnz_max(bi_t, const bi_t, const bi_t);
void bnz_min(bi_t, const bi_t, const bi_t);

#define bnf_init				mpfr_init
#define bnf_init_set(a, b)		mpfr_init_set(a, b, MPFR_RNDN)
#define bnf_init_set_d(a, b)	mpfr_init_set_d(a, b, MPFR_RNDN)
#define bnf_init_set_z(a, b)	mpfr_init_set_z(a, b, MPFR_RNDN)
#define bnf_init_set_si(a, b)	mpfr_init_set_si(a, b, MPFR_RNDN)
#define bnf_init_set_str(a, b, c)	mpfr_init_set_str(a, b, c, MPFR_RNDN)
#define bnf_set(a, b)			mpfr_set(a, b, MPFR_RNDN)
#define bnf_set_z(a, b)			mpfr_set_z(a, b, MPFR_RNDN)
#define bnf_set_d(a, b)			mpfr_set_d(a, b, MPFR_RNDN)
#define bnf_get_z(a, b)			mpfr_get_z(a, b, MPFR_RNDN)
#define bnf_get_si(a)			mpfr_get_si(a, MPFR_RNDN)
#define bnf_get_d(a)			mpfr_get_d(a, MPFR_RNDN)
#define bnf_cmp					mpfr_cmp
#define bnf_cmp_si				mpfr_cmp_si
#define bnf_cmp_d				mpfr_cmp_d
#define bnf_add(a, b, c)		mpfr_add(a, b, c, MPFR_RNDN)
#define bnf_div(a, b, c)		mpfr_div(a, b, c, MPFR_RNDN)
#define bnf_mul(a, b, c)		mpfr_mul(a, b, c, MPFR_RNDN)
#define bnf_pow(a, b, c)		mpfr_pow(a, b, c, MPFR_RNDN)
#define bnf_pow_si(a, b, c)		mpfr_pow_si(a, b, c, MPFR_RNDN)
#define bnf_ui_pow(a, b, c)		mpfr_ui_pow(a, b, c, MPFR_RNDN)
#define bnf_pow_z(a, b, c)		mpfr_pow_z(a, b, c, MPFR_RNDN)
#define bnf_pi(a)				mpfr_const_pi(a, MPFR_RNDN)
#define bnf_sign(a, b)			mpfr_init_set_d(a, mpfr_sgn(b), MPFR_RNDN)
#define bnf_max(a, b, c)		mpfr_max(a, b, c, MPFR_RNDN)
#define bnf_min(a, b, c)		mpfr_min(a, b, c, MPFR_RNDN)
#define bnf_rem(a, b, c)		mpfr_fmod(a, b, c, MPFR_RNDN)


/* special funcntions */
#define bnz_abs					mpz_abs
#define bnf_abs(a, b)			mpfr_abs(a, b, MPFR_RNDN)
#define bnf_acos(a, b)			mpfr_acos(a, b, MPFR_RNDN)
#define bnf_acosh(a, b)			mpfr_acosh(a, b, MPFR_RNDN)
#define bnf_asin(a, b)			mpfr_asin(a, b, MPFR_RNDN)
#define bnf_asinh(a, b)			mpfr_asinh(a, b, MPFR_RNDN)
#define bnf_atan(a, b)			mpfr_atan(a, b, MPFR_RNDN)
#define bnf_atanh(a, b)			mpfr_atanh(a, b, MPFR_RNDN)
#define bnf_cos(a, b)			mpfr_cos(a, b, MPFR_RNDN)
#define bnf_cosh(a, b)			mpfr_cosh(a, b, MPFR_RNDN)
#define bnf_ceil(a, b)			mpfr_ceil(a, b)
#define bnf_erf(a, b)			mpfr_erf(a, b, MPFR_RNDN)
#define bnf_erfc(a, b)			mpfr_erfc(a, b, MPFR_RNDN)
#define bnf_exp(a, b)			mpfr_exp(a, b, MPFR_RNDN)
#define bnf_floor(a, b)			mpfr_floor(a, b)
#define bnf_gamma(a, b)			mpfr_gamma(a, b, MPFR_RNDN)
#define bnf_log(a, b)			mpfr_log(a, b, MPFR_RNDN)
#define bnf_log2(a, b)			mpfr_log2(a, b, MPFR_RNDN)
#define bnf_log10(a, b)			mpfr_log10(a, b, MPFR_RNDN)
#define bnf_round(a, b)			mpfr_round(a, b)
#define bnf_sin(a, b)			mpfr_sin(a, b, MPFR_RNDN)
#define bnf_sinh(a, b)			mpfr_sinh(a, b, MPFR_RNDN)
#define bnf_tan(a, b)			mpfr_tan(a, b, MPFR_RNDN)
#define bnf_tanh(a, b)			mpfr_tanh(a, b, MPFR_RNDN)
#define bnf_trunc(a, b)			mpfr_trunc(a, b)


#define bnz_fprintf				gmp_fprintf
#define bnf_fprintf				mpfr_fprintf

/*mpz_t might have more components then accounted for here */
#define bnz_size(z)				(mpz_size(z) * (sizeof(mp_limb_t)))
/*TODO: not sure bnf_size is right*/
#define bnf_size(z)				(mpfr_custom_get_size(MPFR_RNDN))

#endif

