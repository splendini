
#include "num.h"

int main() {
	num_t n1, n2;

	n1 = num_mk_mi_si(0);
	n2 = num_cp(n1);
	if (num_cmp(n1, n2) != 0) return (EXIT_FAILURE);

	n1 = num_mk_mr_d(0);
	n2 = num_cp(n1);
	if (num_cmp(n1, n2) != 0) return (EXIT_FAILURE);

	n1 = num_mk_bi_str("0");
	n2 = num_cp(n1);
	if (num_cmp(n1, n2) != 0) return (EXIT_FAILURE);

	n1 = num_mk_br_str("0");
	n2 = num_cp(n1);
	if (num_cmp(n1, n2) != 0) return (EXIT_FAILURE);

	return(EXIT_SUCCESS);
}
