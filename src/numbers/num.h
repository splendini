#ifndef NUM_H
#define NUM_H

#include "basenum.h"

#define num_get_type(n)		((n).nt)
/* on 32 bit nt >= MI can generate a warning */
#define numtype_is_valid(nt)	((nt == MI) || ((nt) >= MR && (nt) <= BR) ? true : false)

#define num_get_mi(n)		(((n).nd).mit)
#define num_get_mr(n)		(((n).nd).mrt)
#define num_get_bi(n)		(((n).nd).bit)
#define num_get_br(n)		(((n).nd).brt)

#define num_is_mi(n)		((num_get_type(n) == MI	) ? true : false)
#define num_is_mr(n)		((num_get_type(n) == MR	) ? true : false)
#define num_is_bi(n)		((num_get_type(n) == BI	) ? true : false)
#define num_is_br(n)		((num_get_type(n) == BR	) ? true : false)

#define num_is_integer(n)	(num_is_mi(n) || num_is_bi(n))
#define num_is_real(n)		(num_is_mr(n) || num_is_br(n))
#define num_is_valid(n)		(num_is_integer(n) || num_is_real(n)) 

#define num_set_mi(n, i)	(assert(num_is_mi(n)), (((n).nd).mit) = (i))
#define num_set_mr(n, i)	(assert(num_is_mr(n)), (((n).nd).mrt) = (i))
#define num_set_bi(n, i)	(assert(num_is_bi(n)), (((n).nd).bit) = (i))
#define num_set_br(n, i)	(assert(num_is_br(n)), (((n).nd).brt) = (i))


num_t num_mk_mi_si		(const long i);
#define num_mk_mi_mi	num_mk_mi_si
num_t num_mk_mr_d		(const double d);
#define num_mk_mr_mr	num_mk_mr_d
num_t num_mk_bi_str		(char const *str);
num_t num_mk_bi_bi		(const bi_t a);
num_t num_mk_br_str		(char const *str);
num_t num_mk_br_br		(const br_t a);

extern num_t num_not_valid;

extern num_t num_m_one_mi;
extern num_t num_zero_mi;
extern num_t num_p_one_mi;

extern num_t num_m_inf_mr;
extern num_t num_m_one_mr;
extern num_t num_zero_mr;
extern num_t num_p_one_mr;
extern num_t num_p_e_mr;
extern num_t num_p_pi_mr;
extern num_t num_p_inf_mr;

extern num_t num_nan_mr;

extern num_t num_p_pi_br(void);


#define num_cp_mi(n)		(assert(num_is_mi(n)), num_mk_mi_si(num_get_mi(n)))
#define num_cp_mr(n)		(assert(num_is_mr(n)), num_mk_mr_d(num_get_mr(n)))
num_t num_cp_bi(const num_t n);
num_t num_cp_br(const num_t n);
num_t num_cp(const num_t n);

num_t num_coerce(const num_t n, const ntype toType);

int num_cmp(const num_t n1, const num_t n2);


bool num_is_same(const num_t n1, const num_t n2);
bool num_is_zero(const num_t n);

void num_abs(num_t *r, const num_t n);
void num_acos(num_t *r, const num_t n);
void num_acosh(num_t *r, const num_t n);
void num_asin(num_t *r, const num_t n);
void num_asinh(num_t *r, const num_t n);
void num_atan(num_t *r, const num_t n);
void num_atanh(num_t *r, const num_t n);
void num_ceil(num_t *r, const num_t n);
void num_cos(num_t *r, const num_t n);
void num_cosh(num_t *r, const num_t n);
void num_erf(num_t *r, const num_t n);
void num_erfc(num_t *r, const num_t n);
void num_exp(num_t *r, const num_t n);
void num_floor(num_t *r, const num_t n);
void num_gamma(num_t *r, const num_t n);
void num_log(num_t *r, const num_t n);
void num_log2(num_t *r, const num_t n);
void num_log10(num_t *r, const num_t n);
void num_round(num_t *r, const num_t n);
void num_sign(num_t *r, const num_t n);
void num_sin(num_t *r, const num_t n);
void num_sinh(num_t *r, const num_t n);
void num_tan(num_t *r, const num_t n);
void num_tanh(num_t *r, const num_t n);
void num_trunc(num_t *r, const num_t n);

bool num_is_odd	(const num_t);
bool num_is_even(const num_t);

void num_add	(num_t *r, const num_t n1, const num_t n2);
void num_mul	(num_t *r, const num_t n1, const num_t n2);
void num_pow	(num_t *r, const num_t n1, const num_t n2);
void num_gcd	(num_t *r, const num_t n1, const num_t n2);
void num_max	(num_t *r, const num_t n1, const num_t n2); 
void num_rem	(num_t *r, const num_t n1, const num_t n2); 
void num_min	(num_t *r, const num_t n1, const num_t n2); 

void num_divexact(num_t *r, const num_t n1, const num_t n2);
void num_sub	(num_t *r, const num_t n1, const num_t n2);
void num_divide	(num_t *r, const num_t n1, const num_t n2);

void num_print_raw(FILE *f, const num_t n);

size_t num_size(const num_t n); 

size_t num_iterateSteps(const num_t start, const num_t stop, const num_t step);

void num_common_type(ntype *nt1, const ntype nt2);

/*@-exportlocal@*/
#endif

