
#include "datastructures.h"

#include "uthash.h"
/* undefine the defaults */
#undef uthash_malloc
#undef uthash_free

/* re-define, specifying alternate functions */
#define uthash_malloc(sz) ue_Malloc(sz)
#define uthash_free(ptr,sz) ue_Free(ptr)

//#undef uthash_memcmp
//#define uthash_memcmp(a,b,len) my_memcmp(a,b,len)

//#undef uthash_strlen
//#define uthash_strlen(s) my_strlen(s)

struct ue_ht_char_t {
	char const *str;
	void * value;
	UT_hash_handle hh;
};

struct ue_ht_void_t {
	unsigned int id;
	void * key;
	void * value;
	UT_hash_handle hh;
};


void ue_str_hashtable_find(ue_ht_char_p *ht, char const *name, void **value) {
	ue_ht_char_p s = NULL;

	HASH_FIND(hh, *ht, name, strlen(name), s);
	if (s) {
		*value = s->value;
	} else {
		*value = NULL;
	}
}

void ue_void_hashtable_find(ue_ht_void_p *ht, unsigned int id, void **key, void **value) {
	ue_ht_void_p s = NULL;

	HASH_FIND(hh, *ht, &id, sizeof(unsigned int), s);
	if (s) {
		*key = s->key;
		*value = s->value;
	} else {
		*key = NULL;
		*value = NULL;
	}
}

void ue_str_hashtable_add(ue_ht_char_p *ht, char const *name, void * value) {
	ue_ht_char_p s = NULL;

#ifdef UE_DEBUG
	char *test;
	ue_str_hashtable_find(ht, name, (void *) &test);
	assert(!test);
#endif

	s = (ue_ht_char_p) ue_Malloc(sizeof(struct ue_ht_char_t));
	validateMemory(s, "ue_str_hashtable_add");

	s->str = name;
	s->value = value;

	HASH_ADD_KEYPTR(hh, *ht, s->str, strlen(s->str), s);
}

void ue_void_hashtable_add(ue_ht_void_p *ht, unsigned int id, void * key, void * value) {
	ue_ht_void_p s = NULL;

#ifdef UE_DEBUG
	char *k;
	char *v;
	ue_void_hashtable_find(ht, id, (void *) &k, (void *) &v);
	assert(!k && !v);
#endif

	s = (ue_ht_void_p) ue_Malloc(sizeof(struct ue_ht_void_t));
	validateMemory(s, "ue_uint_hashtable_add");

	s->id = id;
	s->value = value;

	HASH_ADD(hh, *ht, id, sizeof(unsigned int), s);
}

static inline
void ue_void_hashtable_replace(ue_ht_void_p *ht, unsigned int id, void * key, void * value) {
	ue_ht_void_p s = NULL;
	ue_ht_void_p r = NULL;

	s = (ue_ht_void_p) ue_Malloc(sizeof(struct ue_ht_void_t));
	validateMemory(s, "ue_void_hashtable_replace");

	s->id = id;
	s->key = key;
	s->value = value;

	HASH_REPLACE(hh, *ht, id, sizeof(unsigned int), s, r);
}

static inline
size_t ue_str_hashtable_count(ue_ht_char_p ht) {
	return (size_t) HASH_COUNT(ht);
}

static inline
size_t ue_void_hashtable_count(ue_ht_void_p ht) {
	return (size_t) HASH_COUNT(ht);
}

ue_p ue_str_hashtable_values(ue_ht_char_p ht) {
	ue_p list;
	size_t i, len;
	ue_ht_char_p s;

	len = ue_str_hashtable_count(ht);
	list = ue_mk_LumpShell(sList, len);

	for(s = ht, i = 1; s != NULL; s = s->hh.next, i++) {
		ue_set_LumpElem(list, i, (ue_p) s->value);
	}

	return list;
}

ue_p ue_void_hashtable_keyvalues(ue_ht_void_p ht) {
	ue_p keys, values, result;
	size_t i, len;
	ue_ht_void_p s;

	len = ue_void_hashtable_count(ht);
	result = ue_mk_LumpShell(sList, 2);
	keys = ue_mk_LumpShell(sList, len);
	values = ue_mk_LumpShell(sList, len);

	for(s = ht, i = 1; s != NULL; s = s->hh.next, i++) {
		ue_set_LumpElem(keys, i, (ue_p) s->key);
		ue_set_LumpElem(values, i, (ue_p) s->value);
	}

	ue_set_LumpElem(result, 1, keys);
	ue_set_LumpElem(result, 2, values);

	return result;
}

ue_ht_char_p ue_mk_str_hashtable(void) {
	ue_ht_char_p ht = NULL;
	return ht;
}

ue_ht_void_p ue_mk_void_hashtable(void) {
	ue_ht_void_p ht = NULL;
	return ht;
}

extern
unsigned int ue_hash(const ue_p u) {
	unsigned int hv = 0;

	switch (ue_get_type(u)) {

		case SYMBOL:
			HASH_VALUE(u, sizeof(ue_p), hv);
			break;

		case STRING:
			HASH_VALUE(ue_get_String(u), strlen(ue_get_String(u)), hv);
			break;

		case NUMBER: {
			num_t n = ue_get_Number(u);
			ntype nt = num_get_type(n);
			switch (nt) {
				case MI:
					HASH_VALUE(&(num_get_mi(n)), sizeof(mi_t), hv); break;
				case MR:
					HASH_VALUE(&(num_get_mr(n)), sizeof(mr_t), hv); break;
				case BI:
					HASH_VALUE(&(num_get_bi(n)), sizeof(bi_t), hv); break;
				case BR:
					HASH_VALUE(&(num_get_br(n)), sizeof(br_t), hv); break;
				default:
					noCase("ue_hash: Number");\
					return hv;
			}
			break;
		}

		case TENSOR: {
			ten_t n = ue_get_Tensor(u);
			ntype nt = ten_get_type(n);
			size_t nr = ten_get_number_of_values(n);
			switch (nt) {
				case MI: {
					mi_t *data = (mi_t *) ten_get_values(n);
					HASH_VALUE(data, nr * sizeof(mi_t), hv);
					break;
				}
				case MR: {
					mr_t *data = (mr_t *) ten_get_values(n);
					HASH_VALUE(data, nr * sizeof(mr_t), hv);
					break;
				}
				case BI: {
					bi_t *data = (bi_t *) ten_get_values(n);
					HASH_VALUE(data, nr * sizeof(bi_t), hv);
					break;
				}
				case BR: {
					br_t *data = (br_t *) ten_get_values(n);
					HASH_VALUE(data, nr * sizeof(br_t), hv);
					break;
				}
				default:
					noCase("ue_hash: Tensor");\
					return hv;
			}
			break;
		}

		case FUNCTION: return ue_hash(ue_get_FunctionUex(u));

		case DATA: {
			HASH_VALUE(ue_get_DataType(u), sizeof(ue_p), hv);
			ue_hash_Data(&hv, u);
			break;
		}

		case LUMP: {
			size_t i = 1, len = ue_length(u);
			/* to distingish hash(f) from hash(f()) */
			HASH_VALUE(&len, sizeof(size_t), hv);
			hash_combine(&hv, ue_get_Type(u));
			for (i = 1; i <= len; i++) {
				if (!hv) return hv;
				hash_combine(&hv, ue_get_LumpElem(u, i));
			}
			break;
		}

		default:
			noCase("ue_hash");\
			return hv;
	}

	return hv;
}


ue_p ue_Hash(ue_p u) {
	unsigned int h = 0;

	h = ue_hash(u);

	if (h) {
		assert(sizeof(unsigned int) <= sizeof(long)/2);
		return ue_mk_mi_si(h);
	}

	return sFailed;
}

/*
 *	LockupData
 */

typedef struct {
	ue_ht_void_p table;
	ue_p defaultValue;
} lookup_t;

ue_p sLookupData;

#define lookup(e)					((lookup_t *) ue_get_Data(e))
#define	ue_is_LookupData(e)			((ue_is_Data(e)) && (ue_get_DataType(e) == sLookupData))
#define ue_get_LookupData(e)		(assert(ue_is_LookupData(e)),lookup(e))
#define ue_get_LookupDataTable(e)	(assert(ue_is_LookupData(e)),lookup(e)->table)
#define ue_set_LookupDataTable(e, f)(assert(ue_is_LookupData(e)),(lookup(e)->table)=(f))
#define ue_get_LookupDataDefaultValue(e)\
									(assert(ue_is_LookupData(e)),\
									(lookup(e)->defaultValue))
#define ue_get_LookupDataKeyValues(e)\
									ue_void_hashtable_keyvalues(ue_get_LookupDataTable(e))


static
size_t ue_bytecount_LookupData(const ue_p u) {
	size_t bc = 0, len;
	ue_ht_void_p ht;

	assert(ue_is_LookupData(u));
	bc += sizeof(lookup_t);
	bc += sizeof(ue_ht_void_p);
	bc += sizeof(ue_p);

	ht = ue_get_LookupDataTable(u);

	len = ue_void_hashtable_count(ht);
	/* keys + values + defaultValue */
	bc += (2 * len + 1) * sizeof(ue_p);

	bc += len * sizeof(unsigned int);
	return bc;
}

static
ue_p ue_cp_LookupData(const ue_p u) {
	ue_p res, cpkv;
	ue_p cpdefaultValue, cpkeys, cpvalues;

	assert(ue_is_LookupData(u));

	cpkv = ue_cp(ue_get_LookupDataKeyValues(u));
	cpdefaultValue = ue_cp(ue_get_LookupDataDefaultValue(u));

	cpkeys = ue_get_LumpElem(cpkv, 1);
	cpvalues = ue_get_LumpElem(cpkv, 2);

	res = ue_mk_LookupData(cpkeys, cpvalues, cpdefaultValue);
	return res;
}

static
int ue_cmp_LookupData(const ue_p u1, const ue_p u2) {
	assert(ue_is_LookupData(u1) && ue_is_LookupData(u2));
	if (ue_get_LookupDataTable(u1) == ue_get_LookupDataTable(u2))
		return 0;

	if (ue_get_LookupDataTable(u1) < ue_get_LookupDataTable(u2))
		return -1;

	return 1;
}

static
void ue_hash_LookupData(unsigned int *hv, const ue_p u) {
	assert(ue_is_LookupData(u));

	*hv = ue_hash(ue_get_LookupDataKeyValues(u));
	hash_combine(hv, ue_get_LookupDataDefaultValue(u));

	return;
}

static
size_t ue_length_LookupData(const ue_p u) {
	assert(ue_is_LookupData(u));
	return 0;
}

static
void ue_printraw_LookupData(FILE *f, ue_p u, bool ps) {
	ue_p kv, dv;

	if (!ue_is_LookupData(u)) return;

	kv = ue_get_LookupDataKeyValues(u);
	dv = ue_get_LookupDataDefaultValue(u);

	ue_PrintRaw(f, ue_get_DataType(u), ps);
	fprintf(f, "(");
	ue_PrintRaw(f, kv, ps);
	fprintf(f, ", ");
	ue_PrintRaw(f, dv, ps);
	fprintf(f, ")");

	return;
}

static inline void
ue_fill_void_hastable(ue_ht_void_p * ht, const ue_p keys, const ue_p values) {
	ue_p ki, vi;
	size_t i;
	unsigned int hash;

	assert(ue_is_List(keys) && ue_is_List(values));
	assert(ue_get_LumpLength(keys) == ue_get_LumpLength(values));

	for (i = 1; i <= ue_get_LumpLength(keys); i++) {
		vi = ue_get_LumpElem(values, i);
		ki = ue_get_LumpElem(keys, i);
		hash = ue_hash(ki);
		assert(hash);
		ue_void_hashtable_replace(ht, hash, (void *) ki, (void *) vi);
	}
}

extern
ue_p ue_mk_LookupData(const ue_p keys, const ue_p values, const ue_p defaultValue) {
	ue_p res;
	lookup_t * data;
	ue_ht_void_p ht = NULL;

	res = ue_mk_Data();

	data = (lookup_t *) ue_Malloc(sizeof(lookup_t));
	validateMemory(data, "ue_mk_LookupData");

	ue_set_DataType(res, sLookupData);

	ue_fill_void_hastable(&ht, keys, values);

	(*data).table = ht;
	(*data).defaultValue = defaultValue;

	ue_set_Data_bytecount(res, &ue_bytecount_LookupData);
	ue_set_Data_cmp(res, &ue_cmp_LookupData);
	ue_set_Data_cp(res, &ue_cp_LookupData);
	ue_set_Data_hash(res, &ue_hash_LookupData);
	ue_set_Data_length(res, &ue_length_LookupData);
	ue_set_DataPrintRaw(res, &ue_printraw_LookupData);

	ue_set_Data(res, (void *) data);

	return res;
}

extern
ue_p ue_LookupDataAdd(ue_p u, const ue_p keys, const ue_p values) {
	lookup_t * data;
	ue_ht_void_p ht = NULL;

	if (!ue_is_LookupData(u)) return sFailed;

	data = (lookup_t *) ue_get_Data(u);
	ht = (*data).table;

	ue_fill_void_hastable(&ht, keys, values);

	(*data).table = ht;
	ue_set_Data(u, (void *) data);

	return values;
}

extern
ue_p ue_LookupDataDefaultValue(const ue_p u) {
	if (!ue_is_LookupData(u)) return sFailed;
	return ue_get_LookupDataDefaultValue(u);
}

extern
ue_p ue_LookupDataKeyValues(const ue_p u) {
	if (!ue_is_LookupData(u)) return sFailed;
	return ue_get_LookupDataKeyValues(u);
}

extern
ue_p ue_LookupDataFind(const ue_p u, const ue_p keys) {
	lookup_t * data;
	ue_ht_void_p ht;
	unsigned int hash;
	ue_p key, value, v, res, ki;
	size_t len, i;

	if (!ue_is_LookupData(u)) return sFailed;

	data = (lookup_t *) ue_get_Data(u);
	ht = (*data).table;

	if (!ue_is_List(keys)) return sFailed;

	len = ue_get_LumpLength(keys);
	res = ue_mk_LumpShell(sList, len);

	for (i = 1; i <= len; i++) {
		v = (*data).defaultValue;
		ki = ue_get_LumpElem(keys, i);
		hash = ue_hash(ki);
		if (hash) {
			ue_void_hashtable_find(&ht, hash, (void *) &key, (void *) &value);
			if (value) v = (ue_p) value;
		}
		ue_set_LumpElem(res, i, v);
	}

	return res;
}

void ue_init_datastructures() {
	sLookupData = ue_mk_Symbol("LookupData");
}


