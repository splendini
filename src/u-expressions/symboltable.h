
#ifndef	SYMBOLTABLE_H 
#define SYMBOLTABLE_H

#include "uex.h"

void ue_init_symboltable();

void ue_symboltable_lookup(char const *name, ue_p *u);
void ue_symboltable_add(ue_p u);

ue_p ue_SymbolTable(void);

bool ue_mk_UniqueSymbol(ue_p *res, ue_p u);
bool ue_mk_UniqueSymbolList(ue_p *res, ue_p u);

#endif
