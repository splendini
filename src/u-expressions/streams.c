
#include "uex.h"

ue_p sInputStream;
ue_p sOutputStream;

static
size_t ue_bytecount_Stream(const ue_p u) {
	assert(ue_is_Stream(u));
	return 0;
}

static
ue_p ue_cp_Stream(const ue_p u) {
	assert(ue_is_Stream(u));

	/* a 'w' mode stream can not be copied; this would delete
	the content */
	if (strcmp(ue_get_StreamMode(u), "w") == 0) {
		return sFailed;
	} else {
		return ue_mk_Stream(ue_get_String(ue_get_StreamPath(u)), ue_get_StreamMode(u));
	}
}

static
int ue_cmp_Stream(const ue_p u1, const ue_p u2) {
	assert(ue_is_Stream(u1) && ue_is_Stream(u2));
	if (ue_get_StreamType(u1) == ue_get_StreamType(u2))
		return ue_cmp_String(ue_get_StreamPath(u1), ue_get_StreamPath(u2));

	if (ue_get_StreamType(u1) < ue_get_StreamType(u2))
		return -1;

	return 1;
}

static
void ue_hash_Stream(unsigned int *hv, const ue_p u) {
	assert(ue_is_Stream(u));
	hash_combine(hv, ue_mk_String(ue_get_StreamMode(u)));
	hash_combine(hv, ue_get_StreamPath(u));
}

static
size_t ue_length_Stream(const ue_p u) {
	assert(ue_is_Stream(u));
	return 0;
}

static
void ue_printraw_Stream(FILE *f, ue_p m, bool ps) {
	ue_PrintRaw(f, ue_get_DataType(m), ps);
	fprintf(f, "(");
	ue_PrintRaw(f, ue_get_StreamPath(m), ps);
	fprintf(f, ")");
}

extern
ue_p ue_mk_Stream_from_FILE(FILE *file, char const *name, char const *mode) {
	ue_p res;
	stream_t * data;

	assert(strlen(mode) == 1);

	res = ue_mk_Data();

	data = (stream_t *) ue_Malloc(sizeof(stream_t));
	validateMemory(data, "ue_mk_Stream");

	if (strcmp(mode, "r") == 0) {
		ue_set_DataType(res, sInputStream);
	} else {
		ue_set_DataType(res, sOutputStream);
	}

	(*data).file = file;
	(*data).path = ue_mk_String(name);
	(*data).mode = ue_strdup(mode);

	ue_set_Data_bytecount(res, &ue_bytecount_Stream);
	ue_set_Data_cmp(res, &ue_cmp_Stream);
	ue_set_Data_cp(res, &ue_cp_Stream);
	ue_set_Data_hash(res, &ue_hash_Stream);
	ue_set_Data_length(res, &ue_length_Stream);
	ue_set_DataPrintRaw(res, &ue_printraw_Stream);

	ue_set_Data(res, (void *) data);

	return res;
}

extern
ue_p ue_mk_Stream(char const *path, char const *mode) {
	ue_p res;
	FILE *file;

	assert(strlen(mode) == 1);

	file = fopen(path, mode);
	if (!file) {
		fprintf(stderr, "mkStream: file \"%s\" could not be opened.\n", path);
		return sFailed;
	}

	res = ue_mk_Stream_from_FILE(file, path, mode);
	return res;
}

extern
ue_p ue_StreamClose(ue_p u) {
	ue_p res;
	assert(ue_is_Stream(u));

	(void) fclose(ue_get_StreamFP(u));

	res = ue_get_StreamPath(u);
	return res;
}


void ue_init_streams() {
	sInputStream = ue_mk_Symbol("InputStream");
	sOutputStream = ue_mk_Symbol("OutputStream");
}

