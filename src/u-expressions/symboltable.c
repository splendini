
#include "symboltable.h"
#include "datastructures.h"

ue_ht_char_p symtabhash;

void ue_symboltable_lookup(char const *name, ue_p *u) {
	ue_str_hashtable_find(&symtabhash, name, (void *) u);
}

void ue_symboltable_add(ue_p u) {
	assert(ue_is_Symbol(u));
	ue_str_hashtable_add(&symtabhash, ue_get_SymbolName(u), (void *) u);
}

ue_p ue_SymbolTable(void) {
	ue_p symbolTable;

	symbolTable = ue_str_hashtable_values(symtabhash);
	ue_set_LumpType(symbolTable, sSymbolTable);

	return symbolTable;
}


ue_ht_char_p unique_symbol_hashtable = NULL;

bool ue_mk_UniqueSymbol(ue_p *res, ue_p u) {
	char const *name, *s;
	char *dest;
	size_t nlen, ilen;
	ue_p dummy;
	int k = 0;
	long *counter = NULL;

	*res = NULL;

	if (!ue_is_Symbol(u)) return false;

	name = ue_get_SymbolName(u);
	nlen = strlen(name);

	ue_str_hashtable_find(&unique_symbol_hashtable, name, (void *) &counter);
	if (counter) {
		*counter += 1;
	} else {
		counter = (long *) ue_Malloc(sizeof(long));
		validateMemory(counter, "ue_mk_UniqueSymbol");
		*counter = 1;
		ue_str_hashtable_add(&unique_symbol_hashtable, name, (void *) counter);
	}

	while (k < 10 && *counter < LONG_MAX) {
		name = ue_get_SymbolName(u);
		dummy = NULL;

		ilen = snprintf(NULL, 0,"%li", *counter);

		dest = (char *) ue_Malloc(sizeof(char) * (nlen + ilen + 2));
		validateMemory(dest, "ue_mk_UniqueSymbol");
		s = dest;

		while (*name != '\0') {
			*dest++ = *name++;
		}

		*dest++ = '$';

		/* includes '\0' */
		snprintf(dest, ilen + 1, "%li", *counter);

		/* check that no symbol with name s exists */
		ue_symboltable_lookup(s, &dummy);
		if (!dummy) {
			*res = ue_mk_Symbol(s);
			return true;
		} else {
			*counter += 1;
		}
		k++;
	}

	return false;
}

bool ue_mk_UniqueSymbolList(ue_p *res, ue_p u) {
	ue_p tmp, ui;
	size_t i, len;

	if (!ue_is_List(u)) return false;

	len = ue_length(u);
	tmp = ue_mk_LumpShell(sList, len);

	i = 1;
	while (i <= len && ue_mk_UniqueSymbol(&ui, ue_get_LumpElem(u, i))) {
		ue_set_LumpElem(tmp, i, ui);
		i++;
	}

	if (i - 1 == len) {
		*res = tmp;
		return true;
	}

	return false;
}

void ue_init_symboltable() {
	symtabhash = ue_mk_str_hashtable();
}

