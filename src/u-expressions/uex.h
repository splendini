/*
*
* Universal EXpression
*
*/

#ifndef UEX_H
#define UEX_H

#include "num.h"
#include "tensor.h"
#include "utils.h"

/*@-exportlocal@*/
void		ue_init_lib		( );

/* universal expressions: ue */
typedef struct ue_t * ue_p;

typedef enum {
	NUMBER			=	0, 
	TENSOR			=	1,
	SYMBOL			=	2,
	STRING			=	3,
	FUNCTION		=	4,
	DATA			=	5,
	LUMP			=	6
} utype_t;

/* function attributes - increment attr field */
#define NOATTR				0
#define QUOTEFIRST			(1 << 0)
#define QUOTEREST			(1 << 1)
#define QUOTEFIRSTCOERCION	(1 << 2)
#define QUOTERESTCOERCION	(1 << 3)
#define LISTABLE			(1 << 4)
#define LAST_ATTR			LISTABLE

#define QUOTEALL			(QUOTEFIRST | QUOTEREST)
#define QUOTEALLCOERCION	(QUOTEFIRSTCOERCION | QUOTERESTCOERCION)

#define NOFUN		0
#define HOST		(1 << 0)
#define PURE		(1 << 1)
#define LAST_FUN	PURE

/* TODO: uint need to be more specific. e.g. int32_t */
typedef unsigned int fattr_t;
typedef unsigned int ftype_t;

typedef struct {
	char *name;	
	fattr_t attr	:LAST_ATTR;
} symbol_t;

typedef struct {
	ue_p ufunction;	
	union {
		ue_p (*fp)(ue_p state);
		ue_p env;
	} fd;
	fattr_t attr	:LAST_ATTR;
	ftype_t ft		:LAST_FUN;
} function_t;

typedef struct dataops_t {
	size_t (*bytecount)(const ue_p u);
	ue_p (*coerce)(const ue_p u, const ntype toType);
	int (*cmp)(const ue_p u1, const ue_p u2);
	ue_p (*cp)(const ue_p u);
	void (*hash)(unsigned int *hv, const ue_p u);
	size_t (*length)(const ue_p u);
	void (*printraw)(FILE *f, ue_p m, bool ps);
} dataops_t;

typedef struct {
	ue_p type;	
	void *data;
	dataops_t *ops;
} data_t;
 
typedef struct {
	ue_p *elems;
	size_t length;
} lump_t;

struct ue_t {
	union {
		char *cd;
		symbol_t sd;
		num_t nd;
		ten_t td;
		function_t fd;
		data_t dd;
		lump_t ld;
	} ud;
	utype_t t;
};

/*atomic type*/
extern ue_p sSymbol;
extern ue_p sString;
extern ue_p sMInt;
extern ue_p sMReal;
extern ue_p sBInt;
extern ue_p sBReal;
extern ue_p sTensor;
extern ue_p sMIntTensor;
extern ue_p sMRealTensor;
extern ue_p sBIntTensor;
extern ue_p sBRealTensor;

extern ue_p sSystemFunction;
extern ue_p sInterpretedFunction;

extern ue_p sData;

/*lumps*/
extern ue_p sList;
extern ue_p sEmptyList;
extern ue_p sSymbolTable;

/*unevaluated functions*/
extern ue_p sCoerce;
extern ue_p sQuoteCoercion;

/*derived types*/
extern ue_p sAll;
extern ue_p sDone;
extern ue_p sFailed;
extern ue_p sNull;
extern ue_p sQuit;
extern ue_p sRestart;

extern ue_p sSLocale;
extern ue_p symbolTable;

#define ue_get_type(e)			((e)->t)
#define ue_set_type(e, type)	((e)->t) = (type)

#define ue_is_Symbol(e)			((ue_get_type(e) == SYMBOL	) ? true : false)
#define ue_is_String(e)			((ue_get_type(e) == STRING	) ? true : false)
#define ue_is_Number(e)			((ue_get_type(e) == NUMBER	) ? true : false)
#define ue_is_Tensor(e)			((ue_get_type(e) == TENSOR	) ? true : false)
#define	ue_is_Function(e)		((ue_get_type(e) == FUNCTION) ? true : false)
#define	ue_is_Data(e)			((ue_get_type(e) == DATA	) ? true : false)
#define ue_is_Lump(e)			((ue_get_type(e) == LUMP	) ? true : false)


#define ued(e)					((e)->ud)


#define sym(e)					(ued(e).sd)
#define ue_get_SymbolName(e)	(assert(ue_is_Symbol(e)), sym(e).name)
#define ue_set_SymbolName(e, n)	(ue_set_type(e, SYMBOL), (sym(e).name) = (n))
#define ue_has_SymbolAttribute(e, a)	(assert(ue_is_Symbol(e)),\
										 (sym(e).attr) & (a))
#define ue_get_SymbolAttributes(e)		(assert(ue_is_Symbol(e)),\
										 (sym(e).attr))
#define ue_set_SymbolAttributes(e, a)	(assert(ue_is_Symbol(e)),\
										 (sym(e).attr) = (a))
#define ue_add_SymbolAttribute(e, a)	(assert(ue_is_Symbol(e)),\
										 ((sym(e).attr) |= (a)))
#define ue_remove_SymbolAttribute(e, a)	(assert(ue_is_Symbol(e)),\
										 ((sym(e).attr) &= ~(a)))


#define ue_get_String(e)		(assert(ue_is_String(e)), ued(e).cd) 
#define ue_set_String(e, n)		(ue_set_type(e, STRING), (ued(e).cd) = (n))
size_t ue_get_StringLength(ue_p u);
ue_p ue_get_StringCharacters(ue_p u); 


#define ue_get_Number(e)		(assert(ue_is_Number(e)), ued(e).nd) 
#define ue_set_Number(e, n)		(ue_set_type(e, NUMBER), (ued(e).nd) = (n))
#define ue_get_NumberType(e)	(num_get_type(ue_get_Number(e))) 


#define ue_get_Tensor(e)		(assert(ue_is_Tensor(e)), ued(e).td) 
#define ue_set_Tensor(e, t)		(ue_set_type(e, TENSOR), (ued(e).td) = (t))
#define ue_get_TensorType(e)	(ten_get_type(ue_get_Tensor(e))) 
#define ue_get_TensorRank(e)	(ten_get_rank(ue_get_Tensor(e))) 


/* (host/pure)function */
#define fun(e)					(ued(e).fd)
#define ue_get_Function(e)		(assert(ue_is_Function(e)), fun(e))
#define ue_get_FunctionType(e)	(assert(ue_is_Function(e)), fun(e).ft)
#define	ue_is_SystemFunction(e)	(ue_is_Function(e) && ((fun(e).ft) == HOST))
#define	ue_is_PureFunction(e)	(ue_is_Function(e) && ((fun(e).ft) == PURE))

#define ue_get_FunctionUex(e)	(assert(ue_is_Function(e)), fun(e).ufunction)
#define ue_set_FunctionUex(e, f)	(ue_set_type(e, FUNCTION),\
									 (fun(e).ufunction) = (f))
#define	ue_get_SystemFunctionFP(e)	(assert(ue_is_SystemFunction(e)),\
									 (fun(e).fd).fp)
#define	ue_set_SystemFunctionFP(e, f)	(assert(ue_is_Function(e)),\
								 	 ((fun(e).ft) = HOST),\
									 ((fun(e).fd).fp) = (f))
#define ue_get_PureFunctionEnv(e)	(assert(ue_is_PureFunction(e)),\
									 ((fun(e).fd).env))
#define	ue_set_PureFunctionEnv(e, f) (assert(ue_is_Function(e)),\
								 	 ((fun(e).ft) = PURE),\
									 ((fun(e).fd).env) = (f))

#define ue_has_FunctionAttribute(e, a)	(assert(ue_is_Function(e)), (fun(e).attr) & (a))
#define ue_get_FunctionAttributes(e)	(assert(ue_is_Function(e)), (fun(e).attr))
#define ue_set_FunctionAttributes(e, a)	(assert(ue_is_Function(e)), (fun(e).attr) = (a))
#define ue_add_FunctionAttribute(e, a)	(assert(ue_is_Function(e)),\
										 ((fun(e).attr) |= (a)))
#define ue_remove_FunctionAttribute(e, a)	(assert(ue_is_Function(e)),\
									((fun(e).attr) &= ~(a)))


/* data */
#define data(e)					(ued(e).dd)
#define ue_get_Data(e)			(assert(ue_is_Data(e)),data(e).data)
#define ue_set_Data(e, d)		(assert(ue_is_Data(e)),data(e).data = (d))
#define ue_get_DataType(e)		(assert(ue_is_Data(e)),data(e).type)
#define ue_set_DataType(e, t)	(assert(ue_is_Data(e)),(data(e).type) = (t))
#define ue_get_DataOperators(e)		(assert(ue_is_Data(e)),data(e).ops)
#define ue_set_DataOperators(e, o)	(assert(ue_is_Data(e)),(data(e).ops) = (o))

#define ue_set_Data_bytecount(e, o)	(assert(ue_is_Data(e)), data(e).ops->bytecount = o)
#define ue_set_Data_coerce(e, o)	(assert(ue_is_Data(e)), data(e).ops->coerce = o)
#define ue_set_Data_cmp(e, o)		(assert(ue_is_Data(e)), data(e).ops->cmp = o)
#define ue_set_Data_cp(e, o)		(assert(ue_is_Data(e)), data(e).ops->cp = o)
#define ue_set_Data_hash(e, o)		(assert(ue_is_Data(e)), data(e).ops->hash = o)
#define ue_set_Data_length(e, o)	(assert(ue_is_Data(e)), data(e).ops->length = o)
#define ue_set_DataPrintRaw(e, o)	(assert(ue_is_Data(e)), data(e).ops->printraw = o)

ue_p	ue_mk_Data(void);
void	ue_hash_Data(unsigned int *hv, const ue_p u);

/* lump */
#define lump(e)					(ued(e).ld)
#define ue_get_LumpElems(e)		(assert(ue_is_Lump(e)), lump(e).elems)
#define ue_get_LumpLength(e)	(assert(ue_is_Lump(e)), lump(e).length)
#define ue_get_LumpType(e)		(assert(ue_is_Lump(e)), ue_get_LumpElems(e)[0])
#define ue_set_LumpType(e, s)	(assert(ue_is_Lump(e)),\
								 ue_get_LumpElems(e)[0] = (s))
#define ue_get_LumpElem(e, i)	(assert(ue_is_Lump(e) && (i<=lump(e).length)),\
								 ue_get_LumpElems(e)[(i)])
#define ue_set_LumpElem(e, i,s)	(assert(lump(e).elems && (i<=lump(e).length)),\
								 ue_get_LumpElems(e)[(i)] = (s))


ue_p	ue_mk_Symbol	(char const *str);
ue_p	ue_mk_String	(char const *str);
ue_p	ue_mk_Number	(num_t n);
ue_p	ue_mk_mi_si		(long int i);
ue_p	ue_mk_mr_d		(double d);
ue_p	ue_mk_bi_str	(char const *str);
ue_p	ue_mk_br_str	(char const *str);
ue_p	ue_mk_Tensor_ue	(ntype type, size_t rank, size_t *dims, void *data); 
ue_p	ue_mk_Tensor	(ten_t t); 
ue_p	ue_mk_SystemFunction(ue_p function, ue_p (*pf)(const ue_p state));
ue_p	ue_mk_PureFunction(ue_p function, ue_p env);
ue_p	ue_mk_Data		(void);
ue_p	ue_mk_LumpShell	(ue_p t, size_t length); 
ue_p	ue_mk_Lump		(ue_p t, size_t length, ...); 

#define ue_cp_Symbol(s)	(s) /* ue_mk_Symbol would return the same s */
#define ue_cp_String(s)	(ue_mk_String(ue_get_String(s)))
ue_p	ue_cp_Number	(ue_p u);
ue_p	ue_cp_Tensor	(ue_p u);
ue_p	ue_cp_Function	(ue_p u);
ue_p	ue_cp_Lump		(ue_p u);
ue_p	ue_cp			(ue_p u);

int		ue_cmp_Symbol	(const ue_p u1, const ue_p u2); 
int		ue_cmp_String	(const ue_p u1, const ue_p u2); 
int		ue_cmp_Number	(const ue_p u1, const ue_p u2);
int		ue_cmp_Tensor	(const ue_p u1, const ue_p u2);
int		ue_cmp_Function	(const ue_p u1, const ue_p u2);
int		ue_cmp_Data		(const ue_p u1, const ue_p u2);
int		ue_cmp_Lump		(const ue_p u1, const ue_p u2);
int		ue_cmp			(const ue_p u1, const ue_p u2);

bool	ue_Replace		(ue_p *u,
							bool (*fp)(const ue_p, const ue_p),
							ue_p var, ue_p val);
bool	ue_ReplaceList	(ue_p *u,
							bool (*fp)(const ue_p, const ue_p),
							ue_p var, ue_p val);
bool	ue_is_Same		(const ue_p u1, const ue_p u2);
#define ue_is_SameSymbol(a, b)	(assert(ue_is_Symbol(a) && ue_is_Symbol(b)),\
								((a) == (b)))


ue_p	ue_part_scattered	(const ue_p u, const ten_t mi);
ue_p	ue_part_single		(const ue_p u, const ten_t mi);
ue_p	ue_part_structured	(const ue_p u, const ue_p spec);

ue_p	ue_set_part_scattered	(ue_p *u, const ten_t mi, const ue_p vals);
ue_p	ue_set_part_single		(ue_p *u, const ten_t mi, const num_t val);
ue_p	ue_set_part_structured	(ue_p *u, const ue_p spec, const ue_p vals);

ue_p	ue_ConstantArray_Tensor(const ue_p u);
ue_p	ue_Join			(const ue_p u);
ue_p	ue_OuterList	(const ue_p u);
ue_p	ue_Range		(const ue_p u);
ue_p	ue_Sort			(const ue_p u);

ue_p	ue_Coerce		(ue_p u, ntype type);
ntype	ue_CCoerceType	(ue_p prec);

ue_p	ue_LumpAppendTo	(ue_p comp, ue_p u);

ue_p	ue_get_Type		(ue_p m);

void 	ue_Print		(FILE *f, ue_p u);
void 	ue_PrintRaw		(FILE *f, ue_p u, bool ps);

size_t	ue_length		(ue_p);
ue_p	ue_Length		(ue_p);

/* ue_mk_int_str */
/* ue_mk_real_str */
ue_p 	ue_StringToInteger(char const *str);
ue_p 	ue_StringToReal	(char const *str);

#define	cons(a, b)			ue_mk_Lump(sList, 2, a, b)
#define car(c)				ue_get_LumpElem(c, 1)
#define cdr(c)				ue_get_LumpElem(c, 2)
#define ue_is_List(e)		((ue_is_Lump(e)) &&\
							 (ue_get_LumpType(e) == sList))	
#define ue_is_EmptyList(u)	(ue_is_List(u) && (ue_get_LumpLength(u) == 0))


ue_p ue_mk_TensorFromList(const ue_p in);
ue_p ue_TensorTranspose	(ue_p t, size_t length, size_t *order); 


/*num*/
#define ue_is_mi(e)			((ue_is_Number(e) && (ue_get_NumberType(e) == MI))\
							? true : false)
#define ue_is_mr(e)			((ue_is_Number(e) && (ue_get_NumberType(e)) == MR)\
						 	? true : false)
#define ue_is_bi(e)			((ue_is_Number(e) && (ue_get_NumberType(e)) == BI)\
							? true : false)
#define ue_is_br(e)			((ue_is_Number(e) && (ue_get_NumberType(e)) == BR)\
							? true : false)

#define ue_is_NumberExact(e)	((ue_is_mi(e) || ue_is_bi(e)) ? true : false)
#define ue_is_NumberInexact(e)	((ue_is_mr(e) || ue_is_br(e)) ? true : false)

#define ue_get_mi(e)		(assert(ue_is_mi(e)), num_get_mi(ue_get_Number(e)))
#define ue_get_mr(e)		(assert(ue_is_mr(e)), num_get_mr(ue_get_Number(e)))
#define ue_get_bi(e)		(assert(ue_is_mi(e)), num_get_bi(ue_get_Number(e)))
#define ue_get_br(e)		(assert(ue_is_br(e)), num_get_br(ue_get_Number(e)))
/* it's not that usefull to define the above for tensors */

#define ue_is_TensorExact(e)	((ue_is_Tensor(e) &&\
								 ten_is_exact(ue_get_Tensor(e)))\
								 ? true : false)
#define ue_is_TensorInexact(e)	((ue_is_Tensor(e) &&\
								 ten_is_inexact(ue_get_Tensor(e)))\
								 ? true : false)

#define ue_is_Exact(e)		(ue_is_NumberExact(e) || ue_is_TensorExact(e))
#define ue_is_Inexact(e)	(ue_is_NumberInexact(e) || ue_is_TensorInexact(e))

bool	ue_is_Zero		(const ue_p u);

void	dp				(ue_p u);

ue_p	ue_MemoryInUse	(void);
ue_p	ue_ByteCount	(ue_p u);


extern ue_p sE;
extern ue_p sNegativeInfinity;
extern ue_p sNotANumber;
extern ue_p sPositiveInfinity;
extern ue_p sPi;

bool	ue_is_SymbolCoercable	(ue_p u);

#include "uex_math.h"
#include "datastructures.h"
#include "streams.h"

/*@=exportlocal@*/
#endif

