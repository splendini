#include <locale.h>
#include <math.h>

#include "uex.h"
#include "uex_math.h"
#include "symboltable.h"

ue_p sSymbol;
ue_p sString;
ue_p sMInt;
ue_p sMReal;
ue_p sBInt;
ue_p sBReal;
ue_p sTensor;
ue_p sMIntTensor;
ue_p sMRealTensor;
ue_p sBIntTensor;
ue_p sBRealTensor;

ue_p sSystemFunction;
ue_p sInterpretedFunction;

ue_p sData;

ue_p sList;
ue_p sEmptyList;
ue_p sSymbolTable;

ue_p sCoerce;
ue_p sQuoteCoercion;

ue_p sAll;
ue_p sDone;
ue_p sFailed;
ue_p sNull;
ue_p sQuit;
ue_p sRestart;

ue_p sSLocale;
ue_p symbolTable;

ue_p sE;
ue_p sNegativeInfinity;
ue_p sNotANumber;
ue_p sPositiveInfinity;
ue_p sPi;



#define ue_set_LumpMemory(e, l, m)	(ue_set_type(e, LUMP),\
									 (((ued(e).ld).length) = (l)),\
									 ((ued(e).ld).elems) = (m))

#ifdef UE_DEBUG

/* debug print */
void dp(ue_p u) {
	ue_Print(stdout, u);
}

#endif

static
int sign(int a) {
	return (0 < a) - (a < 0);
}

static inline
ue_p ue_mk_Raw(utype_t t) { 
	ue_p res;
	res = (ue_p) ue_Malloc(sizeof(struct ue_t));
	validateMemory(res, "ue_mk_Raw");
	memset(res, 0, sizeof(struct ue_t));
	ue_set_type(res, t);
	return res;
}

extern
ue_p ue_mk_Symbol(char const *name) {
	ue_p sym = NULL;

	ue_symboltable_lookup(name, &sym);
	if (sym) return sym;

	sym = ue_mk_Raw(SYMBOL);
	ue_set_SymbolName(sym, ue_strdup(name));
	ue_set_SymbolAttributes(sym, NOATTR);
	ue_symboltable_add(sym);

	return sym;
}

/*ue_cp_Symbol in *.h*/

extern
int ue_cmp_Symbol(const ue_p u1, const ue_p u2) {
	assert(ue_is_Symbol(u1) && ue_is_Symbol(u2));
	if (u1 == u2) return 0;
	return sign(strcmp(ue_get_SymbolName(u1), ue_get_SymbolName(u2)));
}

static
ue_p coerceSymbol(const ue_p u, const ntype toType) {
	assert(ue_is_Symbol(u));
	if (u == sPi) {
		if (toType == MR) {
			return ue_mk_Number(num_p_pi_mr);
		} else if (toType == BR) {
			return ue_mk_Number(num_p_pi_br());
		} else {
			return u;
		}
	} else if (u == sE) {
		if (toType == MR) {
			return ue_mk_Number(num_p_e_mr);
		} else if (toType == BR) {
			assert(false); /* we do not have BR */
			return ue_mk_Number(num_p_e_mr);
		} else {
			return u;
		}
	} else if (u == sNegativeInfinity) {
		if (toType == MR) {
			return ue_mk_Number(num_m_inf_mr);
		} else if (toType == BR) {
			assert(false); /* we do not have BR */
			return ue_mk_Number(num_m_inf_mr);
		} else {
			return u;
		}
	} else if (u == sNotANumber) {
		if (toType == MR) {
			return ue_mk_Number(num_nan_mr);
		} else if (toType == BR) {
			assert(false); /* we do not have BR */
			return ue_mk_Number(num_nan_mr);
		} else {
			return u;
		}
	} else if (u == sPositiveInfinity) {
		if (toType == MR) {
			return ue_mk_Number(num_p_inf_mr);
		} else if (toType == BR) {
			assert(false); /* we do not have BR */
			return ue_mk_Number(num_p_inf_mr);
		} else {
			return u;
		}
	}
	return u;
}

extern
bool ue_is_SymbolCoercable(const ue_p u) {
	if (u == sPi || u == sE || 
			u == sNegativeInfinity || u == sPositiveInfinity ||
			u == sNotANumber ) {
		return true;
	} else {
		return false;
	}
}

extern
ue_p ue_mk_String(char const *str) {
	char *t, *r, *s;
	ue_p res = ue_mk_Raw(STRING);
	s = ue_strdup(str);
	/* convert literal \n in "1\n2" to newline */
	/* http://stackoverflow.com/questions/18689839/convert-n-to-a-newline-character */
	for (t = r = s; *t != '\0'; t++) {
		if (*t != '\\')
			*r++ = *t;
		else {
			switch (*++t) {
				case 'n':
					*r++ = '\n';
					break;
				case 't':
					*r++ = '\t';
					break;
				/* TODO: what about 'r', 'b' and such */
				default:
					/*default leave /XYZ in*/
					*r++ = *t;
			}
		}
	}
	*r = '\0';
	ue_set_String(res, s);
	return res;
}
/*ue_cp_String in *.h*/

extern
int ue_cmp_String(const ue_p u1, const ue_p u2) {
	assert(ue_is_String(u1) && ue_is_String(u2));
	return sign(strcmp(ue_get_String(u1), ue_get_String(u2)));
}

/* ä can have length > 1 */
extern
size_t ue_get_StringLength(ue_p u) {
	assert(ue_is_String(u));
	return strlen(ue_get_String(u));
}

/* ä is one character */
extern
ue_p ue_get_StringCharacters(ue_p u) {
	size_t l;
	ue_p res;
	assert(ue_is_String(u));
	l = mbstowcs(NULL, ue_get_String(u), 0);
	if (l > (size_t) MI_MAX)
		return sFailed;
	res = ue_mk_mi_si( (mi_t) l);
	return res;
}


static
ue_p ue_join_String(const ue_p u) {
	size_t l, i, n;
	char *s, *dest, *src;
	ue_p res, tmp;

	if (!ue_is_Lump(u) || (ue_length(u) < 1))
		return sFailed;

	l = 0;
	for (i = 1; i <= ue_length(u); i++) {
		tmp = ue_get_LumpElem(u, i);
		if (!ue_is_String(tmp)) return u;
		l += ue_get_StringLength(tmp);
	}
	n = i - 1;

	dest = (char *) ue_Malloc((l + 1) * sizeof(char));
	validateMemory(dest, "ue_join_String");

	s = dest;
	dest[0] = '\0';
	for (i = 1; i <= n; i++) {
		tmp = ue_get_LumpElem(u, i);
		l = ue_get_StringLength(tmp);
		src = ue_get_String(tmp);
		while (*src != '\0')
			*dest++ = *src++;

	}
	*dest++ = '\0';

	res = ue_mk_Raw(STRING);
	ue_set_String(res, s);
	return res;
}

static
ue_p ue_part_scattered_String(const ue_p u, const ten_t mi) {
	mi_t vi;
	mi_t *v;
	size_t lr, l1, i, j;
	size_t rank;
	char *s;
	ue_p res;

	if(!(ue_is_String(u) && ten_is_mi(mi)))
		return sFailed;

	lr = ten_get_dimensions(mi)[0];
	if (lr == 0) {
		/*convert to {1} and set flag */
		return u;
	}

	rank = ten_get_rank(mi);
	if (rank > 1) {
		if (!(rank == 2 && ten_get_dimensions(mi)[1] == 1)) {
			/* for now */
			return u;
		}
	}
 
	l1 = ue_length(u);
	v = (mi_t *) ten_get_values(mi);
	s = (char *) ue_Malloc(sizeof(char) * (lr + 1));
	validateMemory(s, "ue_part_scattered_String");

	for (i = 0, j = 0; i < lr; i++, j++) {
		vi = v[i];
		if (vi < 0) vi = vi + l1 + 1;
		if ( (0 < vi) && ((size_t) vi <= l1) ) {
			s[j] = ue_get_String(u)[ (size_t) vi - 1];
		} else {	
			return u;
		}
	}

	s[lr] = '\0';
	res = ue_mk_String(s);

	return res;

}

extern
ue_p ue_mk_Number(num_t n) {
	ue_p res;
	assert(num_is_valid(n));
	res = ue_mk_Raw(NUMBER);
	ue_set_Number(res, n);
	return res;
}

extern
ue_p ue_cp_Number(const ue_p u) {
	num_t n;
	assert(ue_is_Number(u));
	n = num_cp(ue_get_Number(u));
	return ue_mk_Number(n);
}

extern
int ue_cmp_Number(const ue_p u1, const ue_p u2) {
	assert(ue_is_Number(u1) && ue_is_Number(u2));
	return num_cmp(ue_get_Number(u1), ue_get_Number(u2));
}

static
ue_p coerceNumber(const ue_p u, const ntype toType) {
	num_t n;
	assert(ue_is_Number(u));
	n = num_coerce(ue_get_Number(u), toType);
	return ue_mk_Number(n);
}

extern
ue_p ue_mk_mi_si(long i) {
	ue_p res = ue_mk_Raw(NUMBER);
	ue_set_Number(res, num_mk_mi_si(i));
	return res;
}

extern
ue_p ue_mk_mr_d(double d) {
	ue_p res = ue_mk_Raw(NUMBER);
	ue_set_Number(res, num_mk_mr_d(d));
	return res;
}

extern
ue_p ue_mk_bi_str(char const *str) {
	ue_p res = ue_mk_Raw(NUMBER);
	ue_set_Number(res, num_mk_bi_str(str));
	return res;
}

extern
ue_p ue_mk_br_str(char const *str) {
	ue_p res = ue_mk_Raw(NUMBER);
	ue_set_Number(res, num_mk_br_str(str));
	return res;
}

extern
ue_p ue_mk_Tensor(ten_t t) {
	ue_p res = ue_mk_Raw(TENSOR);
	assert(ten_is_valid(t));
	ue_set_Tensor(res, t);
	return res;
}

extern
ue_p ue_mk_Tensor_ue(const ntype tType, size_t rank, size_t *dims, void *data) {
	ue_p res;
	ten_t t;

	assert(numtype_is_valid(tType));

	res = ue_mk_Raw(TENSOR);
	t = ten_mk(tType, rank, dims, data);
	if (ten_is_valid(t)) {
		ue_set_Tensor(res, t);
		return res;
	}
	return sFailed;
}

static
ue_p coerceTensor(ue_p u, const ntype toType) {
	ten_t t;

	assert(ue_is_Tensor(u));
	assert(numtype_is_valid(toType));

	ten_coerce(&t, ue_get_Tensor(u), toType);
	if (!ten_is_valid(t)) return sFailed;
	return ue_mk_Tensor(t);
}

extern
ue_p ue_cp_Tensor(ue_p u) { 
    ten_t t;
    assert(ue_is_Tensor(u));
    ten_cp(&t, ue_get_Tensor(u));
	if (!ten_is_valid(t)) return sFailed;
	return ue_mk_Tensor(t);
}

extern
int ue_cmp_Tensor(const ue_p u1, const ue_p u2) {
	assert(ue_is_Tensor(u1) && ue_is_Tensor(u2));
	return ten_cmp(ue_get_Tensor(u1), ue_get_Tensor(u2));
}


#define TENSOR_SETDATA_TYPE(type)\
static \
void ueTensor_set_Data2_##type(size_t rank, size_t *dims, size_t len, type##_t *data, ue_p u) {\
	size_t i, nVals, sliceSize;\
	ue_p s, elm;\
	mi_t *mit;\
	mr_t *mrt;\
	bi_t *bit;\
	br_t *brt;\
	ten_t t;\
	num_t n;\
\
	if (ue_is_Tensor(u)) {\
		t = ue_get_Tensor(u);\
		/* coerce(data, t)*/\
		nVals = ten_get_number_of_values(t);\
		switch (ten_get_type(t)) {\
			case MI :\
				mit = (mi_t *) ten_get_values(t);\
				for (i = 0; i < nVals; i++)\
					 coerce_##type##_mi(data[i], mit[i]);\
				break;\
			case MR :\
				mrt = (mr_t *) ten_get_values(t);\
				for (i = 0; i < nVals; i++)\
					 coerce_##type##_mr(data[i], mrt[i]);\
				break;\
			case BI :\
				bit = (bi_t *) ten_get_values(t);\
				for (i = 0; i < nVals; i++)\
					 coerce_##type##_bi(data[i], bit[i]);\
				break;\
			case BR :\
				brt = (br_t *) ten_get_values(t);\
				for (i = 0; i < nVals; i++)\
					 coerce_##type##_br(data[i], brt[i]);\
				break;\
		default :\
				noCase("TENSOR_SETDATA_TYPE TENSOR");\
				return;\
		}\
	return;\
	}\
\
	assert(!ue_is_Tensor(u));\
	if (rank == 0) {\
		n = ue_get_Number(u);\
		switch (num_get_type(n)) {\
			case MI :\
				coerce_##type##_mi(data[0], num_get_mi(n));\
				break;\
			case MR:\
				coerce_##type##_mr(data[0], num_get_mr(n));\
				break;\
			case BI:\
				coerce_##type##_bi(data[0], num_get_bi(n));\
				break;\
			case BR:\
				coerce_##type##_br(data[0], num_get_br(n));\
				break;\
			default :\
				noCase("TENSOR_SETDATA_TYPE NUM");\
				return;\
		}\
		return;\
	}\
\
	/* we have a vector */\
	if (rank == 1) {\
		for(i = 0; i < len; i++ ) {\
			elm = ue_get_LumpElem(u, i + 1);\
			if (ue_is_Number(elm)) {\
				n = ue_get_Number(elm);\
				switch (num_get_type(n)) {\
					case MI :\
						coerce_##type##_mi(data[i], num_get_mi(n));\
						break;\
					case MR:\
						coerce_##type##_mr(data[i], num_get_mr(n));\
						break;\
					case BI:\
						coerce_##type##_bi(data[i], num_get_bi(n));\
						break;\
					case BR:\
						coerce_##type##_br(data[i], num_get_br(n));\
						break;\
					default :\
						noCase("TENSOR_SETDATA_TYPE LUMP ELEM");\
						return;\
				}\
			} else if (ue_is_Tensor(elm)) {\
				t = ue_get_Tensor(elm);\
				if (ten_get_rank(t) == 0) {\
					switch (ten_get_type(n)) {\
						case MI :\
							mit = (mi_t *) ten_get_values(t);\
							coerce_##type##_mi(data[i], mit[0]);\
							break;\
						case MR:\
							mrt = (mr_t *) ten_get_values(t);\
							coerce_##type##_mr(data[i], mrt[0]);\
							break;\
						case BI:\
							bit = (bi_t *) ten_get_values(t);\
							coerce_##type##_bi(data[i], bit[0]);\
							break;\
						case BR:\
							brt = (br_t *) ten_get_values(t);\
							coerce_##type##_br(data[i], brt[0]);\
							break;\
						default :\
							noCase("TENSOR_SETDATA_TYPE LUMP ELEM");\
							return;\
					}\
				} else {\
					return;\
				}\
			} else {\
				return;\
			}\
		}\
		return;\
	}\
\
	/* rest of dimensions */\
	sliceSize = 1;\
	for(i = 1; i < rank; i++) {\
		sliceSize *= dims[i];\
	}\
\
	/* recursive part */\
	for(i = 0; i < dims[0]; i++) {\
		s = ue_get_LumpElem(u, i + 1);\
		ueTensor_set_Data2_##type(rank-1, &dims[1], sliceSize, &data[i*sliceSize], s);\
	}\
}\


TENSOR_SETDATA_TYPE(mi)
TENSOR_SETDATA_TYPE(mr)
TENSOR_SETDATA_TYPE(bi)
TENSOR_SETDATA_TYPE(br)

/* TODO: this should return success/failure */
static
void ue_set_TensorData(size_t rank, size_t *dims, size_t len, void *data, ue_p u, const ntype t) {

	assert(len > 0);
	assert(numtype_is_valid(t));

	switch (t) {
		case (MI) :
			ueTensor_set_Data2_mi(rank, dims, len, (mi_t *) data, u);
			break;

		case (MR) :
			ueTensor_set_Data2_mr(rank, dims, len, (mr_t *) data, u);
			break;

		case (BI) :
			ueTensor_set_Data2_bi(rank, dims, len, (bi_t *) data, u);
			break;

		case (BR) :
			ueTensor_set_Data2_br(rank, dims, len, (br_t *) data, u);
			break;

		default :
			noCase("ue_set_TensorData");
			return;
	}
}

static
bool findTensorDimensionsAndType(ue_p u, size_t maxRank, size_t rank, size_t **dims, ntype *type) {

	size_t i, len, temp, thisRank;
	ue_p u1, elem, slice;
	ten_t t;
	ntype nt;

	assert(numtype_is_valid(*type));

	if (ue_is_Tensor(u)) {
		t = ue_get_Tensor(u);
		switch (ten_get_type(t)) {
			case MI:
				if (*type == MI)
					*type = MI;
				break;

			case MR:
				if (*type < MR)
					*type = MR;
				break;

			case BI:
				if (*type < BI)
					*type = BI;
				break;

			case BR:
				if (*type < BR)
					*type = BR;
				break;

			default :
				return false;
		}
		thisRank = ten_get_rank(t);
		for (i = 0; i < thisRank; i++) {
			temp = ten_get_dimensions(t)[i];
			(*dims)[maxRank - rank + i] = temp;
		}
		return true;
	}

	if (rank == 1 && !ue_is_Tensor(u)) {
		assert(ue_is_Lump(u));
		len = ue_get_LumpLength(u);
		for (i = 1; i <= len; i++) {
			elem = ue_get_LumpElem(u, i);
			nt = -1;
			if (ue_is_Number(elem)) {
				nt = ue_get_NumberType(elem);
			}
			if (ue_is_Tensor(elem)) {
				t = ue_get_Tensor(elem);
				if (ten_get_rank(t) == 0) {
					nt = ten_get_type(t);
				}
			}
			switch (nt) {
				case MI:
					if (*type == MI)
						*type = MI;
					continue;

				case MR:
					if (*type < MR)
						*type = MR;
					continue;

				case BI:
					if (*type < BI)
						*type = BI;
					continue;

				case BR:
					if (*type < BR)
						*type = BR;
					continue;

				default :
					return false;
			}
		};

		(*dims)[maxRank - 1] = len; 
		return true;
	}

	u1 = ue_get_LumpElem(u, 1);
	len = ue_length(u1);

	for (i = 1; i <= ue_get_LumpLength(u); i++) {
		slice = ue_get_LumpElem(u, i);
		if ((len != ue_length(slice)) || !findTensorDimensionsAndType(slice, maxRank, rank - 1, dims, type)) {
			return false;
		}
	}
	(*dims)[maxRank - rank] = ue_length(u);
	return true;
}

static
bool ue_is_UETensor(ue_p u, size_t *rank, size_t **dims, size_t *nVals, ntype *type) {

	size_t i;
	ue_p e;
	ten_t t;

	/* we start by assuming MI tensor */
	*type = MI;

	if (ue_is_Tensor(u)) {
		t = ue_get_Tensor(u);
		*rank = ten_get_rank(t);
		(*dims) = (size_t *) ue_Malloc((*rank) * sizeof(size_t));
		validateMemory(*dims, "ue_is_UETensor");
		for (i = 0; i < *rank; i++)
			(*dims)[i] = ten_get_dimensions(t)[i];
		*nVals = ten_get_number_of_values(t);
		*type = ten_get_type(t);
		return true;
	}

	*rank = 0;
	(*nVals) = 1;
	 *dims = NULL;

	if (ue_is_Number(u)) {
		*type = ue_get_NumberType(u);
		(*dims) = (size_t *) ue_Malloc(sizeof(size_t));
		validateMemory(*dims, "ue_is_UETensor");
		**dims = 0;
		return true;
	}

	/* u = f(1,2) */
	if (!ue_is_Lump(u) || !ue_is_List(u))
		return false;

	e = u;
	while (ue_is_List(e)) {
		if (ue_is_EmptyList(e)) {
			*type = NOTYPE;
			(*nVals) = 0;
			return true;
		}
		(*rank)++;
		e = ue_get_LumpElem(e, 1);
	}

	if (ue_is_Tensor(e)) {
		t = ue_get_Tensor(e);
		(*rank) += ten_get_rank(t);
	}

	(*dims) = (size_t *) ue_Malloc((*rank) * sizeof(size_t));
	validateMemory(*dims, "ue_is_UETensor");

	if (!findTensorDimensionsAndType(u, *rank, *rank, dims, type))
		return false;

	for(i = 0; i < (*rank); i++ ) {
		(*nVals) *= (*dims)[i];
	}

	return true;
}


ue_p ue_mk_TensorFromList(const ue_p in) {
	ntype type = NOTYPE;
	size_t rank = 0, nVals = 0;
	size_t *dims = NULL;
	void *data;
	ue_p tensor;

	if (ue_is_Tensor(in))
		return in;

	if (!(ue_is_List(in) || ue_is_Number(in)))
		return in;

	if (!ue_is_UETensor(in, &rank, &dims, &nVals, &type))
		return in;

	if(!tentype_is_valid(type))
		return in;

	switch ( type ) {
		case MI:
			data = (mi_t *) ue_Malloc( nVals * sizeof(mi_t));
			validateMemory(data, "ue_mk_TensorFromList");
			ue_set_TensorData(rank, dims, nVals, data, in, MI);
			tensor = ue_mk_Tensor_ue(MI, rank, dims, data);
			break;
	
		case MR:
			data = (mr_t *) ue_Malloc( nVals * sizeof(mr_t));
			validateMemory(data, "ue_mk_TensorFromList");
			ue_set_TensorData(rank, dims, nVals, data, in, MR);
			tensor = ue_mk_Tensor_ue(MR, rank, dims, data);
			break;

		case BI:
			data = (bi_t *) ue_Malloc( nVals * sizeof(bi_t));
			validateMemory(data, "ue_mk_TensorFromList");
			ue_set_TensorData(rank, dims, nVals, data, in, BI);
			tensor = ue_mk_Tensor_ue(BI, rank, dims, data);
			break;
	
		case BR:
			data = (br_t *) ue_Malloc( nVals * sizeof(br_t));
			validateMemory(data, "ue_mk_TensorFromList");
			ue_set_TensorData(rank, dims, nVals, data, in, BR);
			tensor = ue_mk_Tensor_ue(BR, rank, dims, data);
			break;

	default :
			/*message*/
			assert(false);
			return sFailed;
	}
	return tensor;
}


/*
ue_p ueTensorToList(const ue_p u) {

	size_t i, rank;
	size_t *dims;
	ue_p u;

	assert(ue_is_Tensor(u));

	rank = ue_get_TensorRank(u);
	*dims = ue_get_TensorDimensions(u);
	nVals = ue_get_TensorNumberOfValues(u);

	u = typedUex(sList, rank, dims);

}
*/

extern
ue_p ue_TensorTranspose(ue_p u, size_t length, size_t *order) {
	ten_t t;

	assert(ue_is_Tensor(u));
	ten_transpose(&t, ue_get_Tensor(u), length, order);
	if (ten_is_valid(t)) return ue_mk_Tensor(t);

	return sFailed;
}

static
ue_p ue_part_single_Tensor(const ue_p u, const ten_t st) {
	ten_t it;
	num_t n;
	mi_t *index;

	if(!(ue_is_Tensor(u) && ten_is_mi(st))) {
		return sFailed;
	}

	it = ue_get_Tensor(u);
	index = ten_get_values(st);

	ten_part_single(&n, it, index);
	if (num_is_valid(n)) return ue_mk_Number(n);

	return sFailed;
}

static
ue_p ue_set_part_single_Tensor(ue_p *u, const ten_t st, const num_t v) {
	ten_t it;
	num_t n;
	ntype nt;
	mi_t *index;

	if(!(ue_is_Tensor(*u) && ten_is_mi(st) && num_is_valid(v))) {
		return sFailed;
	}

	it = ue_get_Tensor(*u);

	nt = ten_get_type(it);
	num_common_type(&nt, num_get_type(v));

	ten_coerce(&it, it, nt);
	n = num_coerce(v, nt);

	index = ten_get_values(st);

	ten_set_part_single(&it, index, n);
	if (ten_is_valid(it)) {
		ue_set_Tensor(*u, it);
		return *u;
	}

	return sFailed;
}

static
ue_p ue_part_scattered_Tensor(const ue_p u, const ten_t st) {
	ten_t rt;

	if(!(ue_is_Tensor(u) && ten_is_mi(st))) {
		return sFailed;
	}

	ten_part_scattered(&rt, ue_get_Tensor(u), st);
	if (ten_is_valid(rt)) return ue_mk_Tensor(rt);

	return sFailed;
}

static
ue_p ue_set_part_scattered_Tensor(ue_p *u, const ten_t st, const ue_p v) {
	ue_p values;
	ten_t it, vt;
	ntype nt;

	if(!(ue_is_Tensor(*u) && ten_is_mi(st))) {
		return sFailed;
	}

	it = ue_get_Tensor(*u);

	values = v;
	if (ue_is_List(v))
		values = ue_mk_TensorFromList(v);

	if (!ue_is_Tensor(values))
		return sFailed;

	vt = ue_get_Tensor(values);

	if (ten_get_number_of_values(vt) != ten_get_dimensions(st)[0])
		return sFailed;

	nt = ten_get_type(it);
	num_common_type(&nt, ten_get_type(vt));

	ten_coerce(&it, it, nt);
	ten_coerce(&vt, vt, nt);

	ten_set_part_scattered(&it, st, vt);
	if (ten_is_valid(it)) {
		ue_set_Tensor(*u, it);
		return *u;
	}

	return sFailed;
}

static
ue_p ue_part_structured_Tensor(const ue_p u, size_t n, const ten_t * spec) {
	ten_t rt;

	ten_part_structured(&rt, ue_get_Tensor(u), n, spec);
	if (ten_is_valid(rt)) return ue_mk_Tensor(rt);

	return sFailed;
}

static
ue_p ue_set_part_structured_Tensor(ue_p *u, size_t n, const ten_t * mi, const ten_t v) {
	ten_t it = ue_get_Tensor(*u);

	ten_set_part_structured(&it, n, mi, v);
	if (ten_is_valid(it)) {
		ue_set_Tensor(*u, it);
		return *u;
	}

	return sFailed;
}


extern
ue_p ue_mk_SystemFunction(ue_p function, ue_p (*fp)(const ue_p state)) {
	ue_p res = ue_mk_Raw(FUNCTION);
	ue_set_FunctionUex(res, function);
	ue_set_SystemFunctionFP(res, fp);
	ue_set_FunctionAttributes(res, NOATTR);
	return res;
}

extern
ue_p ue_mk_PureFunction(ue_p function, ue_p env) {
	ue_p res = ue_mk_Raw(FUNCTION);
	ue_set_FunctionUex(res, function);
	ue_set_PureFunctionEnv(res, env);
	ue_set_FunctionAttributes(res, NOATTR);
	return res;
}

extern
ue_p ue_cp_Function(ue_p u) {
	ue_p res;
	assert(ue_is_Function(u));
	res = ue_mk_Raw(FUNCTION);
	ue_set_FunctionUex(res, ue_cp(ue_get_FunctionUex(u)));
	ue_set_FunctionAttributes(res, ue_get_FunctionAttributes(u));
	switch(ue_get_FunctionType(u)) {
		case HOST:
			ue_set_SystemFunctionFP(res, ue_get_SystemFunctionFP(u));
			break;
		case PURE:
			ue_set_PureFunctionEnv(res, ue_get_PureFunctionEnv(u));
			break;
		default:
			noCase("ue_get_Type fun");
			return sFailed;
		}
	return res;	
}

static
ue_p coercePureFunction(ue_p u, const ntype toType) {
	ue_p f, fun, body;
 
	assert(ue_is_PureFunction(u));
	assert(numtype_is_valid(toType));

	f = ue_cp_Function(u);
	fun = ue_get_FunctionUex(f);
	body = ue_get_LumpElem(fun, 2);
	body = ue_Coerce(body, toType);
	ue_set_LumpElem(fun, 2, body);
	return f;
}

extern
int ue_cmp_Function(const ue_p u1, const ue_p u2) {
	assert(ue_is_Function(u1) && ue_is_Function(u2));
	if (ue_get_FunctionType(u1) == ue_get_FunctionType(u2))
		return ue_cmp(ue_get_FunctionUex(u1), ue_get_FunctionUex(u2));

	if (ue_get_FunctionType(u1) < ue_get_FunctionType(u2))
		return -1;

	return 1;

}

extern
ue_p ue_mk_Data(void) {
	ue_p u = ue_mk_Raw(DATA);
	dataops_t *ops;

	ops = (dataops_t *) ue_Malloc(sizeof(struct dataops_t));
	validateMemory(ops, "ue_mk_Data");

	ops->bytecount = NULL;
	ops->cmp = NULL;
	ops->coerce = NULL;
	ops->cp = NULL;
	ops->hash = NULL;
	ops->length = NULL;
	ops->printraw = NULL;

	ue_set_DataOperators(u, ops);

	return u;
}

static inline
size_t ue_size_data(const ue_p u) {
	assert(ue_is_Data(u));
	if (ue_get_DataOperators(u)->bytecount) {
		return ue_get_DataOperators(u)->bytecount(u);
	}
	assert(false);
	return 0;
}

static inline
ue_p coerceData(const ue_p u, const ntype toType) {
	assert(ue_is_Data(u));
	if (ue_get_DataOperators(u)->coerce) {
		return ue_get_DataOperators(u)->coerce(u, toType);
	}
	assert(false);
	return u;
}

extern
int ue_cmp_Data(const ue_p u1, const ue_p u2) {
	assert(ue_is_Data(u1) && ue_is_Data(u2));
	if (ue_get_DataOperators(u1)->cmp) {
		return ue_get_DataOperators(u1)->cmp(u1, u2);
	}
	assert(false);
	return 0;
}

static inline
ue_p ue_cp_Data(const ue_p u) {
	assert(ue_is_Data(u));
	if (ue_get_DataOperators(u)->cp) {
		return ue_get_DataOperators(u)->cp(u);
	}
	assert(false);
	return u;
}

extern
void ue_hash_Data(unsigned int *hv, const ue_p u) {
	assert(ue_is_Data(u));
	if (ue_get_DataOperators(u)->hash) {
		ue_get_DataOperators(u)->hash(hv, u);
		return;
	}
	assert(false);
	*hv = 0;
}

static inline
size_t ue_get_DataLength(const ue_p u) {
	assert(ue_is_Data(u));
	if (ue_get_DataOperators(u)->length) {
		return ue_get_DataOperators(u)->length(u);
	}
	assert(false);
	return 0;
}

static inline
void data_print_raw(FILE *f, ue_p m, bool ps) {
	assert(ue_is_Data(m));
	if (ue_get_DataOperators(m)->printraw) {
		ue_get_DataOperators(m)->printraw(f, m, ps);
	} else {
		ue_PrintRaw(f, ue_get_DataType(m), ps);
		fprintf(f, "(");
		fprintf(f, " - Data - ");
		fprintf(f, ")");
	}
}

extern
ue_p ue_mk_LumpShell(ue_p t, size_t length) {
	ue_p res;
	ue_p *mem;

	#ifdef UE_DEBUG
	size_t i;
	#endif

	assert(length < (size_t) MI_MAX);

	res = ue_mk_Raw(LUMP);
	mem = (ue_p *) ue_Malloc((length + 1) * sizeof(struct ue_t)); 
	validateMemory(mem, "ue_mk_LumpShell");

	ue_set_LumpMemory(res, length, mem); 

	ue_set_LumpType(res, t); 

	#ifdef UE_DEBUG
	for (i = 1; i <= length; i++) {
		mem[i] = sNull;
	}
	#endif

	return res;
}

/*
 ue_mk_Lump(type, n, e1, ..., en) 
 n ues + 1 type, start from 0 -> n
*/
extern
ue_p ue_mk_Lump(ue_p t, size_t length, ...) {
	va_list ap;
	size_t i;
	ue_p res, tmp;

	res = ue_mk_LumpShell(t, length);

	va_start(ap, length);

	for (i = 1; i <= length; i++) {
		tmp = va_arg(ap, ue_p);
		assert(tmp);
		/* for unique compound use ue_cp_Lump */
		ue_set_LumpElem(res, i, tmp);
	}

	va_end (ap);

	return res;
}

static
ue_p ue_LumpResize(ue_p u, size_t newLength) {
	ue_p *mem;

	assert(ue_is_Lump(u));
	assert( newLength < (size_t) MI_MAX);

	mem = ue_get_LumpElems(u);
	mem = (ue_p *) ue_Realloc(mem, newLength * sizeof(struct ue_t));
	validateMemory(mem, "ue_LumpResize");

	ue_set_LumpMemory(u, newLength, mem); 

	return u;
}

extern
ue_p ue_cp_Lump(ue_p u) {
	size_t i, length;
	ue_p res;
	ue_p *mem;

	assert(ue_is_Lump(u));
	length = ue_get_LumpLength(u);

	res = ue_mk_Raw(LUMP);
	mem = (ue_p *) ue_Malloc((length + 1) * sizeof(struct ue_t)); 
	validateMemory(mem, "ue_cp_Lump");

	ue_set_LumpMemory(res, length, mem); 

	for (i = 0; i <= length; i++) {
		ue_set_LumpElem(res, i, ue_cp(ue_get_LumpElem(u, i)));
	}

	return res;
}

static
ue_p ue_join_Lump(const ue_p u) {
	size_t l;
	size_t i, j, pos;
	ue_p res, tmp = sEmptyList, type;

	if (!ue_is_Lump(u) || ue_length(u) < 1)
		return sFailed;

	l = 0;
	for (i = 1; i <= ue_length(u); i++) {
		tmp = ue_get_LumpElem(u, i);
		if (!ue_is_Lump(tmp)) return u;
		l += ue_length(tmp);
	}

	type = ue_get_LumpType(tmp);
	res = ue_mk_LumpShell(type, l);

	pos = 1;
	for (i = 1; i <= ue_length(u); i++) {
		tmp = ue_get_LumpElem(u, i);
		if (type != ue_get_LumpType(tmp)) return u;
		for (j = 1; j <= ue_length(tmp); j++, pos++) {
			ue_set_LumpElem(res, pos, ue_get_LumpElem(tmp, j)); 
		}
	}	

	return res;
}

static
ue_p ue_part_scattered_Lump(const ue_p u, const ten_t mi) {
	mi_t *v;
	mi_t vi;
	size_t lr, l1, i;
	size_t rank;
	ue_p res;

	if(!(ue_is_Lump(u) && ten_is_mi(mi)))
		return sFailed;

	lr = ten_get_dimensions(mi)[0];
	if (lr == 0) {
		/*convert to {1} and set flag */
		return u;
	}

	rank = ten_get_rank(mi);
	if (rank > 1) {
		/* for now */
		return u;
	}
 
	l1 = ue_length(u);
	v = (mi_t *) ten_get_values(mi);
	res = ue_mk_LumpShell(ue_get_LumpType(u), lr);
	for (i = 1; i <= lr; i++) {
		vi = v[i - 1];
		if (vi < 0) vi = vi + l1 + 1;
		if ( (0 <= vi) && ((size_t) vi <= l1) ) {
			ue_set_LumpElem(res, i, ue_get_LumpElem(u, (size_t) vi));
		} else {	
			return u;
		}
	}

	return res;

}

extern
int ue_cmp_Lump(const ue_p u1, const ue_p u2) {
	size_t i = 0, l1, l2;
	int last = 0;

	assert(ue_is_Lump(u1) && ue_is_Lump(u2));

	l1 = ue_get_LumpLength(u1);
	l2 = ue_get_LumpLength(u2);
	while ( i <= l1 && i <= l2 && !(last = ue_cmp(ue_get_LumpElem(u1,i), ue_get_LumpElem(u2,i)))) {
		i++;
	}
	if (l1 == l2) {
		return last;
	} else if ( l1 > l2 && last == 0) {
		return 1;
	} else if (l1 < l2 && last == 0) {
		return -1;
	} else {
		return last;
	}
}

extern
ue_p ue_LumpAppendTo(ue_p u, ue_p u1) {
	size_t l;

	assert(ue_is_Lump(u));
	/* appending to sEmptyList is not what is wanted here
 	* a new empty list must be created ue_mk_Lump(sList,0) and
 	* there we can append to. */
	assert(u != sEmptyList);

	l = ue_get_LumpLength(u) + 1;
	assert( l < (size_t) MI_MAX );

	u = ue_LumpResize(u, l);
	ue_set_LumpElem(u, l, u1);	

	return u;
}

static 
ue_p coerceLump(ue_p u, const ntype toType) {
	size_t i, length;
	ue_p cp, ui, type;
	bool cqf = false, cqr = false;

	assert(ue_is_Lump(u));
	length = ue_get_LumpLength(u);

	if ((length == 1) && (ue_get_Type(u) == sQuoteCoercion)) {
		return ue_get_LumpElem(u, 1);
	}

	cp = ue_cp_Lump(u);
	type = ue_get_LumpType(cp);
	if (ue_is_Symbol(type)) {
		cqf = (bool) ue_has_SymbolAttribute(type, QUOTEFIRSTCOERCION);
		cqr = (bool) ue_has_SymbolAttribute(type, QUOTERESTCOERCION);
	}

	for (i = 0; i <= length; i++) {
		ui = ue_get_LumpElem(cp, i);
		if (((i != 1 || !cqf) && (i == 1 || !cqr)))
			ui = ue_Coerce( ui, toType);
		ue_set_LumpElem(cp, i, ui);
	}

	return cp;
}



/********************************
general functions
********************************/

extern ue_p ue_get_Type(ue_p m) {
	switch(ue_get_type(m)) {
		case SYMBOL:		return sSymbol;
		case STRING:		return sString;

		case NUMBER:
			switch(ue_get_NumberType(m)) {
				case MI:	return sMInt;
				case MR:	return sMReal;
				case BI:	return sBInt;
				case BR:	return sBReal;
				default:	return sFailed;
			}

		case TENSOR:
			switch(ue_get_TensorType(m)) {
				case MI:	return sMIntTensor;
				case MR:	return sMRealTensor;
				case BI:	return sBIntTensor;
				case BR:	return sBRealTensor;
				default:	return sFailed;
			}

		case FUNCTION:
			switch(ue_get_FunctionType(m)) {
				case HOST:	return sSystemFunction;
				case PURE:	return sInterpretedFunction;
				default:	return sFailed;
			}

		case DATA:			return ue_get_DataType(m);

		case LUMP:			return ue_get_LumpType(m);

		default:
			noCase("ue_get_Type");
			return sFailed;
	}
}

extern
void ue_PrintRaw(FILE *f, ue_p m, bool ps) {
	size_t i;
	switch(ue_get_type(m) ) {
		case SYMBOL:
			fprintf(f, "%s", ue_get_SymbolName(m));
			break;

		case STRING:
			/* should the "" be printed? */
			if (ps)
				fprintf(f, "\"%s\"", ue_get_String(m));
			else 
				fprintf(f, "%s", ue_get_String(m));
			break;

		case NUMBER:
			num_print_raw(f, ue_get_Number(m));
			break;

		case TENSOR:
			ten_print_raw(f, ue_get_Tensor(m));
			break;

		case FUNCTION:
			ue_PrintRaw(f, ue_get_FunctionUex(m), ps);
			break;

		case DATA:
			data_print_raw(f, m, ps);
			break;

		case LUMP:
			ue_PrintRaw(f, ue_get_LumpType(m), ps);
			fprintf(f, "(");
			if (ue_get_LumpLength(m) >= 1) {
				ue_PrintRaw(f, ue_get_LumpElem(m,1), ps);
				for(i = 2; i <= ue_get_LumpLength(m); i++ ) {
					fprintf(f, ", ");
					ue_PrintRaw(f, ue_get_LumpElem(m,i), ps);
				}
			}
			fprintf(f, ")");
			break;

		default:
			noCase("uePrintRaw");
			break;
	}
}

extern
void ue_Print(FILE *f, ue_p e) {
	ue_PrintRaw(f, e, true);
	fprintf(f, "\n");
	/*TODO: check return value*/
	(void) fflush(f);
}

extern
int ue_cmp_Type(const ue_p u1, const ue_p u2) {
	if (ue_get_type(u1) < ue_get_type(u2)) return -1;
	if (ue_get_type(u1) > ue_get_type(u2)) return 1;
	return 0;
}

extern
int ue_cmp(const ue_p u1In, const ue_p u2In) {
	utype_t rt1, rt2;
	ue_p u1 = u1In, u2 = u2In;

	/* pointer equal */
	if (u1 == u2) return 0;

	rt1 = ue_get_type(u1);
	rt2 = ue_get_type(u2);

	if (rt1 == SYMBOL) {
		u1 = coerceSymbol(u1, MR);
		rt1 = ue_get_type(u1);
	};
	if (rt1 == NUMBER && ue_get_type(u2) == SYMBOL) {
		u2 = coerceSymbol(u2, MR);
		rt2 = ue_get_type(u2);
	}

	if (rt1 < rt2) return -1;
	if (rt1 > rt2) return  1;

	switch (rt1) {
		case SYMBOL:	return ue_cmp_Symbol	(u1, u2);
		case STRING:	return ue_cmp_String	(u1, u2);
		case NUMBER:	return ue_cmp_Number	(u1, u2);
		case TENSOR:	return ue_cmp_Tensor	(u1, u2);
		case FUNCTION:	return ue_cmp_Function	(u1, u2);
		case DATA:		return ue_cmp_Data		(u1, u2);
		case LUMP:		return ue_cmp_Lump		(u1, u2);

		default:
			noCase("ue_cmp");
			return 0;
	}
}

extern
bool ue_is_Same(const ue_p u1, const ue_p u2) {
	utype_t rt1;

	/* pointer equal */
	if (u1 == u2) return true;

	rt1 = ue_get_type(u1);

	if (rt1 != ue_get_type(u2)) return false;

	switch (rt1) {
		case NUMBER:
			return num_is_same(ue_get_Number(u1), ue_get_Number(u2));

		case TENSOR:
			return ten_is_same(ue_get_Tensor(u1), ue_get_Tensor(u2));

		default:
			return ue_cmp(u1, u2) == 0;
	}
}

static
int cmpue(const void *p1, const void *p2) {
	ue_p *u1 = (ue_p *) p1;
	ue_p *u2 = (ue_p *) p2;
	return ue_cmp(*u1, *u2);
}

extern
ue_p ue_Sort(const ue_p u) {
	size_t l;
	ue_p tmp;
	if(!ue_is_Lump(u)) return u;

	l = ue_get_LumpLength(u);
	if (l < 2) return u;
	tmp = ue_cp(u);
	qsort(&ue_get_LumpElems(tmp)[1], l, sizeof(ue_p), cmpue);
	return tmp;
}

extern
ue_p ue_Coerce(const ue_p u, const ntype toType) {	
	switch (ue_get_type(u)) {
		case SYMBOL:		return coerceSymbol(u, toType);

		case STRING:		return u;

		case NUMBER:		return coerceNumber(u, toType);
		case TENSOR:		return coerceTensor(u, toType);

		case FUNCTION:
			switch(ue_get_FunctionType(u)) {
				case HOST:	return u;
				case PURE:	return coercePureFunction(u, toType);
				default:
					noCase("ue_Coerce function");
					return sFailed;
			}

		case DATA:			return coerceData(u, toType);

		case LUMP:			return coerceLump(u, toType);
		default:
			noCase("ue_Coerce");
			return sFailed;
	}
}

extern
ntype ue_CCoerceType(ue_p prec) {
	ntype toType;
	if (prec == sMInt || prec == sMIntTensor) {
		toType = MI;
	} else if (prec == sMReal || prec == sMRealTensor) {
		toType = MR;
	} else if (prec == sBInt || prec == sBIntTensor) {
		toType = BI;
	} else if (prec == sBReal || prec == sBRealTensor) {
		toType = BR;
	} else {
		toType = (ntype) NUMCOUNT;
	}
	return toType;
}

extern
ue_p ue_cp(ue_p u) {
	switch (ue_get_type(u)) {
		case SYMBOL:		return ue_cp_Symbol(u);
		case STRING:		return ue_cp_String(u);
		case NUMBER:		return ue_cp_Number(u);
		case TENSOR:		return ue_cp_Tensor(u);
		case FUNCTION:		return ue_cp_Function(u);
		case DATA:			return ue_cp_Data(u);
		case LUMP:			return ue_cp_Lump(u);

		default:
			noCase("ue_cp");
			return sFailed;
	}
}

extern
ue_p ue_Join(const ue_p u) {
	ue_p u1;
	size_t l;
	assert(ue_is_Lump(u));

	l = ue_get_LumpLength(u);
	if (l < 1) return sEmptyList;

	u1 = ue_get_LumpElem(u, 1);
	if (l == 1) return u1;

	switch (ue_get_type(u1)) {
		case STRING:		return ue_join_String(u);
		case LUMP:			return ue_join_Lump(u);

		default:
			return u;
	}
}

extern
ue_p ue_ConstantArray_Tensor(const ue_p u) {
	size_t len, i;
	ue_p u1, u2, ui;
	ten_t v, t;
	mi_t *index;

	if (ue_get_LumpLength(u) != 2) return sFailed;

	u1 = ue_get_LumpElem(u, 1);
	u2 = ue_get_LumpElem(u, 2);

	if (ue_is_List(u1)) {
		u1 = ue_mk_TensorFromList(u1);
	}

	if (ue_is_Number(u1)) {
	 	v = ten_mk_num_t(ue_get_Number(u1));
	} else if (ue_is_Tensor(u1)) {
		v = ue_get_Tensor(u1);
	} else if (ue_is_Symbol(u1)) {
		return u;
	} else {
		return sFailed;
	}

	if (ue_is_Tensor(u2)) {
		return sFailed;
	} else if (!ue_is_List(u2)) {
		u2 = ue_mk_Lump(sList, 1, u2);
	}

	len = ue_get_LumpLength(u2);

	index = (mi_t *) ue_Malloc(len * sizeof(ten_t));
	validateMemory(index, "ue_ConstantArray_Tensor");

	for (i = 1; i <= len; i++) {
		ui = ue_get_LumpElem(u2, i);
		if (ue_is_mi(ui)) {
			index[i - 1] = num_get_mi(ue_get_Number(ui));
		} else {
			return sFailed;
		}
	}

	ten_constantarray(&t, v, len, index);

	if (!ten_is_valid(t)) return sFailed;
	return ue_mk_Tensor(t);
}

static
ue_p ue_OuterList_Tensor(const ue_p u) {
	size_t len = ue_get_LumpLength(u);
	size_t i;
	ten_t *tens, t;
	ue_p ui;

	if (len < 1) return sFailed;

	tens = (ten_t *) ue_Malloc(len * sizeof(ten_t));
	validateMemory(tens, "ue_OuterList_Tensor");

	for (i = 1; i <= len; i++) {
		ui = ue_get_LumpElem(u, i);
		if (ue_is_Tensor(ui) && (ue_get_TensorRank(ui)) > 0) {
			tens[i - 1] = ue_get_Tensor(ui);
		} else {
			return sFailed;
		}
	}

	ten_outerlist(&t, len, tens);

	if (!ten_is_valid(t)) return sFailed;
	return ue_mk_Tensor(t);
}

extern
ue_p ue_OuterList(const ue_p u) {
	ue_p ui;
	size_t l, i;
	bool try_tensor = true;
	ntype ttype;

	assert(ue_is_Lump(u));

	l = ue_get_LumpLength(u);
	if (l < 1) return sFailed;

	ui = ue_get_LumpElem(u, 1);
	if (ue_is_Tensor(ui)) {
		ttype = ue_get_TensorType(ui);
	} else {
		try_tensor = false;
	}

	if (try_tensor) {
		for (i = 2; i <= l; i++) {
			ui = ue_get_LumpElem(u, i);
			if (!ue_is_Tensor(ui)) return u;
			if (ttype != ue_get_TensorType(ui)) {
				try_tensor = false;
				break;
			}
		}
	} 

	if (try_tensor) 
		return ue_OuterList_Tensor(u);
	else
		return u;
}

static inline
ue_p iRange(num_t start, num_t stop, num_t step) {
	ten_t t;
	ten_range(&t, start, stop, step);
	if (!ten_is_valid(t)) return sEmptyList;
	return ue_mk_Tensor(t);
}

extern
ue_p ue_Range(const ue_p u) {
	ue_p start, stop, step;
	size_t l;

	assert(ue_is_Lump(u));

	l = ue_get_LumpLength(u);
	if (l < 1 || l > 3) return sFailed;

	if (l == 1) {
		start = ue_POne;
		stop = ue_get_LumpElem(u, 1);
		step = ue_POne;
	} else if (l == 2) {
		start = ue_get_LumpElem(u, 1);
		stop = ue_get_LumpElem(u, 2);
		step = ue_POne;
	} else if (l == 3) {
		start = ue_get_LumpElem(u, 1);
		stop = ue_get_LumpElem(u, 2);
		step = ue_get_LumpElem(u, 3);
	} else {
		return u;
	}


	if (ue_is_Number(start) && ue_is_Number(stop) && ue_is_Number(step)) {
		return iRange(ue_get_Number(start), ue_get_Number(stop), ue_get_Number(step));
	}

	return u;
}

extern
bool ue_Replace(ue_p *u, bool (*fp)(const ue_p, const ue_p), ue_p var, ue_p val) {
	switch ( ue_get_type(*u) ) {
		case SYMBOL:
		case STRING:
		case NUMBER:
		case TENSOR:
		case FUNCTION:
		case DATA: {
			if (fp(*u, var)) *u = val;
			return true;
		}

		case LUMP: {
			ue_p tmp = ue_get_LumpType(*u);
			size_t i, len = ue_length(*u);

			if (ue_Replace(&tmp, fp, var, val)) {
				ue_set_LumpType(*u, tmp);
			}

			for (i = 1; i <= len; i++) {
				tmp = ue_get_LumpElem(*u, i);
				if (ue_Replace(&tmp, fp, var, val)) {
					ue_set_LumpElem(*u, i, tmp);
				}
			}
			return true;
		}

		default:
			return false;
	}	

	return false;
}

extern bool
ue_ReplaceList(ue_p *u, bool (*fp)(const ue_p, const ue_p), ue_p var, ue_p val) {
	size_t len, i;

	assert(ue_is_List(var));
	assert(ue_is_List(val));

	len = ue_length(var);
	if ( len != ue_length(val))
		return false;


	i = 1;
	while (i <= len &&
		ue_Replace(u, fp, 
			ue_get_LumpElem(var, i),
			ue_get_LumpElem(val, i))) 
		{
		i++;
	}

	if (i - 1 == len) return true;

	return false;
}

extern
ue_p ue_part_scattered(const ue_p u, const ten_t mi) {

	if (ue_length(u) == 0) return u;

	switch ( ue_get_type(u) ) {
		case STRING: {
			return ue_part_scattered_String(u, mi);
		}
		case TENSOR: {
			return ue_part_scattered_Tensor(u, mi);
		}

		case LUMP: {
			return ue_part_scattered_Lump(u, mi);
		}

		default:
			return u;
	}
}

extern
ue_p ue_set_part_scattered(ue_p *u, const ten_t mi, const ue_p vals) {

	if (ue_length(*u) == 0) return sFailed;

	switch ( ue_get_type(*u) ) {
/*
		case STRING: {
			return ue_set_part_scattered_String(u, mi);
		}
*/
		case TENSOR: {
			return ue_set_part_scattered_Tensor(u, mi, vals);
		}
/*
		case LUMP: {
			return ue_set_part_scattered_Lump(u, mi);
		}
*/
		default:
			return sFailed;
	}
}

extern
ue_p ue_part_single(const ue_p u, const ten_t mi) {

	ue_p res = sFailed;

	switch ( ue_get_type(u) ) {
		case STRING: {
			if (ten_get_dimensions(mi)[0] != 1) return sFailed;
			res = ue_part_scattered_String(u, mi);
			if (res == u) return sFailed;
			break;
		}
		case TENSOR: {
			res = ue_part_single_Tensor(u, mi);
			if (res == u) return sFailed;
			break;
		}
		case LUMP: {
			res = ue_part_scattered_Lump(u, mi);
			if (res == u) return sFailed;
			res = ue_get_LumpElem(res, 1);
			break;
		}
		default:
			return u;
	}

	return res;
}

extern
ue_p ue_set_part_single(ue_p *u, const ten_t mi, const num_t v) {

	ue_p res = sFailed;

	switch ( ue_get_type(*u) ) {
/*
		case STRING: {
			if (ten_get_dimensions(mi)[0] != 1) return sFailed;
			res = ue_set_part_scattered_String(*u, mi);
			break;
		}
*/
		case TENSOR: {
			res = ue_set_part_single_Tensor(u, mi, v);
			break;
		}
/*
		case LUMP: {
			res = ue_set_part_scattered_Lump(u, mi);
			break;
		}
*/
		default:
			return *u;
	}

	return res;
}

extern
ue_p ue_part_structured	(const ue_p u, const ue_p spec) {
	size_t i, n;
	ue_p ui;
	ten_t *mi;

	if (ue_length(u) == 0 || !ue_is_Lump(spec)) return u;

	n = ue_length(spec);
	if (n == 0) return sFailed;

	mi = (ten_t *) ue_Malloc(n * sizeof(ten_t));
	validateMemory(mi, "ue_part_structured");

	for (i = 0; i < n; i++) {
		ui = ue_get_LumpElem(spec, i + 1);
		if (ui == sAll && ue_is_Tensor(u)) {
			ten_t t = ue_get_Tensor(u);
			size_t rank = ten_get_rank(t);
			size_t *dims = ten_get_dimensions(t);
			if (i < rank) {
				ui = iRange(num_p_one_mi, num_mk_mi_si(dims[i]), num_p_one_mi);
			}
		}
		if (ue_is_Number(ui)) ui = ue_mk_Lump(sList, 1, ui);
		if (ue_is_List(ui)) {
			if (ue_length(ui) < 1) return sEmptyList;
			ui = ue_mk_TensorFromList(ui);
		}
		if (ue_is_Tensor(ui) && ue_get_TensorType(ui) == MI) {
			mi[i] = ue_get_Tensor(ui);
		} else {
			return sFailed;
		}
	}	

	switch ( ue_get_type(u) ) {
		case STRING: {
			if (n > 1) return sFailed;
			return ue_part_scattered_String(u, mi[0]);
		}
		case TENSOR: {
			return ue_part_structured_Tensor(u, n, mi);
		}
		case LUMP: {
			return sFailed;
		}
		default:
			return u;
	}

	return u;
}

extern
ue_p ue_set_part_structured	(ue_p *u, const ue_p spec, const ue_p vals) {
	size_t i, n;
	ue_p ui;
	ten_t *mi;
	ue_p v = vals;

	if (ue_length(*u) == 0 || !ue_is_Lump(spec)) return *u;

	n = ue_length(spec);
	if (n == 0) return sFailed;

	mi = (ten_t *) ue_Malloc(n * sizeof(ten_t));
	validateMemory(mi, "ue_part_structured");

	for (i = 0; i < n; i++) {
		ui = ue_get_LumpElem(spec, i + 1);
		if (ui == sAll && ue_is_Tensor(*u)) {
			ten_t t = ue_get_Tensor(*u);
			size_t rank = ten_get_rank(t);
			size_t *dims = ten_get_dimensions(t);
			if (i < rank) {
				ui = iRange(num_p_one_mi, num_mk_mi_si(dims[i]), num_p_one_mi);
			}
		}
		if (ue_is_Number(ui)) ui = ue_mk_Lump(sList, 1, ui);
		if (ue_is_List(ui)) ui = ue_mk_TensorFromList(ui);
		if (ue_is_Tensor(ui) && ue_get_TensorType(ui) == MI) {
			mi[i] = ue_get_Tensor(ui);
		} else {
			return sFailed;
		}
	}	

	switch ( ue_get_type(*u) ) {
/*
		case STRING: {
			if (n > 1) return sFailed;
			return ue_part_scattered_String(u, mi[0]);
		}
*/
		case TENSOR: {
			if (ue_is_List(v))
				v = ue_mk_TensorFromList(v);
			else if (ue_is_Number(v)) {
				ten_t r;
				ten_t num = ten_mk_num_t(ue_get_Number(v));
				mi_t index[1] = {1};
				for (i = 0; i < n; i++)
					index[0] *= ten_get_number_of_values(mi[i]);

				ten_constantarray(&r, num, 1, index);
				if (!ten_is_valid(r)) return sFailed;
				v = ue_mk_Tensor(r);
			}

			if (!ue_is_Tensor(v))
				return sFailed;

			return ue_set_part_structured_Tensor(u, n, mi, ue_get_Tensor(v));
		}
		case LUMP: {
			return sFailed;
		}
		default:
			return *u;
	}

	return *u;
}

extern
size_t ue_length(ue_p u) {
	switch (ue_get_type(u)) {
		case SYMBOL:
		case NUMBER:
			return 0;

		case STRING:
			return ue_get_StringLength(u);

		case TENSOR:
			return ten_get_dimensions(ue_get_Tensor(u))[0];

		case FUNCTION:
			return 2;
 
		case DATA:
			return ue_get_DataLength(u);

		case LUMP:
			return ue_get_LumpLength(u);

		default:
			noCase("ue_length");
			return 0;
	}
}

extern
ue_p ue_Length(ue_p u) {
	size_t l;
	ue_p res;
	l = ue_length(u);
	if (l > (size_t) MI_MAX)
		return sFailed;
	res = ue_mk_mi_si( (mi_t) l);
	return res;
}

extern
ue_p ue_StringToInteger(char const *str) {
	long val;
	ue_p res;
	/*numStringToInteger*/
	errno = 0;
	val = strtol(str, NULL, 10);

	if (errno == ERANGE) {
		res = ue_mk_bi_str(str);
		return res;
	}
	res = ue_mk_mi_si(val);
	return res;
}

extern
ue_p ue_StringToReal(char const *str) {
	double val;
	ue_p res;

	errno = 0;
	val = strtod(str, NULL);

	if (errno == ERANGE) {
		res = ue_mk_br_str(str);
		return res;
	}
	/* TODO: unfortuntly this is not the end of the story:
		as for example "9007199254740993." shows.
		decimal digits log10(2^DBL_MANT_DIG)
	 */
	res = ue_mk_mr_d(val);
	return res;
}

extern
bool ue_is_Zero(const ue_p u) {
	if (ue_get_type(u) == NUMBER)
		return num_is_zero(ue_get_Number(u));
	return false;
}


extern
ue_p ue_MemoryInUse(void) {
	size_t s = ue_HeapSize();
	if (s <= (size_t) MI_MAX)
		return ue_mk_mi_si((mi_t) s);

	return sFailed;
}

/* TODO: this does not seem quite right */
static
size_t ue_size(ue_p u) {
	size_t i, res;

	res = sizeof(struct ue_t);
	switch (ue_get_type(u)) {
		case SYMBOL:
			/* are always shared; stop recursion here */
			res += ue_ExpressionSize((const void *) sSymbol);
			res += ue_ExpressionSize((const void *) ue_get_SymbolName(u));
			/* some stuff is missing here ... */
			return res;
		case STRING:
			res += ue_ExpressionSize((const void *) sString);
			res += ue_ExpressionSize((const void *) ue_get_String(u));
			return res;
		case NUMBER:
			res += ue_size(ue_get_Type(u));
			res += num_size(ue_get_Number(u));
			return res;
		case TENSOR:
			res += ue_size(ue_get_Type(u));
			res += ten_size(ue_get_Tensor(u));
			return res;
		case FUNCTION:
			res += ue_size(ue_get_Type(u));
			res += ue_size(ue_get_FunctionUex(u));
			/* some stuff is missing here ... */
			return res;
		case DATA:
			res += ue_size(ue_get_DataType(u));
			res += ue_size_data(u);
			return res;
		case LUMP: 
			res += sizeof(size_t);
			res += ue_size(ue_get_Type(u));
			for (i = 1; i <= ue_get_LumpLength(u); i++) {
				res += ue_size(ue_get_LumpElem(u, 1));
			}
			return res;

		default:
			noCase("ue_size");
			return 0;
	}
}

ue_p ue_ByteCount(ue_p u) {
	size_t s = ue_size(u);
	if (s <= (size_t) MI_MAX)
		return ue_mk_mi_si((mi_t) s);

	return sFailed;
}

static
void ue_init() {

	char* this_locale;
	/* setting the following will make the decimal point need to
	be a comma in the de_DE.UTF-8 locale */
	/*this_locale = setlocale(LC_ALL,"");*/
	/*this_locale = setlocale(LC_ALL, NULL);*/
	/* LANG=de_DE.UTF-8 */
	this_locale = setlocale(LC_CTYPE, "");

	/* bootstrap symbols*/
	ue_init_symboltable();
	sSymbol = ue_mk_Symbol("Symbol");
	sSymbolTable = ue_mk_Symbol("SymbolTable");

	sString = ue_mk_Symbol("String");

	/*machine numbers*/
	sMInt = ue_mk_Symbol("MachineInteger");
	sMReal = ue_mk_Symbol("MachineReal");

	/*big numbers*/
	sBInt = ue_mk_Symbol("Integer");
	sBReal = ue_mk_Symbol("Real");

	/*machine tensor*/
	sMIntTensor = ue_mk_Symbol("MachineIntegerTensor");
	sMRealTensor = ue_mk_Symbol("MachineRealTensor");

	/*big tensor*/
	sBIntTensor = ue_mk_Symbol("IntegerTensor");
	sBRealTensor = ue_mk_Symbol("RealTensor");

	/*primitive functions*/
	sSystemFunction = ue_mk_Symbol("SystemFunction");
	sInterpretedFunction = ue_mk_Symbol("InterpretedFunction");

	sCoerce = ue_mk_Symbol("Coerce");
	sQuoteCoercion = ue_mk_Symbol("QuoteCoercion");

	sAll = ue_mk_Symbol("All");
	sDone = ue_mk_Symbol("Done");
	sFailed = ue_mk_Symbol("Failed");
	sList = ue_mk_Symbol("List");
	sNull = ue_mk_Symbol("Null");
	sQuit = ue_mk_Symbol("Quit");
	sRestart = ue_mk_Symbol("Restart");

	/* coercable symbols */
	sE					= ue_mk_Symbol("\\e");
	sNegativeInfinity	= ue_mk_Symbol("\\-inf");
	sNotANumber			= ue_mk_Symbol("\\nan");
	sPositiveInfinity	= ue_mk_Symbol("\\inf");
	sPi					= ue_mk_Symbol("\\pi");

	/* system variables */
	sSLocale = ue_mk_String(this_locale);

	sEmptyList = ue_mk_Lump(sList, 0);

}

void ue_init_lib() {
	ue_init_bignum();
	ue_init();
	ue_init_math();
	ue_init_streams();
	ue_init_datastructures();
}
