
#ifndef	DATASTRUCTURES_H 
#define DATASTRUCTURES_H

#include "uex.h"

typedef struct ue_ht_char_t * ue_ht_char_p;
typedef struct ue_ht_void_t * ue_ht_void_p;

ue_ht_char_p ue_mk_str_hashtable(void);
void	ue_str_hashtable_find(ue_ht_char_p *ht, char const *name, void **value);
void	ue_str_hashtable_add(ue_ht_char_p *ht, char const *name, void *value);
ue_p	ue_str_hashtable_values(ue_ht_char_p ht);

ue_ht_void_p ue_mk_void_hashtable(void);
void ue_void_hashtable_find(ue_ht_void_p *ht, unsigned int id, void **key, void **value);
void	ue_void_hashtable_add(ue_ht_void_p *ht, unsigned int id, void *key, void *value);
ue_p	ue_void_hashtable_keyvalues(ue_ht_void_p ht);

ue_p ue_Hash(ue_p u);
unsigned int ue_hash(const ue_p u);

/* https://stackoverflow.com/a/2595226 */
static inline
void hash_combine(unsigned int *seed, ue_p u) {
	unsigned int h = ue_hash(u);
	if (*seed && h)
		*seed ^= (h + 0x9e3779b9 + ((*seed)<<6) + ((*seed)>>2));
	else
		*seed = 0;
}


void	ue_init_datastructures();
ue_p	ue_mk_LookupData(const ue_p keys, const ue_p values, const ue_p defaultValue);
ue_p	ue_LookupDataFind(const ue_p data, const ue_p values);
ue_p	ue_LookupDataAdd(ue_p data, const ue_p keys, const ue_p values);
ue_p	ue_LookupDataDefaultValue(const ue_p data);
ue_p	ue_LookupDataKeyValues(const ue_p data);


#endif
