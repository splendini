
#ifndef	UEX_MATH_H 
#define UEX_MATH_H

#include "uex.h"

extern ue_p ue_MInfinity;
extern ue_p ue_MOne;
extern ue_p ue_Zero;
extern ue_p ue_POne;
extern ue_p ue_PInfinity;

extern ue_p sAbs;
extern ue_p sArcCos;
extern ue_p sArcCosh;
extern ue_p sArcSin;
extern ue_p sArcSinh;
extern ue_p sArcTan;
extern ue_p sArcTanh;
extern ue_p sCeil;
extern ue_p sCos;
extern ue_p sCosh;
extern ue_p sErf;
extern ue_p sErfc;
extern ue_p sExp;
extern ue_p sFloor;
extern ue_p sGamma;
extern ue_p sLog;
extern ue_p sLog2;
extern ue_p sLog10;
extern ue_p sMod;
extern ue_p sSign;
extern ue_p sSin;
extern ue_p sSinh;
extern ue_p sTan;
extern ue_p sTanh;

extern ue_p sGCD, sfGCD;
extern ue_p sMax, sfMax;
extern ue_p sMin, sfMin;
extern ue_p sAdd, sfAdd;
extern ue_p sPower, sfPower;
extern ue_p sRational, sfRational;
extern ue_p sComplex, sfComplex;
extern ue_p sMultiply, sfMultiply;

extern ue_p ue_mk_Complex(ue_p n, ue_p d);
extern ue_p ue_mk_Rational(ue_p n, ue_p d);

void	ue_init_math( );

ue_p	ue_Abs		(const ue_p u);
ue_p	ue_ArcCos	(const ue_p u);
ue_p	ue_ArcCosh	(const ue_p u);
ue_p	ue_ArcSin	(const ue_p u);
ue_p	ue_ArcSinh	(const ue_p u);
ue_p	ue_ArcTan	(const ue_p u);
ue_p	ue_ArcTanh	(const ue_p u);
ue_p	ue_Ceiling	(const ue_p u);
ue_p	ue_Cos		(const ue_p u);
ue_p	ue_Cosh		(const ue_p u);
ue_p	ue_Erf		(const ue_p u);
ue_p	ue_Erfc		(const ue_p u);
ue_p	ue_Exp		(const ue_p u);
ue_p	ue_Floor	(const ue_p u);
ue_p	ue_Gamma	(const ue_p u);
ue_p	ue_Log		(const ue_p u);
ue_p	ue_Log2		(const ue_p u);
ue_p	ue_Log10	(const ue_p u);
ue_p	ue_Remainder(const ue_p u);
ue_p	ue_Round	(const ue_p u);
ue_p	ue_Sign		(const ue_p u);
ue_p	ue_Sin		(const ue_p u);
ue_p	ue_Sinh		(const ue_p u);
ue_p	ue_Tan		(const ue_p u);
ue_p	ue_Tanh		(const ue_p u);
ue_p	ue_Truncate	(const ue_p u);

ue_p	ue_GCD		(const ue_p u);
ue_p	ue_Max		(const ue_p u);
ue_p	ue_Min		(const ue_p u);
ue_p	ue_Add		(const ue_p u);
ue_p	ue_Power	(const ue_p u);
ue_p	ue_Multiply	(const ue_p u);

#define ue_is_Even(u)	(ue_is_Number(u) && (num_is_even(ue_get_Number(u))))
#define ue_is_Odd(u)	(ue_is_Number(u) && (num_is_odd(ue_get_Number(u))))

ue_p	ue_TensorApply(const ue_p f, const ue_p t);

#endif
