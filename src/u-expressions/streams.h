
#ifndef	STREAMS_H
#define STREAMS_H

/* TODO: should give a warning if uex.h is not loaded */

void ue_init_streams();

typedef struct {
	ue_p path;
	FILE *file;
	const char *mode;
} stream_t;

extern ue_p sInputStream;
extern ue_p sOutputStream;

/* input/output streams */
#define stream(e)				((stream_t *) ue_get_Data(e))
#define	ue_is_InputStream(e)	((ue_is_Data(e)) && (ue_get_DataType(e) == sInputStream))
#define	ue_is_OutputStream(e)	((ue_is_Data(e)) && (ue_get_DataType(e) == sOutputStream))
#define	ue_is_Stream(e)			(ue_is_InputStream(e) || ue_is_OutputStream(e))
#define ue_get_Stream(e)		(assert(ue_is_Stream(e)),stream(e))
#define ue_get_StreamType(e)	(assert(ue_is_Stream(e)),ue_get_DataType(e))
#define ue_set_StreamType(e, t)	(assert(ue_is_Stream(e)),ue_set_DataType(e, t))
#define ue_get_StreamFP(e)		(assert(ue_is_Stream(e)),stream(e)->file)
#define ue_set_StreamFP(e, f)	(assert(ue_is_Stream(e)),(stream(e)->file) = (f))
#define ue_get_StreamPath(e)	(assert(ue_is_Stream(e)),stream(e)->path)
#define ue_set_StreamPath(e, f)	(assert(ue_is_Stream(e)),(stream(e)->path) = (f))
#define ue_get_StreamMode(e)	(assert(ue_is_Stream(e)),stream(e)->mode)
#define ue_set_StreamMode(e, f)	(assert(ue_is_Stream(e)),(stream(e)->mode) = (f))

ue_p	ue_mk_Stream_from_FILE(FILE *file, char const *name, char const *mode);
ue_p	ue_mk_Stream	(char const *path, char const *mode);
ue_p	ue_StreamClose	(ue_p u);



#endif
