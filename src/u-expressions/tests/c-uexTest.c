#include <stdlib.h>
#include "uex.h"

int main () {
	/*size_t dims[1];*/
	/*size_t *dims_ptr;*/
	/*size_t rank = 1;*/
	/*bool test;*/
	/*ntype type;*/
	/*ten_t ttmp;*/
	/*num_t ntmp;*/
	ue_p tmp, tc1, tc2;
	size_t len;

	ue_init_lib();

	tmp = ue_mk_Symbol("test");
/*
	dp(tmp);
	dims[0] = 0;
*/
/*
	tmp = ue_mk_Tensor_ue(MI, rank, &dims[0], &dims[0]);
	if(tmp != sFailed) exit(EXIT_FAILURE);
*/

	/*tmp = ue_mk_Tensor_ue_ten_t(ttmp);*/
	/* tenFprintF is static */
	/* (void) tenFprintF(stdout, tmp); */
/*
	tmp = ue_cp_Tensor(tmp);
	if(tmp != sFailed) exit(EXIT_FAILURE);
*/
/*
	type = MI;
	tmp = ue_mk_Tensor_ue(type, rank, &dims[0], &dims[0]);
*/
	/*ueSetTensorData(1, &dims[0], 1, &dims[0], tmp, NUMCOUNT);*/
	/* findTensorDimensionsAndType is static */ 
/*
	dims[0] = 1;
	type = MI;
	tmp = ue_mk_Tensor_ue(type, rank, &dims[0], &dims[0]);
	dp(tmp);
	test = ue_is_UETensor(tmp, &rank, &dims_ptr, &dims[0], &type);
	if(!test) exit(EXIT_FAILURE);
*/
	tmp = ue_mk_String("test");
	tmp = ue_cp(tmp);
/*
  dp(tmp);
*/
	if (!ue_is_Same(tmp, ue_mk_String("test"))) exit(EXIT_FAILURE);

	/*
	tmp = ue_mk_Number(ntmp);
	tmp = ue_get_Type(tmp);
	if (!ue_is_Same(tmp, sFailed)) exit(EXIT_FAILURE);
	*/

	/*
	tmp = ue_mk_Tensor_ue_ten_t(ttmp);
	tmp = ue_get_Type(tmp);
	if (!ue_is_Same(tmp, sFailed)) exit(EXIT_FAILURE);
	*/

	tc1 = ue_mk_String("evil");
	tc1->t = 666;
	tmp = ue_get_Type(tc1);
/*
	dp(tmp);
*/
	if (!ue_is_Same(tmp, sFailed)) exit(EXIT_FAILURE);

	ue_PrintRaw(stdout, tc1, true);
	fprintf(stdout, "\n");

	tc2 = ue_mk_String("evil");
/*
	dp(tc2);
*/
	if (ue_is_Same(tc1, tc2)) exit(EXIT_FAILURE);


	tmp = ue_cp(tc1);
	if (!ue_is_Same(tmp, sFailed)) exit(EXIT_FAILURE);

	/* ueCoerceToType(tc1, MI) static */;
	
	len = ue_length(tc1);
	if (len != 0) exit(EXIT_FAILURE);

	exit(EXIT_SUCCESS);
}
