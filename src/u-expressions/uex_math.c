
#include <limits.h>

#include "uex_math.h"
#include "num.h"
#include "tensor.h"

ue_p ue_MInfinity;
ue_p ue_MOne;
ue_p ue_Zero;
ue_p ue_POne;
ue_p ue_PInfinity;

ue_p sAbs;
ue_p sArcCos;
ue_p sArcCosh;
ue_p sArcSin;
ue_p sArcSinh;
ue_p sArcTan;
ue_p sArcTanh;
ue_p sCeil;
ue_p sCos;
ue_p sCosh;
ue_p sErf;
ue_p sErfc;
ue_p sExp;
ue_p sFloor;
ue_p sGamma;
ue_p sLog;
ue_p sLog2;
ue_p sLog10;
ue_p sMod;
ue_p sSign;
ue_p sSin;
ue_p sSinh;
ue_p sTan;
ue_p sTanh;

ue_p sGCD, sfGCD;
ue_p sMax, sfMax;
ue_p sMin, sfMin;
ue_p sAdd, sfAdd;
ue_p sPower, sfPower;
ue_p sRational, sfRational;
ue_p sComplex, sfComplex;
ue_p sMultiply, sfMultiply;


#define UE_MATH_FUNCTION_ARG1(name, fun)\
extern ue_p ue_##name(const ue_p u) {\
	size_t l;\
	ue_p arg;\
\
	if (!ue_is_Lump(u)) {\
		/* TODO: message */\
		return sFailed;\
	}\
\
	l = ue_get_LumpLength(u);\
\
	if (l != 1) {\
		/* TODO: message */\
		return sFailed;\
	}\
\
	arg = ue_get_LumpElem(u, 1);\
\
	if (ue_is_Number(arg)) {\
		num_t n = ue_get_Number(arg);\
		num_t res;\
		num_##fun(&res, n);\
		if (num_is_valid(res)) {\
			return ue_mk_Number(res);\
		}\
	}\
\
	if (ue_is_Tensor(arg)) {\
		ten_t t = ue_get_Tensor(arg);\
		ten_t res;\
		ten_##fun(&res, t);\
		if (ten_is_valid(res)) {\
			return ue_mk_Tensor(res);\
		}\
	}\
\
	return u;\
}\

/* ue_Fun vs, num_fun and ten_fun */
UE_MATH_FUNCTION_ARG1( Abs,		abs)
UE_MATH_FUNCTION_ARG1( ArcCos,	acos)
UE_MATH_FUNCTION_ARG1( ArcCosh,	acosh)
UE_MATH_FUNCTION_ARG1( ArcSin,	asin)
UE_MATH_FUNCTION_ARG1( ArcSinh,	asinh)
UE_MATH_FUNCTION_ARG1( ArcTan,	atan)
UE_MATH_FUNCTION_ARG1( ArcTanh,	atanh)
UE_MATH_FUNCTION_ARG1( Ceiling,	ceil)
UE_MATH_FUNCTION_ARG1( Cos,		cos)
UE_MATH_FUNCTION_ARG1( Cosh,	cosh)
UE_MATH_FUNCTION_ARG1( Erf,		erf)
UE_MATH_FUNCTION_ARG1( Erfc,	erfc)
UE_MATH_FUNCTION_ARG1( Exp,		exp)
UE_MATH_FUNCTION_ARG1( Floor,	floor)
UE_MATH_FUNCTION_ARG1( Gamma,	gamma)
UE_MATH_FUNCTION_ARG1( Log,		log)
UE_MATH_FUNCTION_ARG1( Log2,	log2)
UE_MATH_FUNCTION_ARG1( Log10,	log10)
UE_MATH_FUNCTION_ARG1( Round,	round)
UE_MATH_FUNCTION_ARG1( Sign,	sign)
UE_MATH_FUNCTION_ARG1( Sin,		sin)
UE_MATH_FUNCTION_ARG1( Sinh,	sinh)
UE_MATH_FUNCTION_ARG1( Tan,		tan)
UE_MATH_FUNCTION_ARG1( Tanh,	tanh)
UE_MATH_FUNCTION_ARG1( Truncate,trunc)


#define UE_MATH_FUNCTION_ARG2(name, fun)\
extern ue_p ue_##name(const ue_p u) {\
	size_t l;\
	ue_p arg1, arg2;\
\
	if (!ue_is_Lump(u)) {\
		/* TODO: message */\
		return sFailed;\
	}\
\
	l = ue_get_LumpLength(u);\
\
	if (l != 2) {\
		/* TODO: message */\
		return sFailed;\
	}\
\
	arg1 = ue_get_LumpElem(u, 1);\
	arg2 = ue_get_LumpElem(u, 2);\
\
	if (ue_is_Number(arg1) && ue_is_Number(arg2)) {\
		num_t n1 = ue_get_Number(arg1);\
		num_t n2 = ue_get_Number(arg2);\
		num_t res;\
		num_##fun(&res, n1, n2);\
		if (num_is_valid(res)) {\
			return ue_mk_Number(res);\
		}\
	}\
\
	if (ue_is_Tensor(arg1) && ue_is_Tensor(arg2)) {\
		ten_t t1 = ue_get_Tensor(arg1);\
		ten_t t2 = ue_get_Tensor(arg2);\
		ten_t res;\
		ten_##fun(&res, t1, t2);\
		if (ten_is_valid(res)) {\
			return ue_mk_Tensor(res);\
		}\
	}\
\
	return u;\
}\


UE_MATH_FUNCTION_ARG2( Remainder,	rem)


#define ARITHMETIC(name, fun, is_scalar, sNeutral, nNeutral, collect)\
extern \
ue_p ue_##name(const ue_p u) {\
	ue_p u1, ui, type;\
	ue_p list = NULL;\
	ue_p coercables = NULL;\
	ue_p rest = NULL;\
	ue_p rest2 = NULL;\
	size_t i, j, l;\
	bool isNum = false, isTen = false;\
	num_t n = ue_get_Number(nNeutral);\
	ten_t t = ten_mk_num_t(n);\
	ntype ctype = num_get_type(n);\
	ue_p res = sNeutral;\
\
	if (!ue_is_Lump(u)) {\
		/* TODO: message */\
		return sFailed;\
	}\
\
	l = ue_get_LumpLength(u);\
	if (l == 0) return sNeutral;\
	if ((l == 1) && !is_scalar) return ue_get_LumpElem(u, 1);\
\
	for (i = 1; i <= l; i++) {\
		ui = ue_get_LumpElem(u, i);\
		switch (ue_get_type(ui)) {\
			case NUMBER:\
				isNum = true;\
				num_##fun(&n, n, ue_get_Number(ui));\
				if (!num_is_valid(n)) {\
					/* TODO: message: gcd(20, 10.) */\
					return u;\
				}\
				break;\
			case TENSOR:\
				if (!isTen) {\
					isTen = true;\
					if (!is_scalar) {\
						t = ue_get_Tensor(ui);\
						continue;\
					}\
				}\
				ten_##fun(&t, t, ue_get_Tensor(ui));\
				if (!ten_is_valid(t)) {\
					/*TODO: message */\
					return u;\
				}\
				break;\
			default:\
				if (ue_is_List(ui) && is_scalar) {\
					ue_set_LumpType(ui, sf##name);\
					ui = ue_##name(ui);\
					if (ue_is_Number(ui)) {\
						isNum = true;\
						num_##fun(&n, n, ue_get_Number(ui));\
						if (!num_is_valid(n)) {\
							/* TODO: message */\
							return u;\
						}\
						break;\
					}\
				}\
  				if (ue_is_List(ui)) {\
 					if (!list) {\
 						list = ue_mk_Lump(sList, 0);\
 					}\
					list = ue_LumpAppendTo(list, ui);\
				} else if (ue_is_Symbol(ui) && ue_is_SymbolCoercable(ui)) {\
 					if (!coercables) {\
 						coercables = ue_mk_Lump(sList, 0);\
 					}\
					coercables = ue_LumpAppendTo(coercables, ui);\
 				} else {\
 					if (!rest) {\
						rest = ue_mk_Lump(sList, 0);\
					}\
					rest = ue_LumpAppendTo(rest, ui);\
				}\
		}\
	}\
\
	if(isNum && isTen) {\
		isNum = false;\
		ten_##fun(&t, t, ten_mk_num_t(n));\
		if(!ten_is_valid(t)) {\
			/*TODO: message */\
			return u;\
		}\
		res = ue_mk_Tensor(t);\
	} else if (isNum) {\
		res = ue_mk_Number(n);\
	} else if (isTen) {\
		res = ue_mk_Tensor(t);\
	}\
\
	if (!list && !rest && !coercables) return res;\
\
	if (isNum && num_is_integer(n)) {\
		if (num_cmp(n, ue_get_Number(nNeutral)) == 0) {\
			isNum = false;\
		} else if ((ue_##name == ue_Multiply) && num_is_zero(n)) {\
			/* Returns MInt or BInt 0; */\
			return res;\
		}\
	}\
\
	if (coercables) {\
 		if (ue_is_Inexact(res)) {\
 			if (ue_is_Number(res)) ctype = ue_get_NumberType(res);\
 			if (ue_is_Tensor(res)) ctype = ue_get_TensorType(res);\
 			for (i = 1; i <= ue_length(coercables); i++) {\
 				ui = ue_get_LumpElem(coercables, i);\
 				ui = ue_Coerce(ui, ctype);\
				ui = ue_mk_Lump(sf##name, 2, res, ui);\
 				res = ue_##name(ui);\
 			}\
	 	} else {\
			if (!rest) {\
				rest = coercables;\
			} else {\
				rest = ue_mk_Lump(sList, 2, rest, coercables);\
				rest = ue_Join(rest);\
			}\
	 	}\
	}\
\
	if (!list && !rest) return res;\
\
	if (list) {\
		size_t listlen = ue_length(list);\
		ue_p list1 = ue_cp(ue_get_LumpElem(list, 1));\
		size_t len1 = ue_length(list1);\
		ue_p temp = ue_mk_LumpShell(sf##name, 2);\
		for (i = 2; i <= listlen; i++) {\
			ue_p listi = ue_get_LumpElem(list, i);\
			if (len1 != ue_length(listi)) {\
				/* TODO message */\
				return u;\
			}\
			for (j = 1; j <= len1; j++) {\
				ue_set_LumpElem(temp, 1, ue_get_LumpElem(list1, j));\
				ue_set_LumpElem(temp, 2, ue_get_LumpElem(listi, j));\
				ue_set_LumpElem(list1, j, ue_##name(temp));\
			}\
		}\
		if (isNum) {\
			ue_set_LumpElem(temp, 2, res);\
			for (j = 1; j <= len1; j++) {\
				ue_set_LumpElem(temp, 1, ue_get_LumpElem(list1, j));\
				ue_set_LumpElem(list1, j, ue_##name(temp));\
			}\
		}\
		if (isTen) {\
			if(len1 != ue_length(res)) {\
				/* TODO message */\
				return u;\
			}\
			ue_p list1Ten = ue_mk_TensorFromList(list1);\
			if (ue_is_Tensor(list1Ten)) {\
				t = ue_get_Tensor(res);\
				ten_##fun(&t, t, ue_get_Tensor(list1Ten));\
				list1 = ue_mk_Tensor(t);\
			} else if (!rest) {\
				ue_set_LumpElem(temp, 1, list1);\
				ue_set_LumpElem(temp, 2, res);\
				return temp;\
			} else {\
				/* TODO: convert tensor to list */\
				return u;\
			}\
		}\
		res = list1;\
	}\
\
	if (!rest) return res;\
\
	rest = ue_Sort(rest);\
\
	l = ue_get_LumpLength(rest);\
	u1 = ue_get_LumpElem(rest, 1);\
	type = ue_get_Type(u);\
	rest2 = ue_mk_Lump(type, 0);\
	for (i = 2, j = 1; i <= l; i++) {\
		ui = ue_get_LumpElem(rest, i);\
		if (ue_is_Same(u1, ui)) {\
			j++;\
		} else {\
			assert(j < (size_t) MI_MAX);\
			if (j != 1) {\
				u1 = collect(u1, ue_mk_mi_si(j));\
			}\
			rest2 = ue_LumpAppendTo(rest2, u1);\
			j = 1;\
			u1 = ui;\
		}\
	}\
	assert(j < (size_t) MI_MAX);\
	if (j != 1) {\
		u1 = collect(u1, ue_mk_mi_si(j));\
	}\
	rest2 = ue_LumpAppendTo(rest2, u1);\
\
	rest = rest2;\
\
	if (!isNum && !isTen && !list) {\
		if (ue_get_LumpLength(rest) == 1) {\
			return ue_get_LumpElem(rest, 1);\
		}\
	} else {\
		rest = ue_LumpAppendTo(rest, res);\
	}\
	return rest;\
}\

#define addCollect(u, j)		ue_mk_Lump(sfMultiply, 2, u, j)
ARITHMETIC(Add, add, false, ue_Zero, ue_Zero, addCollect)

#define mulCollect(u, j)		ue_mk_Lump(sfPower, 2, u, j)
ARITHMETIC(Multiply, mul, false, ue_POne, ue_POne, mulCollect)

#define gcdCollect(u, j)		(u)
ARITHMETIC(GCD, gcd, false, ue_Zero, ue_Zero, gcdCollect)

#define maxCollect(u, j)		(u)
ARITHMETIC(Max, max, true, sNegativeInfinity, ue_MInfinity, maxCollect)

#define minCollect(u, j)		(u)
ARITHMETIC(Min, min, true, sPositiveInfinity, ue_PInfinity, minCollect)


extern ue_p ue_Power(const ue_p u) {
	size_t l;
	ue_p base, exp;
	l = ue_get_LumpLength(u);

	if (l == 0) return ue_mk_mi_si(1);
	if (l == 1) return ue_get_LumpElem(u, 1);
	if (l > 2) return u;

	base = ue_get_LumpElem(u, 1);
	exp = ue_get_LumpElem(u, 2);

	if (ue_is_Number(base) && ue_is_Number(exp)) {
		num_t res;
		num_t b = ue_get_Number(base);
		num_t e = ue_get_Number(exp);
		bool b_p = (num_cmp(b, num_zero_mi) >= 0);
		bool e_p = (num_cmp(e, num_zero_mi) >= 0);
	
		if (num_is_real(b)) {
			if (b_p) {
				num_pow(&res, b, e);
				return ue_mk_Number(res);	
			}
			if (num_is_integer(e)) {
				num_pow(&res, b, e);
				return ue_mk_Number(res);	
			}
		}

		if (num_is_integer(e)) {
			if (e_p) {
				num_pow(&res, b, e);
				return ue_mk_Number(res);	
			} else { /* rational */
				num_abs(&res, e);	
				num_pow(&res, b, res);
				return ue_mk_Rational(ue_mk_mi_si(1), ue_mk_Number(res));
			}
		}

		if (num_is_integer(b) && b_p) {
			num_pow(&res, b, e);
			return ue_mk_Number(res);	
		}

		/* complex case */
		/*
		if (!b_p) {
			return ue_mk_Complex;
		}*/

		/* Power(E, e * Log(b)) */
		return u;
	}

	if (ue_is_Number(exp)) {
		num_t e = ue_get_Number(exp);
		if (num_cmp(e, num_p_one_mi) == 0) return base;
		if (ue_is_Tensor(base)) {
			exp = ue_mk_Tensor(ten_mk_num_t(e));
		}
	}

	if (ue_is_Number(base) && ue_is_Tensor(exp)) {
		base = ue_mk_Tensor(ten_mk_num_t(ue_get_Number(base)));
	}

	if (ue_is_Tensor(base) && ue_is_Tensor(exp)) {
		ten_t res;
		ten_t b = ue_get_Tensor(base);
		ten_t e = ue_get_Tensor(exp);
/*
		bool b_p = (tenCmp(b, tenZero) >= 0);
		bool e_p = (tenCmp(e, tenZero) >= 0);
*/
		bool b_p = true;
		bool e_p = true;
		if ((b_p && e_p) /*|| (num_is_Real(b) && num_is_Int(e))*/) { 
			ten_power(&res, b, e);
			if (ten_is_valid(res)) return ue_mk_Tensor(res);
			return u;
		}
	}

	bool ibase = ue_is_Inexact(base);
	bool iexp = ue_is_Inexact(exp);

	if (ibase && ue_is_Symbol(exp)) {
		ue_p temp;
		ntype ctype = MR; 
		if (ue_is_Number(base)) ctype = ue_get_NumberType(base);
 		if (ue_is_Tensor(base)) ctype = ue_get_TensorType(base);
 		exp = ue_Coerce(exp, ctype);
		
		temp = ue_mk_Lump(sPower, 2, base, exp);
 		return ue_Power(temp);
	}

	if (!ibase && iexp) {
		ue_p temp;
		ntype ctype = MR;
		if (ue_is_Number(exp)) ctype = ue_get_NumberType(exp);
 		if (ue_is_Tensor(exp)) ctype = ue_get_TensorType(exp);

		if (ue_is_Number(base) || ue_is_Tensor(base)) {
 			base = ue_Coerce(base, ctype);
			temp = ue_mk_Lump(sPower, 2, base, exp);
	 		return ue_Power(temp);
		}
	}

	return u;
}

extern ue_p ue_mk_Rational(ue_p n, ue_p d) {
	
	if (ue_cmp(d, ue_POne) == 0) return n;
	if (ue_cmp(n, ue_Zero) == 0) return ue_Zero;
	if (ue_is_Same(n, d)) return ue_POne;
	if (ue_cmp(d, ue_Zero) == 0) return sFailed;

	if (ue_is_Number(n) && ue_is_Number(d)) {
		num_t nn = ue_get_Number(n);
		num_t nd = ue_get_Number(d);
		if (num_is_integer(nn) && num_is_integer(nd)) {
			num_t g;
			bool sign = false;
			if (num_cmp(nd, num_zero_mi) < 0) sign = true;
			num_gcd(&g, nn, nd);
			num_divexact(&nn, nn, g);
			num_divexact(&nd, nd, g);
			if (sign) {
				num_mul(&nn, nn, num_m_one_mi);
				num_mul(&nd, nd, num_m_one_mi);
			}
			if (num_cmp(nd, num_p_one_mi) == 0) return ue_mk_Number(nn);
			return ue_mk_Lump(sfRational, 2, ue_mk_Number(nn), ue_mk_Number(nd));
		} else {
			num_pow(&nd, nd, num_m_one_mr);
			num_mul(&nd, nd, nn);
			return ue_mk_Number(nd);
		}
	}

	return ue_mk_Lump(sfRational, 2, n, d);
}

extern ue_p ue_mk_Complex(ue_p re, ue_p im) {
	if (ue_cmp(im, ue_Zero) == 0) return re;
	return ue_mk_Lump(sfComplex, 2, re, im);
}

extern ue_p ue_TensorApply(const ue_p f, const ue_p t) {
	ten_t res;

	assert(ue_is_Tensor(t));

	if (f == sfAdd ) {
		ten_apply_add(&res, ue_get_Tensor(t)); 
	} else if (f == sfMultiply) {
		ten_apply_mul(&res, ue_get_Tensor(t)); 
	} else if (f == sfGCD) {
		ten_apply_gcd(&res, ue_get_Tensor(t)); 
	} else if (f == sfPower) {
		ten_apply_power(&res, ue_get_Tensor(t)); 
	} else {
		return sFailed;
	}

	return ue_mk_Tensor(res);
}

void ue_init_math() {

	ue_MInfinity	= ue_mk_Number(num_m_inf_mr);
	ue_MOne			= ue_mk_Number(num_m_one_mi);
	ue_Zero			= ue_mk_Number(num_zero_mi);
	ue_POne			= ue_mk_Number(num_p_one_mi);
	ue_PInfinity	= ue_mk_Number(num_p_inf_mr);

	sAbs		= ue_mk_Symbol("Abs");
	sArcCos		= ue_mk_Symbol("ArcCos");
	sArcCosh	= ue_mk_Symbol("ArcCosh");
	sArcSin		= ue_mk_Symbol("ArcSin");
	sArcSinh	= ue_mk_Symbol("ArcSinh");
	sArcTan		= ue_mk_Symbol("ArcTan");
	sArcTanh	= ue_mk_Symbol("ArcTanh");
	sCos		= ue_mk_Symbol("Cos");
	sCosh		= ue_mk_Symbol("Cosh");
	sErf		= ue_mk_Symbol("Erf");
	sErfc		= ue_mk_Symbol("Erfc");
	sExp		= ue_mk_Symbol("Exp");
	sGamma		= ue_mk_Symbol("Gamma");
	sLog		= ue_mk_Symbol("Log");
	sLog2		= ue_mk_Symbol("Log2");
	sLog10		= ue_mk_Symbol("Log10");
	sSin		= ue_mk_Symbol("Sin");
	sSinh		= ue_mk_Symbol("Sinh");
	sTan		= ue_mk_Symbol("Tan");
	sTanh		= ue_mk_Symbol("Tanh");

	/* we initialize temporary system functions
 	* to be replaced */
	sGCD		=	sfGCD		= ue_mk_Symbol("GCD");
	sMax		=	sfMax		= ue_mk_Symbol("Max");
	sMin		=	sfMin		= ue_mk_Symbol("Min");
	sAdd		=	sfAdd		= ue_mk_Symbol("Add");
	sPower		=	sfPower 	= ue_mk_Symbol("Power");
	sRational	=	sfRational	= ue_mk_Symbol("Rational");
	sMultiply	=	sfMultiply	= ue_mk_Symbol("Multiply");

}

