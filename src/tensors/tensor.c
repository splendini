
#include "tensor.h"

ten_t ten_not_valid;

#define ten_set_type(e, type)	(((e).nt) = (type))
#define ten_set_type_mi(t)		ten_set_type((t), MI)
#define ten_set_type_mr(t)		ten_set_type((t), MR)
#define ten_set_type_bi(t)		ten_set_type((t), BI)
#define ten_set_type_br(t)		ten_set_type((t), BR)

#define numtype_mi				MI
#define numtype_mr				MR
#define numtype_bi				BI
#define numtype_br				BR

static inline
size_t size_t_min(size_t a, size_t b) {
	return (a<b)? a: b;
}

static inline
size_t size_t_max(size_t a, size_t b) {
	return (a>b)? a: b;
}

static inline
void ten_noop_arg1(ten_t *r, const ten_t t1) {
	ten_set_type(*r, NOTYPE);
}

static inline
void ten_noop_arg2(ten_t *r, const ten_t t1, const ten_t t2) {
	ten_set_type(*r, NOTYPE);
}




/* perhaps const void *data could be made to work*/
extern
ten_t ten_mk(const ntype tType, const size_t rank, const size_t *dims, void *data) {

	size_t numberOfValues, i;
	size_t *localDims;
	ten_t res;

	assert(tentype_is_valid(tType));
	assert(dims != NULL);
	assert(data != NULL);

	ten_set_type(res, tType);
	ten_set_rank(res, rank);

	numberOfValues = 1;
	for(i = 0; i < rank; i++ ) {
		numberOfValues *= dims[i];
	}
	ten_set_number_of_values(res, numberOfValues);

	/* should call tenMalloc */
	localDims = (size_t*) ue_Malloc(size_t_max(1, rank) * sizeof(size_t));
	validateMemory(localDims, "ten_mk");
	localDims[0] = 0;
	for(i = 0; i < rank; i++) {
		localDims[i] = dims[i];
	}
	ten_set_dimensions(res, localDims);

	/* for unique tensor use ten_cp */
	ten_set_values(res, data);
	return res;
}

#define MK_TEN_FROM_TYPE(it)\
static \
ten_t ten_mk_##it(const num_t n) {\
\
	size_t *localDims;\
	it##_t *data;\
	ten_t res;\
\
	ten_set_type_##it(res);\
	ten_set_rank(res, 0);\
	ten_set_number_of_values(res, 1);\
\
	localDims = (size_t*) ue_Malloc(sizeof(size_t));\
	validateMemory(localDims, "localDims: ten_mk_");\
	localDims[0] = 0;\
	ten_set_dimensions(res, localDims);\
\
	data = (it##_t *) ue_Malloc(sizeof(it##_t));\
	validateMemory(data, "data: ten_mk_");\
	coerce_##it##_##it(data[0], num_get_##it(n));\
	ten_set_values(res, (void *) data);\
	return res;\
}\

MK_TEN_FROM_TYPE(mi)
MK_TEN_FROM_TYPE(mr)
MK_TEN_FROM_TYPE(bi)
MK_TEN_FROM_TYPE(br)

static
ten_t (*mkTen_num_t[NUMCOUNT])(const num_t n1) = {
	ten_mk_mi,
	ten_mk_mr,
	ten_mk_bi,
	ten_mk_br
};

extern
ten_t ten_mk_num_t(const num_t n) {
	assert(num_is_valid(n));
	return (*mkTen_num_t[num_get_type(n)])(n);
}

ten_t ten_not_valid = {.nt = -1};

#define PRINT_TENSOR_PART_TYPE(type)\
static \
void ten_print_rawPart_##type(FILE *f, size_t rank, size_t *dims, size_t len, type##_t *data) {\
	size_t i, sliceSize;\
	/* we have a vector */\
	if (rank == 1 ) {\
		/*@-sysunrecog@*/\
		print_raw_##type(f, data[0]);\
		/*@=sysunrecog@*/\
		for(i = 1; i < len; i++ ) {\
			fprintf(f, ", ");\
			print_raw_##type(f, data[i]);\
		}\
		return;\
	}\
	/* rest of dimensions */\
	sliceSize = 1;\
	for(i = 1; i < rank; i++) {\
		sliceSize *= dims[i];\
	}\
	/* recursive part */\
	fprintf(f, "List(");\
	ten_print_rawPart_##type (f, rank-1, &dims[1], sliceSize, &data[0]);\
	fprintf(f, ")");\
	for(i = 1; i < dims[0]; i++) {\
		fprintf(f, ", List(");\
		ten_print_rawPart_##type (f, rank-1, &dims[1], sliceSize, &data[i*sliceSize]);\
		fprintf(f, ")");\
	}\
}\

PRINT_TENSOR_PART_TYPE(mi)
PRINT_TENSOR_PART_TYPE(mr)
PRINT_TENSOR_PART_TYPE(bi)
PRINT_TENSOR_PART_TYPE(br)

#define PRINT_TENSOR_TYPE(type)\
static \
void ten_print_raw_##type(FILE *f, ten_t t) {\
	size_t i;\
	size_t rank, sliceSize;\
	size_t *dims;\
	type##_t *data;\
\
	rank = ten_get_rank(t);\
	dims = ten_get_dimensions(t);\
	data = ten_get_values(t);\
\
	if (rank == 0) {\
		fprintf(f, "TensorData(");\
		print_raw_##type(f, data[0]);\
		fprintf(f, ")");\
		return;\
	}\
\
	sliceSize = 1;\
	for(i = 0; i < rank; i++) {\
		sliceSize *= dims[i];\
	}\
\
	fprintf(f, "TensorData(List(");\
	ten_print_rawPart_##type (f, rank, dims, sliceSize, data);\
	fprintf(f, "))");\
}\


PRINT_TENSOR_TYPE(mi)
PRINT_TENSOR_TYPE(mr)
PRINT_TENSOR_TYPE(bi)
PRINT_TENSOR_TYPE(br)

static
void (*printRawTen[NUMCOUNT])(FILE *f, const ten_t t) = {
	ten_print_raw_mi,
	ten_print_raw_mr,
	ten_print_raw_bi,
	ten_print_raw_br
};

extern
void ten_print_raw(FILE *f, const ten_t t) {
	ntype type = ten_get_type(t);
	assert(tentype_is_valid(type));
	(*printRawTen[type])(f, t);
}

#define TENSOR_SCALAR_LOOP(fun, r, tt1) \
static \
void ten_##fun##_##r##_##tt1(ten_t *t, const ten_t t1) {\
	size_t i, l1, rank = 0;\
	size_t dims[1];\
	r##_t *v;\
	tt1##_t *v1;\
\
	*t = ten_not_valid;\
\
	assert(ten_is_valid(t1));\
	l1 = ten_get_number_of_values(t1);\
	v1 = (tt1##_t *) ten_get_values(t1);\
\
	v = (r##_t *) ue_Malloc(sizeof(r##_t));\
	validateMemory(v, " "#fun);\
\
	coerce_##r##_##tt1(v[0], v1[0]);\
	for(i = 1; i < l1; i++) {\
		fun##_##r##_##tt1(v[0], v1[i]);\
	}\
\
	dims[0] = 0;\
	*t = ten_mk(numtype_##r, rank, dims, (void *) v);\
}\

#define max_mi_mi(r, a)	((r) = (cmp_mi_mi(r, a) > 0 ? (r) : (a)))
#define max_mr_mr(r, a)	((r) = (cmp_mr_mr(r, a) > 0 ? (r) : (a)))
#define max_bi_bi(r, a)	(bnz_max(r, r, a))
#define max_br_br(r, a)	(bnf_max(r, r, a))
TENSOR_SCALAR_LOOP(max, mi, mi)
TENSOR_SCALAR_LOOP(max, mr, mr)
TENSOR_SCALAR_LOOP(max, bi, bi)
TENSOR_SCALAR_LOOP(max, br, br)

static
void (*maxTen[NUMCOUNT])(ten_t *r, const ten_t t1) = {
	ten_max_mi_mi,
	ten_max_mr_mr,
	ten_max_bi_bi,
	ten_max_br_br
};


extern
void ten_max_arg1(ten_t *r, const ten_t t) {
	assert(ten_is_valid(t));
	(*maxTen[ten_get_type(t)])(r, t);
}


#define min_mi_mi(r, a)	((r) = (cmp_mi_mi(r, a) < 0 ? (r) : (a)))
#define min_mr_mr(r, a)	((r) = (cmp_mr_mr(r, a) < 0 ? (r) : (a)))
#define min_bi_bi(r, a)	(bnz_min(r, r, a))
#define min_br_br(r, a)	(bnf_min(r, r, a))
TENSOR_SCALAR_LOOP(min, mi, mi)
TENSOR_SCALAR_LOOP(min, mr, mr)
TENSOR_SCALAR_LOOP(min, bi, bi)
TENSOR_SCALAR_LOOP(min, br, br)

static
void (*minTen[NUMCOUNT])(ten_t *r, const ten_t t1) = {
	ten_min_mi_mi,
	ten_min_mr_mr,
	ten_min_bi_bi,
	ten_min_br_br
};


extern
void ten_min_arg1(ten_t *r, const ten_t t) {
	assert(ten_is_valid(t));
	(*minTen[ten_get_type(t)])(r, t);
}


#define TENSOR_LOOP(fun, r, tt1) \
static \
void ten_##fun##_##r##_##tt1(ten_t *t, const ten_t t1) {\
	size_t i, l1, rank;\
	size_t *dims;\
	r##_t *v;\
	tt1##_t *v1;\
\
	*t = ten_not_valid;\
\
	assert(ten_is_valid(t1));\
	l1 = ten_get_number_of_values(t1);\
	v1 = (tt1##_t *) ten_get_values(t1);\
\
	v = (r##_t *) ue_Malloc(l1 * sizeof(r##_t));\
	validateMemory(v, " "#fun);\
\
	for(i = 0; i < l1; i++) {\
		fun##_##r##_##tt1(v[i], v1[i]);\
	}\
\
	rank = ten_get_rank(t1);\
	dims = ten_get_dimensions(t1);\
	*t = ten_mk(numtype_##r, rank, dims, (void *) v);\
}\


TENSOR_LOOP(coerce, mi, mi)
TENSOR_LOOP(coerce, mi, mr)
TENSOR_LOOP(coerce, mi, bi)
TENSOR_LOOP(coerce, mi, br)

TENSOR_LOOP(coerce, mr, mi)
TENSOR_LOOP(coerce, mr, mr)
TENSOR_LOOP(coerce, mr, bi)
TENSOR_LOOP(coerce, mr, br)

TENSOR_LOOP(coerce, bi, mi)
TENSOR_LOOP(coerce, bi, mr)
TENSOR_LOOP(coerce, bi, bi)
TENSOR_LOOP(coerce, bi, br)

TENSOR_LOOP(coerce, br, mi)
TENSOR_LOOP(coerce, br, mr)
TENSOR_LOOP(coerce, br, bi)
TENSOR_LOOP(coerce, br, br)


static
void (*cpTen[NUMCOUNT])(ten_t *r, const ten_t t1) = {
	ten_coerce_mi_mi,
	ten_coerce_mr_mr,
	ten_coerce_bi_bi,
	ten_coerce_br_br
};


extern
void ten_cp(ten_t *r, const ten_t t) {
	assert(ten_is_valid(t));
	(*cpTen[ten_get_type(t)])(r, t);
}

static
void (*coerceTen[NUMCOUNT][NUMCOUNT])(ten_t *r, const ten_t t1) = {
	{ten_coerce_mi_mi, ten_coerce_mi_mr, ten_coerce_mi_bi, ten_coerce_mi_br},
	{ten_coerce_mr_mi, ten_coerce_mr_mr, ten_coerce_mr_bi, ten_coerce_mr_br},
	{ten_coerce_bi_mi, ten_coerce_bi_mr, ten_coerce_bi_bi, ten_coerce_bi_br},
	{ten_coerce_br_mi, ten_coerce_br_mr, ten_coerce_br_bi, ten_coerce_br_br},
};


extern
void ten_coerce(ten_t *r, const ten_t t, const ntype toType) {
	ntype fromType;

	assert(ten_is_valid(t));
	assert(tentype_is_valid(toType));

	fromType = ten_get_type(t); 

	if (fromType == toType) {
		*r = t;
		return;
	}

	(*coerceTen[toType][fromType])(r, t);
}

static
void coerce_or_copy(ten_t *r, const ten_t t, ntype type) {
	assert(ten_is_valid(t));
	assert(tentype_is_valid(type));
	if (ten_get_type(t) == type) {
		ten_cp(r, t);
		return;
	} else {
		ten_coerce(r, t, type);
		return;
	}
}

#define TEN_ARG1_FP_DISPATCH(fun)\
static \
void (* fun##Ten[NUMCOUNT])(ten_t *r, const ten_t t1) = {\
	ten_##fun##_mi_mi,\
	ten_##fun##_mr_mr,\
	ten_##fun##_bi_bi,\
	ten_##fun##_br_br\
};\
\
extern \
void ten_##fun(ten_t *r, const ten_t t) {\
	assert(ten_is_valid(t));\
	(* fun##Ten[ten_get_type(t)])(r, t);\
}\

/*abs*/
TENSOR_LOOP(abs, mi, mi)
TENSOR_LOOP(abs, mr, mr)
TENSOR_LOOP(abs, bi, bi)
TENSOR_LOOP(abs, br, br)
TEN_ARG1_FP_DISPATCH(abs)

/*acos*/
#define ten_acos_mi_mi	ten_noop_arg1
TENSOR_LOOP(acos, mr, mr)
#define ten_acos_bi_bi	ten_noop_arg1
TENSOR_LOOP(acos, br, br)
TEN_ARG1_FP_DISPATCH(acos)

/*acosh*/
#define ten_acosh_mi_mi	ten_noop_arg1
TENSOR_LOOP(acosh, mr, mr)
#define ten_acosh_bi_bi	ten_noop_arg1
TENSOR_LOOP(acosh, br, br)
TEN_ARG1_FP_DISPATCH(acosh)

/*asin*/
#define ten_asin_mi_mi	ten_noop_arg1
TENSOR_LOOP(asin, mr, mr)
#define ten_asin_bi_bi	ten_noop_arg1
TENSOR_LOOP(asin, br, br)
TEN_ARG1_FP_DISPATCH(asin)

/*asinh*/
#define ten_asinh_mi_mi	ten_noop_arg1
TENSOR_LOOP(asinh, mr, mr)
#define ten_asinh_bi_bi	ten_noop_arg1
TENSOR_LOOP(asinh, br, br)
TEN_ARG1_FP_DISPATCH(asinh)

/*atan*/
#define ten_atan_mi_mi	ten_noop_arg1
TENSOR_LOOP(atan, mr, mr)
#define ten_atan_bi_bi	ten_noop_arg1
TENSOR_LOOP(atan, br, br)
TEN_ARG1_FP_DISPATCH(atan)

/*atanih*/
#define ten_atanh_mi_mi	ten_noop_arg1
TENSOR_LOOP(atanh, mr, mr)
#define ten_atanh_bi_bi	ten_noop_arg1
TENSOR_LOOP(atanh, br, br)
TEN_ARG1_FP_DISPATCH(atanh)

/*ceil*/
TENSOR_LOOP(ceil, mi, mi)
TENSOR_LOOP(ceil, mr, mr)
TENSOR_LOOP(ceil, bi, bi)
TENSOR_LOOP(ceil, br, br)
TEN_ARG1_FP_DISPATCH(ceil)

/*cos*/
#define ten_cos_mi_mi	ten_noop_arg1
TENSOR_LOOP(cos, mr, mr)
#define ten_cos_bi_bi	ten_noop_arg1
TENSOR_LOOP(cos, br, br)
TEN_ARG1_FP_DISPATCH(cos)

/*cosh*/
#define ten_cosh_mi_mi	ten_noop_arg1
TENSOR_LOOP(cosh, mr, mr)
#define ten_cosh_bi_bi	ten_noop_arg1
TENSOR_LOOP(cosh, br, br)
TEN_ARG1_FP_DISPATCH(cosh)

/*erf*/
#define ten_erf_mi_mi	ten_noop_arg1
TENSOR_LOOP(erf, mr, mr)
#define ten_erf_bi_bi	ten_noop_arg1
TENSOR_LOOP(erf, br, br)
TEN_ARG1_FP_DISPATCH(erf)

/*erfc*/
#define ten_erfc_mi_mi	ten_noop_arg1
TENSOR_LOOP(erfc, mr, mr)
#define ten_erfc_bi_bi	ten_noop_arg1
TENSOR_LOOP(erfc, br, br)
TEN_ARG1_FP_DISPATCH(erfc)

/*exp*/
#define ten_exp_mi_mi	ten_noop_arg1
TENSOR_LOOP(exp, mr, mr)
#define ten_exp_bi_bi	ten_noop_arg1
TENSOR_LOOP(exp, br, br)
TEN_ARG1_FP_DISPATCH(exp)

/*floor*/
TENSOR_LOOP(floor, mi, mi)
TENSOR_LOOP(floor, mr, mr)
TENSOR_LOOP(floor, bi, bi)
TENSOR_LOOP(floor, br, br)
TEN_ARG1_FP_DISPATCH(floor)

/*gamma*/
#define ten_gamma_mi_mi	ten_noop_arg1
/*not very accurate*/
/*TENSOR_LOOP(gamma, mr, mr)*/
#define ten_gamma_mr_mr	ten_noop_arg1
#define ten_gamma_bi_bi	ten_noop_arg1
TENSOR_LOOP(gamma, br, br)
TEN_ARG1_FP_DISPATCH(gamma)

/*log*/
#define ten_log_mi_mi	ten_noop_arg1
TENSOR_LOOP(log, mr, mr)
#define ten_log_bi_bi	ten_noop_arg1
TENSOR_LOOP(log, br, br)
TEN_ARG1_FP_DISPATCH(log)

/*log2*/
#define ten_log2_mi_mi	ten_noop_arg1
TENSOR_LOOP(log2, mr, mr)
#define ten_log2_bi_bi	ten_noop_arg1
TENSOR_LOOP(log2, br, br)
TEN_ARG1_FP_DISPATCH(log2)

/*log10*/
#define ten_log10_mi_mi	ten_noop_arg1
TENSOR_LOOP(log10, mr, mr)
#define ten_log10_bi_bi	ten_noop_arg1
TENSOR_LOOP(log10, br, br)
TEN_ARG1_FP_DISPATCH(log10)

/*round*/
TENSOR_LOOP(round, mi, mi)
TENSOR_LOOP(round, mr, mr)
TENSOR_LOOP(round, bi, bi)
TENSOR_LOOP(round, br, br)
TEN_ARG1_FP_DISPATCH(round)

/*sign*/
TENSOR_LOOP(sign, mi, mi)
TENSOR_LOOP(sign, mr, mr)
TENSOR_LOOP(sign, bi, bi)
TENSOR_LOOP(sign, br, br)
TEN_ARG1_FP_DISPATCH(sign)

/*sin*/
#define ten_sin_mi_mi	ten_noop_arg1
TENSOR_LOOP(sin, mr, mr)
#define ten_sin_bi_bi	ten_noop_arg1
TENSOR_LOOP(sin, br, br)
TEN_ARG1_FP_DISPATCH(sin)

/*sinh*/
#define ten_sinh_mi_mi	ten_noop_arg1
TENSOR_LOOP(sinh, mr, mr)
#define ten_sinh_bi_bi	ten_noop_arg1
TENSOR_LOOP(sinh, br, br)
TEN_ARG1_FP_DISPATCH(sinh)

/*tan*/
#define ten_tan_mi_mi	ten_noop_arg1
TENSOR_LOOP(tan, mr, mr)
#define ten_tan_bi_bi	ten_noop_arg1
TENSOR_LOOP(tan, br, br)
TEN_ARG1_FP_DISPATCH(tan)

/*tanh*/
#define ten_tanh_mi_mi	ten_noop_arg1
TENSOR_LOOP(tanh, mr, mr)
#define ten_tanh_bi_bi	ten_noop_arg1
TENSOR_LOOP(tanh, br, br)
TEN_ARG1_FP_DISPATCH(tanh)

/*trunc*/
TENSOR_LOOP(trunc, mi, mi)
TENSOR_LOOP(trunc, mr, mr)
TENSOR_LOOP(trunc, bi, bi)
TENSOR_LOOP(trunc, br, br)
TEN_ARG1_FP_DISPATCH(trunc)


#define TENSOR_TENSOR_LOOP(fun, r, tt1, tt2)\
static \
void ten_##fun##_##tt1##_##tt2(ten_t *t, ten_t t1, ten_t t2) {\
	size_t i, j, l1, l2, r1, r2, step;\
	size_t *d1, *d2;\
	r##_t *v;\
	ten_t res;\
\
	*t = ten_not_valid;\
\
	assert(ten_is_valid(t1));\
	assert(ten_is_valid(t2));\
\
	r1 = ten_get_rank(t1);\
	r2 = ten_get_rank(t2);\
\
	d1 = ten_get_dimensions(t1);\
	d2 = ten_get_dimensions(t2);\
\
	for (i = 0; i < size_t_min(r1, r2); i++) {\
		if (d1[i] != d2[i]) {\
			/* TODO: message */\
			ten_set_type(*t, NOTYPE);\
			return;\
		}\
	}\
\
	if (r1 < r2) {\
		l1 = ten_get_number_of_values(t1);\
		l2 = ten_get_number_of_values(t2);\
		coerce_or_copy(&res, t2, numtype_##r);\
	} else {\
		l1 = ten_get_number_of_values(t2);\
		l2 = ten_get_number_of_values(t1);\
		coerce_or_copy(&res, t1, numtype_##r);\
	}\
\
	step = l2/l1;\
	assert(step * l1 == l2);\
\
	v = (r##_t *) ten_get_values(res);\
\
	if (r1 < r2) {\
		tt1##_t *v1 = ten_get_values(t1);\
		tt2##_t *v2 = ten_get_values(t2);\
		for (j = 0; j < l1; j++)\
			for (i = 0; i < step; i++)\
				fun##_##tt1##_##tt2(v[i + j*step], v1[j], v2[i + j*step]);\
\
	} else {\
		tt1##_t *v1 = ten_get_values(t1);\
		tt2##_t *v2 = ten_get_values(t2);\
		for (j = 0; j < l1; j++)\
			for (i = 0; i < step; i++)\
				fun##_##tt1##_##tt2(v[i + j*step], v1[i + j*step], v2[j]);\
	}\
\
	*t = res;\
}\

TENSOR_TENSOR_LOOP(add, mi, mi, mi)
TENSOR_TENSOR_LOOP(add, mr, mi, mr)
TENSOR_TENSOR_LOOP(add, bi, mi, bi)
TENSOR_TENSOR_LOOP(add, br, mi, br)

TENSOR_TENSOR_LOOP(add, mr, mr, mi)
TENSOR_TENSOR_LOOP(add, mr, mr, mr)
TENSOR_TENSOR_LOOP(add, mr, mr, bi)
TENSOR_TENSOR_LOOP(add, mr, mr, br)

TENSOR_TENSOR_LOOP(add, bi, bi, mi)
TENSOR_TENSOR_LOOP(add, mr, bi, mr)
TENSOR_TENSOR_LOOP(add, bi, bi, bi)
TENSOR_TENSOR_LOOP(add, br, bi, br)

TENSOR_TENSOR_LOOP(add, br, br, mi)
TENSOR_TENSOR_LOOP(add, mr, br, mr)
TENSOR_TENSOR_LOOP(add, br, br, bi)
TENSOR_TENSOR_LOOP(add, br, br, br)


static
void (*addTT[NUMCOUNT][NUMCOUNT])(ten_t *r, const ten_t t1, const ten_t t2) = {
	{ten_add_mi_mi, ten_add_mi_mr, ten_add_mi_bi, ten_add_mi_br},
	{ten_add_mr_mi, ten_add_mr_mr, ten_add_mr_bi, ten_add_mr_br},
	{ten_add_bi_mi, ten_add_bi_mr, ten_add_bi_bi, ten_add_bi_br},
	{ten_add_br_mi, ten_add_br_mr, ten_add_br_bi, ten_add_br_br}
};

void ten_add(ten_t *r, ten_t t1, ten_t t2) {
	(*addTT[ten_get_type(t1)][ten_get_type(t2)])(r, t1, t2);
}


TENSOR_TENSOR_LOOP(rem, mi, mi, mi)
TENSOR_TENSOR_LOOP(rem, mr, mi, mr)
TENSOR_TENSOR_LOOP(rem, bi, mi, bi)
TENSOR_TENSOR_LOOP(rem, br, mi, br)

TENSOR_TENSOR_LOOP(rem, mr, mr, mi)
TENSOR_TENSOR_LOOP(rem, mr, mr, mr)
TENSOR_TENSOR_LOOP(rem, mr, mr, bi)
TENSOR_TENSOR_LOOP(rem, mr, mr, br)

TENSOR_TENSOR_LOOP(rem, bi, bi, mi)
TENSOR_TENSOR_LOOP(rem, mr, bi, mr)
TENSOR_TENSOR_LOOP(rem, bi, bi, bi)
TENSOR_TENSOR_LOOP(rem, br, bi, br)

TENSOR_TENSOR_LOOP(rem, br, br, mi)
TENSOR_TENSOR_LOOP(rem, mr, br, mr)
TENSOR_TENSOR_LOOP(rem, br, br, bi)
TENSOR_TENSOR_LOOP(rem, br, br, br)

static
void (*remTT[NUMCOUNT][NUMCOUNT])(ten_t *r, const ten_t t1, const ten_t t2) = {
	{ten_rem_mi_mi, ten_rem_mi_mr, ten_rem_mi_bi, ten_rem_mi_br},
	{ten_rem_mr_mi, ten_rem_mr_mr, ten_rem_mr_bi, ten_rem_mr_br},
	{ten_rem_bi_mi, ten_rem_bi_mr, ten_rem_bi_bi, ten_rem_bi_br},
	{ten_rem_br_mi, ten_rem_br_mr, ten_rem_br_bi, ten_rem_br_br}
};

void ten_rem(ten_t *r, ten_t t1, ten_t t2) {
	(*remTT[ten_get_type(t1)][ten_get_type(t2)])(r, t1, t2);
}


TENSOR_TENSOR_LOOP(mul, mi, mi, mi)
TENSOR_TENSOR_LOOP(mul, mr, mi, mr)
TENSOR_TENSOR_LOOP(mul, bi, mi, bi)
TENSOR_TENSOR_LOOP(mul, br, mi, br)

TENSOR_TENSOR_LOOP(mul, mr, mr, mi)
TENSOR_TENSOR_LOOP(mul, mr, mr, mr)
TENSOR_TENSOR_LOOP(mul, mr, mr, bi)
TENSOR_TENSOR_LOOP(mul, mr, mr, br)

TENSOR_TENSOR_LOOP(mul, bi, bi, mi)
TENSOR_TENSOR_LOOP(mul, mr, bi, mr)
TENSOR_TENSOR_LOOP(mul, bi, bi, bi)
TENSOR_TENSOR_LOOP(mul, br, bi, br)

TENSOR_TENSOR_LOOP(mul, br, br, mi)
TENSOR_TENSOR_LOOP(mul, mr, br, mr)
TENSOR_TENSOR_LOOP(mul, br, br, bi)
TENSOR_TENSOR_LOOP(mul, br, br, br)


static
void (*mulTT[NUMCOUNT][NUMCOUNT])(ten_t *r, const ten_t t1, const ten_t t2) = {
	{ten_mul_mi_mi, ten_mul_mi_mr, ten_mul_mi_bi, ten_mul_mi_br},
	{ten_mul_mr_mi, ten_mul_mr_mr, ten_mul_mr_bi, ten_mul_mr_br},
	{ten_mul_bi_mi, ten_mul_bi_mr, ten_mul_bi_bi, ten_mul_bi_br},
	{ten_mul_br_mi, ten_mul_br_mr, ten_mul_br_bi, ten_mul_br_br}
};

void ten_mul(ten_t *r, ten_t t1, ten_t t2) {
	(*mulTT[ten_get_type(t1)][ten_get_type(t2)])(r, t1, t2);
}


/* possibly there should be a mechanism to auto coerce to mi if possible */
TENSOR_TENSOR_LOOP(pow, bi, mi, mi)
TENSOR_TENSOR_LOOP(pow, mr, mi, mr)
/* This is insane */
/*TENSOR_TENSOR_LOOP(pow, bi, mi, bi)*/
TENSOR_TENSOR_LOOP(pow, br, mi, br)
TENSOR_TENSOR_LOOP(pow, mr, mr, mi)
TENSOR_TENSOR_LOOP(pow, mr, mr, mr)
/*TENSOR_TENSOR_LOOP(pow, mr, mr, bi)*/
/*TENSOR_TENSOR_LOOP(pow, mr, mr, br)*/
TENSOR_TENSOR_LOOP(pow, bi, bi, mi)
/*TENSOR_TENSOR_LOOP(pow, mr, bi, mr)*/
/*TENSOR_TENSOR_LOOP(pow, bi, bi, bi)*/
/*TENSOR_TENSOR_LOOP(pow, br, bi, br)*/
TENSOR_TENSOR_LOOP(pow, br, br, mi)
/*TENSOR_TENSOR_LOOP(pow, mr, br, mr)*/
TENSOR_TENSOR_LOOP(pow, br, br, bi)
TENSOR_TENSOR_LOOP(pow, br, br, br)

static
void (*powerTT[NUMCOUNT][NUMCOUNT])(ten_t *r, const ten_t t1, const ten_t t2) = {
	{ten_pow_mi_mi, ten_pow_mi_mr, ten_noop_arg2, ten_pow_mi_br},
	{ten_pow_mr_mi, ten_pow_mr_mr, ten_noop_arg2, ten_noop_arg2},
	{ten_pow_bi_mi, ten_noop_arg2, ten_noop_arg2, ten_noop_arg2},
	{ten_pow_br_mi, ten_noop_arg2, ten_pow_br_bi, ten_pow_br_br}
};

void ten_power(ten_t *r, ten_t t1, ten_t t2) {
	(*powerTT[ten_get_type(t1)][ten_get_type(t2)])(r, t1, t2);
}


TENSOR_TENSOR_LOOP(gcd, mi, mi, mi)
TENSOR_TENSOR_LOOP(gcd, bi, mi, bi)
TENSOR_TENSOR_LOOP(gcd, bi, bi, mi)
TENSOR_TENSOR_LOOP(gcd, bi, bi, bi)

static
void (*gcdTT[NUMCOUNT][NUMCOUNT])(ten_t *r, const ten_t t1, const ten_t t2) = {
	{ten_gcd_mi_mi, ten_noop_arg2, ten_gcd_mi_bi, ten_noop_arg2},
	{ten_noop_arg2, ten_noop_arg2, ten_noop_arg2, ten_noop_arg2},
	{ten_gcd_bi_mi, ten_noop_arg2, ten_gcd_bi_bi, ten_noop_arg2},
	{ten_noop_arg2, ten_noop_arg2, ten_noop_arg2, ten_noop_arg2}
};

void ten_gcd(ten_t *r, ten_t t1, ten_t t2) {
	(*gcdTT[ten_get_type(t1)][ten_get_type(t2)])(r, t1, t2);
}


#define TENSOR_APPLY(fun, r, tt1) \
static \
void ten_apply_##fun##_##tt1(ten_t *t, const ten_t t1) {\
	size_t i, j, l1, lr, rank, rank_t1;\
	size_t *dims;\
	r##_t *v;\
	tt1##_t *v1;\
\
	*t = ten_not_valid;\
\
	assert(ten_is_valid(t1));\
\
	rank_t1 = ten_get_rank(t1);\
	if (rank_t1 <= 0) {\
		ten_coerce_##r##_##r(t, t1);\
		return;\
	}\
\
	rank = rank_t1 - 1;\
\
	dims = (size_t*) ue_Malloc(rank * sizeof(size_t));\
	validateMemory(dims, "localDims: ten_apply_");\
	lr = 1;\
	for (i = 0; i < rank; i++) {\
		dims[i] = ten_get_dimensions(t1)[i + 1];\
		lr *= dims[i];\
	}\
\
	v = (r##_t *) ue_Malloc(lr * sizeof(r##_t));\
	validateMemory(v, " "#fun);\
\
	l1 = ten_get_number_of_values(t1);\
	v1 = (tt1##_t *) ten_get_values(t1);\
\
	for (i = 0; i < lr; i++) {\
		coerce_##r##_##tt1(v[i], v1[i]);\
		for (j = lr + i; j < l1; j += lr) {\
			fun##_##r##_##tt1(v[i], v[i], v1[j]);\
		}\
	}\
\
	*t = ten_mk(numtype_##r, rank, dims, (void *) v);\
}\

#define TEN_APPLY_FP_DISPATCH(name, fun)\
static \
void (* apply_##fun##Ten[NUMCOUNT])(ten_t *r, const ten_t t1) = {\
	ten_apply_##fun##_mi,\
	ten_apply_##fun##_mr,\
	ten_apply_##fun##_bi,\
	ten_apply_##fun##_br\
};\
\
extern \
void ten_apply_##name(ten_t *r, const ten_t t) {\
	assert(ten_is_valid(t));\
	(* apply_##fun##Ten[ten_get_type(t)])(r, t);\
}\


TENSOR_APPLY(add, mi, mi)
TENSOR_APPLY(add, mr, mr)
TENSOR_APPLY(add, bi, bi)
TENSOR_APPLY(add, br, br)
TEN_APPLY_FP_DISPATCH(add, add)

TENSOR_APPLY(mul, mi, mi)
TENSOR_APPLY(mul, mr, mr)
TENSOR_APPLY(mul, bi, bi)
TENSOR_APPLY(mul, br, br)
TEN_APPLY_FP_DISPATCH(mul, mul)

TENSOR_APPLY(pow, bi, mi)
TENSOR_APPLY(pow, mr, mr)
#define ten_apply_pow_bi	ten_noop_arg1
TENSOR_APPLY(pow, br, br)
TEN_APPLY_FP_DISPATCH(power, pow)

TENSOR_APPLY(gcd, mi, mi)
#define ten_apply_gcd_mr	ten_noop_arg1
TENSOR_APPLY(gcd, bi, bi)
#define ten_apply_gcd_br	ten_noop_arg1
TEN_APPLY_FP_DISPATCH(gcd, gcd)


#define TENSOR_CMP(tt1, tt2)\
static \
int ten_cmp_##tt1##_##tt2(ten_t t1, ten_t t2) {\
	int cmp;\
	size_t i, len, rank;\
	size_t *dims1, *dims2;\
	tt1##_t *data1;\
	tt2##_t *data2;\
\
	rank = ten_get_rank(t1);\
	if (rank < ten_get_rank(t2)) return -1;\
	if (rank > ten_get_rank(t2)) return  1;\
\
	dims1 = ten_get_dimensions(t1);\
	dims2 = ten_get_dimensions(t2);\
\
	len = 1;\
	for (i = 0; i < rank; i++) {\
		len *= dims1[i];\
		if (dims1[i] < dims2[i]) return -1;\
		if (dims1[i] > dims2[i]) return  1;\
	}\
\
	data1 = (tt1##_t *) ten_get_values(t1);\
	data2 = (tt2##_t *) ten_get_values(t2);\
\
	for (i = 0; i < len; i++) {\
		cmp = cmp_##tt1##_##tt2(data1[i], data2[i]);\
		if (cmp) return cmp;\
	}\
\
	return 0;\
}\



TENSOR_CMP(mi, mi)
TENSOR_CMP(mi, mr)
TENSOR_CMP(mi, bi)
TENSOR_CMP(mi, br)

TENSOR_CMP(mr, mi)
TENSOR_CMP(mr, mr)
TENSOR_CMP(mr, bi)
TENSOR_CMP(mr, br)

TENSOR_CMP(bi, mi)
TENSOR_CMP(bi, mr)
TENSOR_CMP(bi, bi)
TENSOR_CMP(bi, br)

TENSOR_CMP(br, mi)
TENSOR_CMP(br, mr)
TENSOR_CMP(br, bi)
TENSOR_CMP(br, br)

static
int (*cmp[NUMCOUNT][NUMCOUNT])(const ten_t t1, const ten_t t2) = {
	{ten_cmp_mi_mi, ten_cmp_mi_mr, ten_cmp_mi_bi, ten_cmp_mi_br},
	{ten_cmp_mr_mi, ten_cmp_mr_mr, ten_cmp_mr_bi, ten_cmp_mr_br},
	{ten_cmp_bi_mi, ten_cmp_bi_mr, ten_cmp_bi_bi, ten_cmp_bi_br},
	{ten_cmp_br_mi, ten_cmp_br_mr, ten_cmp_br_bi, ten_cmp_br_br}
};

int ten_cmp(ten_t t1, ten_t t2) {
	return (*cmp[ten_get_type(t1)][ten_get_type(t2)])(t1, t2);
}


void ten_max(ten_t *t, const ten_t t1, const ten_t t2) {
	ten_t tm1, tm2;
	ten_max_arg1( &tm1, t1);
	ten_max_arg1( &tm2, t2);
	if (ten_cmp(tm1, tm2) > 0) {
		*t = tm1;
	} else {
		*t = tm2;
	}
}

void ten_min(ten_t *t, const ten_t t1, const ten_t t2) {
	ten_t tm1, tm2;
	ten_min_arg1( &tm1, t1);
	ten_min_arg1( &tm2, t2);
	if (ten_cmp(tm1, tm2) < 0) {
		*t = tm1;
	} else {
		*t = tm2;
	}

}

extern
bool ten_is_same(ten_t t1, ten_t t2) {
	ntype tt1 = ten_get_type(t1);
	ntype tt2 = ten_get_type(t2);
	assert(ten_is_valid(t1) && ten_is_valid(t2));
	if (tt1 != tt2) return false;
	return (*cmp[tt1][tt2])(t1, t2) == 0;
}

#define TENSOR_TRANSPOSE(r) \
static \
void ten_transpose_##r##_##r(ten_t *t, const ten_t t1, const size_t length, const size_t *perm) {\
	size_t i, j, k, l1, rank;\
	size_t *dims, *tdims;\
	r##_t *v;\
	r##_t *v1;\
\
	*t = ten_not_valid;\
\
	assert(ten_is_valid(t1));\
\
	rank = ten_get_rank(t1);\
	if (rank == 0) {\
		ten_cp(t, t1);\
		return;\
	}\
\
	dims = ten_get_dimensions(t1);\
\
	if(length > rank)\
		return;\
\
	/* not implemented yet */\
	if(rank > 2)\
		return;\
\
	tdims = (size_t *) ue_Malloc(rank * sizeof(size_t));\
	validateMemory(tdims, "localDims: ten_Transpose_"#r);\
\
	for (i = 0; i < length; i++) {\
		if (perm[i] <= rank) {\
			tdims[i] = dims[perm[i] - 1];\
		} else {\
			return;\
		}\
	}\
	for (i = length; i < rank; i++) {\
		tdims[i] = dims[i];\
	}\
\
	l1 = ten_get_number_of_values(t1);\
	v1 = (r##_t *) ten_get_values(t1);\
\
	v = (r##_t *) ue_Malloc(l1 * sizeof(r##_t));\
	validateMemory(v, "ten_Transpose_"#r);\
\
	k = 0;\
	/*for(s = 0; s < rank; s++) {\*/\
		for(i = 0; i < tdims[0]; i++) {\
			for(j = 0; j < tdims[1]; j++) {\
				coerce_##r##_##r(v[k++], v1[j * tdims[0] + i]);\
			}\
		}\
	/*}\*/\
\
	*t = ten_mk(numtype_##r, rank, tdims, (void *) v);\
}\

TENSOR_TRANSPOSE(mi)
TENSOR_TRANSPOSE(mr)
TENSOR_TRANSPOSE(bi)
TENSOR_TRANSPOSE(br)
static
void (* transposeTen[NUMCOUNT])\
	(ten_t *r, const ten_t t1, const size_t length, const size_t *order) = {
	ten_transpose_mi_mi,
	ten_transpose_mr_mr,
	ten_transpose_bi_bi,
	ten_transpose_br_br
};

extern
void ten_transpose(ten_t *r, const ten_t t, const size_t length, const size_t *order) {
	assert(ten_is_valid(t));
	(* transposeTen[ten_get_type(t)])(r, t, length, order);
}

/*
extern
void ten_Transpose(ten_t *t, const ten_t ten, const size_t length, const size_t *order) {
	size_t i, swap, rank;
	size_t *dims;

	*t = ten_not_valid;

	rank = ten_get_rank(ten);
	dims = ten_get_dimensions(ten);

	if(length > rank)
		return;

	for (i = 0; i < length - 1; i++) {
		swap = dims[i];
		if (order[i] - 1 < rank) {
			dims[i] = dims[order[i] - 1];
			dims[order[i] - 1] = swap;
		}
	}

	return;
}
*/
size_t ten_size(const ten_t t) {
	size_t res = 0; 

	res += sizeof(ten_get_type(t));
	res += ue_ExpressionSize((const void *) ten_get_dimensions(t));
	res += sizeof(ten_get_rank(t));
	res += sizeof(ten_get_number_of_values(t));

	switch (ten_get_type(t)) {
		case MI:
		case MR:
		case BI:
		case BR:
			res += ue_ExpressionSize((const void *) ten_get_values(t));
			return res;

		default:
			noCase("ten_size");
			return 0;
	}
	return res;
}

/* https://en.wikipedia.org/wiki/Row-major_order */
#define TEN_PART_SINGLE_CORE(r)\
static inline \
void ten_part_single_core_##r(num_t *n, const ten_t it, mi_t *index, size_t *dimprod) { \
	size_t i;\
	size_t pos = 0;\
	size_t irank = ten_get_rank(it);\
	size_t *idim = ten_get_dimensions(it);\
	r##_t *idata = ten_get_values(it);\
	mi_t temp;\
\
	*n = num_not_valid;\
\
	for (i = 0; i < irank; i++ ) {\
		temp = index[i] - 1;\
		if (temp < 0) temp += idim[i] + 1;\
		if ((size_t) temp >= idim[i]) return;\
		pos += dimprod[i] * temp;\
	}\
\
	*n = num_mk_##r##_##r(idata[pos]);\
	return;\
}\

TEN_PART_SINGLE_CORE(mi)
TEN_PART_SINGLE_CORE(mr)
TEN_PART_SINGLE_CORE(bi)
TEN_PART_SINGLE_CORE(br)

static
void (*partsingcTen[NUMCOUNT])(num_t *n, const ten_t it, mi_t *index, size_t *dimprod) = {
	ten_part_single_core_mi,
	ten_part_single_core_mr,
	ten_part_single_core_bi,
	ten_part_single_core_br
};

extern \
void ten_part_single(num_t *n, const ten_t it, mi_t *index) {
	size_t i;\
	size_t irank = ten_get_rank(it);\
	size_t *idim;\
	size_t dimprod[ irank];\
\
	*n = num_not_valid;\
\
	if (irank == 0) return;\
\
	idim = ten_get_dimensions(it);\
	if (!idim) return;\
\
	dimprod[irank - 1] = 1;\
	for (i = irank - 1; i > 0; i--) {\
		dimprod[i - 1] = dimprod[i] * idim[i];\
	}\
\
	(*partsingcTen[ten_get_type(it)])(n, it, index, dimprod);\

	return;\
}\

#define TEN_SET_PART_SINGLE_CORE(r)\
static inline \
void ten_set_part_single_core_##r(ten_t *it, mi_t *index, size_t *dimprod, num_t v) { \
	size_t i;\
	size_t pos = 0;\
	size_t irank;\
	size_t *idim;\
	r##_t *idata;\
	mi_t temp;\
\
	irank = ten_get_rank(*it);\
	idim = ten_get_dimensions(*it);\
	idata = ten_get_values(*it);\
\
	assert(ten_get_type(*it) == num_get_type(v));\
\
	for (i = 0; i < irank; i++ ) {\
		temp = index[i] - 1;\
		if (temp < 0) temp += idim[i] + 1;\
		if ((size_t) temp >= idim[i]) return;\
		pos += dimprod[i] * temp;\
	}\
\
	coerce_##r##_##r(idata[pos], num_get_##r(v));\
	return;\
}\

TEN_SET_PART_SINGLE_CORE(mi)
TEN_SET_PART_SINGLE_CORE(mr)
TEN_SET_PART_SINGLE_CORE(bi)
TEN_SET_PART_SINGLE_CORE(br)

static
void (*setpartsingcTen[NUMCOUNT])(ten_t *it, mi_t *index, size_t *dimprod, num_t v) = {
	ten_set_part_single_core_mi,
	ten_set_part_single_core_mr,
	ten_set_part_single_core_bi,
	ten_set_part_single_core_br
};

extern \
void ten_set_part_single(ten_t *it, mi_t *index, const num_t v) {
	size_t i;\
	size_t irank = ten_get_rank(*it);\
	size_t *idim;\
	size_t dimprod[ irank];\
\
	assert(ten_get_type(*it) == num_get_type(v));\
\
	if (irank == 0) return;\
\
	idim = ten_get_dimensions(*it);\
	if (!idim) return;\
\
	dimprod[irank - 1] = 1;\
	for (i = irank - 1; i > 0; i--) {\
		dimprod[i - 1] = dimprod[i] * idim[i];\
	}\
\
	(*setpartsingcTen[ten_get_type(*it)])(it, index, dimprod, v);\

	return;\
}\


/* https://en.wikipedia.org/wiki/Row-major_order */
#define TEN_PART_SCATTERED(r)\
static \
void ten_part_scattered_##r(ten_t *rt, const ten_t it, const ten_t st) {\
	size_t i, rlength;\
	size_t srank, irank, rrank;\
	size_t rl;\
	size_t subdim;\
	size_t *sdim, *idim, *rdim;\
	size_t *dimprod;\
	mi_t *indices;\
	r##_t *rdata;\
	num_t n;\
\
	*rt = ten_not_valid;\
\
	if(!ten_is_mi(st)) return;\
\
	srank = ten_get_rank(st);\
	if (srank != 2) return;\
 \
	sdim = ten_get_dimensions(st);\
	if (!sdim) return;\
	rl = sdim[0];\
	subdim = sdim[1];\
\
	irank = ten_get_rank(it);\
	if (irank == 0) return;\
\
	idim = ten_get_dimensions(it);\
	if (!idim) return;\
\
	rrank = irank - subdim + 1;\
	if (rrank < 1) return;\
\
	rdim = (size_t *) ue_Malloc(rrank  * sizeof(size_t));\
	validateMemory(rdim, "ten_part_scattered");\
\
	rlength = rdim[0] = rl;\
	for (i = 1; i < rrank; i++) {\
		rdim[i] = idim[i];\
		rlength *= idim[i];\
	}\
\
	dimprod = (size_t *) ue_Malloc(irank * sizeof(size_t));\
	validateMemory(dimprod, "ten_part_scattered");\
\
	dimprod[irank - 1] = 1;\
	for (i = irank - 1; i > 0; i--) {\
		dimprod[i - 1] = dimprod[i] * idim[i];\
	}\
\
	indices = ten_get_values(st);\
\
	rdata = (r##_t *) ue_Malloc( rlength * sizeof(r##_t));\
	validateMemory(rdata, "ue_part_Tensor");\
	for (i = 0; i < rlength; i++) {\
		ten_part_single_core_##r(&n, it, &(indices[i * subdim]), dimprod);\
		if (num_is_valid(n)) {\
			coerce_##r##_##r(rdata[i], num_get_##r(n));\
		} else {\
			return;\
		}\
	}\
\
	*rt = ten_mk(ten_get_type(it), rrank, rdim, (void *) rdata);\
	return;\
}\

TEN_PART_SCATTERED(mi)
TEN_PART_SCATTERED(mr)
TEN_PART_SCATTERED(bi)
TEN_PART_SCATTERED(br)

static
void (*partscatTen[NUMCOUNT])(ten_t *r, const ten_t it, const ten_t st) = {
	ten_part_scattered_mi,
	ten_part_scattered_mr,
	ten_part_scattered_bi,
	ten_part_scattered_br
};

extern
void ten_part_scattered(ten_t *r, const ten_t it, const ten_t st) {
	assert(ten_is_valid(it));
	assert(ten_is_valid(st));
	(*partscatTen[ten_get_type(it)])(r, it, st);
}

#define TEN_SET_PART_SCATTERED(r)\
static \
void ten_set_part_scattered_##r(ten_t *it, const ten_t st, const ten_t v) {\
	size_t i, rlength;\
	size_t srank, irank, rrank;\
	size_t rl;\
	size_t subdim;\
	size_t *sdim, *idim, *rdim;\
	size_t *dimprod;\
	mi_t *indices;\
	r##_t *vt;\
	num_t n;\
\
	if(!ten_is_mi(st)) return;\
\
	srank = ten_get_rank(st);\
	if (srank != 2) return;\
 \
	sdim = ten_get_dimensions(st);\
	if (!sdim) return;\
	rl = sdim[0];\
	subdim = sdim[1];\
\
	irank = ten_get_rank(*it);\
	if (irank == 0) return;\
\
	idim = ten_get_dimensions(*it);\
	if (!idim) return;\
\
	rrank = irank - subdim + 1;\
	if (rrank < 1) return;\
\
	rdim = (size_t *) ue_Malloc(rrank  * sizeof(size_t));\
	validateMemory(rdim, "ten_set_part_scattered");\
\
	rlength = rdim[0] = rl;\
	for (i = 1; i < rrank; i++) {\
		rdim[i] = idim[i];\
		rlength *= idim[i];\
	}\
\
	dimprod = (size_t *) ue_Malloc(irank * sizeof(size_t));\
	validateMemory(dimprod, "ten_part_scattered");\
\
	dimprod[irank - 1] = 1;\
	for (i = irank - 1; i > 0; i--) {\
		dimprod[i - 1] = dimprod[i] * idim[i];\
	}\
\
	indices = ten_get_values(st);\
\
	vt = (r##_t *) ten_get_values(v);\
\
	for (i = 0; i < rlength; i++) {\
		n = num_mk_##r##_##r(vt[i]);\
		ten_set_part_single_core_##r(it, &(indices[i * subdim]), dimprod, n);\
	}\
\
	return;\
}\

TEN_SET_PART_SCATTERED(mi)
TEN_SET_PART_SCATTERED(mr)
TEN_SET_PART_SCATTERED(bi)
TEN_SET_PART_SCATTERED(br)

static
void (*setpartscatTen[NUMCOUNT])(ten_t *it, const ten_t st, const ten_t v) = {
	ten_set_part_scattered_mi,
	ten_set_part_scattered_mr,
	ten_set_part_scattered_bi,
	ten_set_part_scattered_br
};

extern
void ten_set_part_scattered(ten_t *it, const ten_t st, const ten_t v) {
	assert(ten_is_valid(*it));
	assert(ten_is_valid(st));
	assert(ten_is_valid(v));
	(*setpartscatTen[ten_get_type(*it)])(it, st, v);
}


void
ten_part_structured(ten_t *r, const ten_t in, size_t n, const ten_t *spec) {

	size_t *oDims, *rDims;
	size_t fDims[2];
	size_t i, iRank, oRank, rRank, nele = 1;
	ten_t o, f, temp;

	ten_outerlist(&o, n, spec);
	if (!ten_is_valid(o) || !ten_is_mi(o)) {
		*r = ten_not_valid;
		return;
	}

	oRank = ten_get_rank(o);
	oDims = ten_get_dimensions(o);

	iRank = ten_get_rank(in);

	if (oRank != (iRank + 1)) {
		*r = ten_not_valid;
		return;
	}

	rRank = iRank;
	rDims = (size_t *) ue_Malloc(rRank * sizeof(size_t));
	validateMemory(rDims, "ten_structured_part");

	for (i = 0; i < iRank; i++) {
		rDims[i] = oDims[i];
		nele *= oDims[i];
	}

	fDims[0] = nele;
	fDims[1] = iRank;
	f = ten_mk(MI, 2, fDims, (void *) ten_get_values(o)); 

	ten_part_scattered(&temp, in, f);
	if (!ten_is_valid(temp)) {
		*r = ten_not_valid;
		return;
	}

	*r = ten_mk(ten_get_type(temp), rRank, rDims, ten_get_values(temp));
}

void
ten_set_part_structured(ten_t *it, size_t n, const ten_t *spec, const ten_t v) {

	size_t *oDims;
	size_t fDims[2];
	size_t i, iRank, oRank, nele = 1;
	ten_t o, f;

	ten_outerlist(&o, n, spec);
	if (!ten_is_valid(o) || !ten_is_mi(o)) {
		return;
	}

	oRank = ten_get_rank(o);
	oDims = ten_get_dimensions(o);

	iRank = ten_get_rank(*it);

	if (oRank != (iRank + 1)) {
		return;
	}

	for (i = 0; i < iRank; i++) {
		nele *= oDims[i];
	}

	fDims[0] = nele;
	fDims[1] = iRank;
	f = ten_mk(MI, 2, fDims, (void *) ten_get_values(o)); 

	ten_set_part_scattered(it, f, v);
}


#define TEN_CONSTANTARRAY(r)\
void ten_constantarray_##r(ten_t *r, const ten_t v, size_t n, const mi_t *index) {\
	size_t i, rank, nele = 1, vnele = ten_get_number_of_values(v);\
	size_t *dims;\
	ten_t res;\
	r##_t *data, *values;\
\
	*r = ten_not_valid;\
\
	rank = ten_get_rank(v) + n;\
\
	/* should call tenMalloc */\
	dims = (size_t *) ue_Malloc(rank * sizeof(size_t));\
	validateMemory(dims, "ten_constantarray");\
\
	dims[0] = 0;\
	for(i = 0; i < n; i++) {\
		dims[i] = index[i];\
		nele *= dims[i];\
	}\
	for( ; i < rank; i++) {\
		dims[i] = ten_get_dimensions(v)[n - i];\
		nele *= dims[i];\
	}\
\
	data = (r##_t *) ue_Malloc(nele * sizeof(mi_t));\
	validateMemory(data, "ten_constantarray");\
\
	values = (r##_t *) ten_get_values(v);\
	for (i = 0; i < nele; i++) {\
		coerce_##r##_##r(data[i], values[i % vnele]);\
	}\
\
	ten_set_type_##r(res);\
	ten_set_rank(res, rank);\
	ten_set_dimensions(res, dims);\
	ten_set_number_of_values(res, nele);\
	ten_set_values(res, (void *) data);\
\
	*r = res;\
\
}\

TEN_CONSTANTARRAY(mi)
TEN_CONSTANTARRAY(mr)
TEN_CONSTANTARRAY(bi)
TEN_CONSTANTARRAY(br)

static
void (*constantarrayTen[NUMCOUNT])(ten_t *r, const ten_t v, const size_t n, const mi_t *index) = {
	ten_constantarray_mi,
	ten_constantarray_mr,
	ten_constantarray_bi,
	ten_constantarray_br
};

extern
void ten_constantarray(ten_t *r, const ten_t v, size_t n, const mi_t *index) {
	(*constantarrayTen[ten_get_type(v)])(r, v, n, index);
}



#define TEN_OUTERLIST(r)\
static \
void ten_outerlist_##r(ten_t *rt, const size_t n, const ten_t *tens) {\
	size_t i, j, p, temp, rrank = 1, nele = n;\
	size_t offset = 0;\
	size_t *rdims, *tdims, *index;\
	r##_t *data, *current;\
	ten_t res;\
\
	for (i = 0; i < n; i++) {\
		rrank += ten_get_rank(tens[i]);\
		nele *= ten_get_number_of_values(tens[i]);\
	}\
\
	rdims = (size_t*) ue_Malloc(size_t_max(1, rrank) * sizeof(size_t));\
	validateMemory(rdims, "ten_outerlist");\
\
	for (p = i = 0; i < n; i++) {\
		temp = ten_get_rank(tens[i]);\
		tdims = ten_get_dimensions(tens[i]);\
		for (j = 0; j < temp; j++) {\
			rdims[p++] = tdims[j];\
		}\
	}\
	assert(p == rrank - 1);\
	rdims[p] = n;\
\
	data = (r##_t *) ue_Malloc(nele * sizeof(r##_t));\
	validateMemory(data, "ten_outerlist");\
\
	index = (size_t *) ue_Malloc(n * sizeof(size_t));\
	validateMemory(index, "ten_outerlist");\
	for (i = 0; i < n; i++) index[i] = 0;\
\
	current = (r##_t *) ue_Malloc((n - 1) * sizeof(r##_t));\
	validateMemory(current, "ten_outerlist");\
\
	while (offset < nele) {\
		for (i = 0; i < (n - 1); i++) {\
			coerce_##r##_##r(\
				current[i],\
				((r##_t *) ten_get_values(tens[i]))[index[i]]\
			);\
		}\
\
		for (j = 0; j < ten_get_number_of_values(tens[n-1]); j++) {\
			for (i = 0; i < (n - 1); i++)\
				coerce_##r##_##r(data[i + offset], current[i]);\
\
			coerce_##r##_##r(\
				data[n - 1  + offset],\
				((r##_t *) ten_get_values(tens[n-1]))[j]\
			);\
			offset += n;\
		}\
\
		for (i = n - 1; i > 0; i--) {\
			if (index[i-1] == ten_get_number_of_values(tens[i-1]) - 1) {\
				index[i-1] = 0;\
				index[i] += 1;\
			} else {\
				index[i-1] += 1;\
				break;\
			}\
		}\
\
	}\
\
	ten_set_type_##r(res);\
	ten_set_rank(res, rrank);\
	ten_set_dimensions(res, rdims);\
	ten_set_number_of_values(res, nele);\
	ten_set_values(res, (void *) data);\
\
	*rt = res;\
}\

TEN_OUTERLIST(mi)
TEN_OUTERLIST(mr)
TEN_OUTERLIST(bi)
TEN_OUTERLIST(br)

static
void (*outerlistTen[NUMCOUNT])(ten_t *r, const size_t n, const ten_t *tens) = {
	ten_outerlist_mi,
	ten_outerlist_mr,
	ten_outerlist_bi,
	ten_outerlist_br
};

extern
void ten_outerlist(ten_t *r, size_t n, const ten_t *tens) {
	(*outerlistTen[ten_get_type(tens[0])])(r, n, tens);
}


extern
void ten_range(ten_t *r, num_t start, num_t stop, num_t step) {
	size_t nele, i, rrank = 1, rdim[1];
	ten_t t;
	num_t nstart, nstep;
	ntype nt;

	nele = num_iterateSteps(start, stop, step);

	if (nele ==0) {
		*r = ten_not_valid;
		return;
	}

	rdim[0] = nele;

	nt = num_get_type(step);
	num_common_type(&nt, num_get_type(start));
	num_common_type(&nt, num_get_type(stop));

	nstart = num_coerce(start, nt);
	nstep = num_coerce(step, nt);

	switch (nt) {
		case MI: {
			mi_t *rdata = (mi_t *) ue_Malloc( nele * sizeof(mi_t));
			validateMemory(rdata, "ten_range");

			mi_t s = num_get_mi(nstart);

			for (i = 0; i < nele; i++) {
				//coerce_mi_mi(rdata[i], num_get_mi(nstep));
				//num_add(&ni, ni, step);
				rdata[i] = s;
				s += num_get_mi(nstep);
			}
			t = ten_mk(MI, rrank, rdim, (void *) rdata);\
			break;
		}
		case MR: {
			mr_t *rdata = (mr_t *) ue_Malloc( nele * sizeof(mr_t));
			validateMemory(rdata, "ten_range");
		
			for (i = 0; i < nele; i++) {
				//ni = num_mk_mi_si(i);
				//num_mul(&ni, ni, nstep);
				//num_add(&ni, ni, nstart);
				//coerce_mr_mr(rdata[i], num_get_mr(ni));
				// more accurate then rdata[i] = ni; ni += step;
				rdata[i] = i * num_get_mr(nstep) + num_get_mr(nstart);
			}
			t = ten_mk(MR, rrank, rdim, (void *) rdata);\
			break;
		}
		case BI: {
			bi_t *rdata = (bi_t *) ue_Malloc( nele * sizeof(bi_t));
			validateMemory(rdata, "ten_range");

			num_t ni = nstart;
			for (i = 0; i < nele; i++) {
				coerce_bi_bi(rdata[i], num_get_bi(ni));
				num_add(&ni, ni, nstep);
			}
			t = ten_mk(BI, rrank, rdim, (void *) rdata);\
			break;
		}
		case BR: {
			br_t *rdata = (br_t *) ue_Malloc( nele * sizeof(br_t));
			validateMemory(rdata, "ten_range");
		
			num_t ni = nstart;
			for (i = 0; i < nele; i++) {
				ni = num_mk_mi_si(i);
				num_mul(&ni, ni, nstep);
				num_add(&ni, ni, nstart);
				coerce_br_br(rdata[i], num_get_br(nstep));
			}
			t = ten_mk(BR, rrank, rdim, (void *) rdata);\
			break;
		}

		default:
			noCase("ten_range");
			t = ten_not_valid;
	}

	*r = t;
}

