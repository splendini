#ifndef TENSOR_H
#define TENSOR_H

#include "basenum.h"
#include "num.h"
#include "utils.h"

typedef struct {
	ntype nt;
	size_t *dims;
	size_t rank;
	size_t nVals;
	void *values;
} ten_t;

#define ten_get_type(e)				((e).nt)
#define tentype_is_valid(t)			(numtype_is_valid(t))

#define ten_get_dimensions(e)		((e).dims)
#define ten_get_rank(e)				((e).rank)
#define ten_get_number_of_values(e)	((e).nVals)
#define ten_get_values(e)			((e).values)

#define ten_is_mi(t)				(ten_get_type(t) == MI)
#define ten_is_mr(t)				(ten_get_type(t) == MR)
#define ten_is_bi(t)				(ten_get_type(t) == BI)
#define ten_is_br(t)				(ten_get_type(t) == BR)

#define ten_is_exact(t)				(ten_is_mi(t) || ten_is_bi(t))
#define ten_is_inexact(t)			(ten_is_mr(t) || ten_is_br(t))
#define ten_is_valid(t)				(ten_is_exact(t) || ten_is_inexact(t))

#define ten_set_dimensions(e, di)	(((e).dims) = (di))
#define ten_set_rank(e, de)			(((e).rank) = (de))
#define ten_set_number_of_values(e, nv)	(((e).nVals) = (nv))
#define ten_set_values(e, val)		(((e).values) = (val))

ten_t ten_mk(const ntype tType, const size_t rank, const size_t *dims, void *data); 
ten_t ten_mk_num_t(const num_t n);

extern ten_t ten_not_valid;

void ten_transpose(ten_t *t, const ten_t ten, const size_t length, const size_t *order);
void ten_cp(ten_t *r, const ten_t t); 
void ten_coerce(ten_t *r, const ten_t t, const ntype targetType);

int ten_cmp(ten_t t1, ten_t t2);
bool ten_is_same(ten_t t1, ten_t t2);

void ten_print_raw(FILE *f, ten_t t);

void ten_abs	(ten_t *r, const ten_t t);
void ten_acos	(ten_t *r, const ten_t t);
void ten_acosh	(ten_t *r, const ten_t t);
void ten_asin	(ten_t *r, const ten_t t);
void ten_asinh	(ten_t *r, const ten_t t);
void ten_atan	(ten_t *r, const ten_t t);
void ten_atanh	(ten_t *r, const ten_t t);
void ten_ceil	(ten_t *r, const ten_t t);
void ten_cos	(ten_t *r, const ten_t t);
void ten_cosh	(ten_t *r, const ten_t t);
void ten_erf	(ten_t *r, const ten_t t);
void ten_erfc	(ten_t *r, const ten_t t);
void ten_exp	(ten_t *r, const ten_t t);
void ten_floor	(ten_t *r, const ten_t t);
void ten_gamma	(ten_t *r, const ten_t t);
void ten_log	(ten_t *r, const ten_t t);
void ten_log2	(ten_t *r, const ten_t t);
void ten_log10	(ten_t *r, const ten_t t);
void ten_round	(ten_t *r, const ten_t t);
void ten_sign	(ten_t *r, const ten_t t);
void ten_sin	(ten_t *r, const ten_t t);
void ten_sinh	(ten_t *r, const ten_t t);
void ten_tan	(ten_t *r, const ten_t t);
void ten_tanh	(ten_t *r, const ten_t t);
void ten_trunc	(ten_t *r, const ten_t t);

void ten_max_arg1(ten_t *r, const ten_t t);
void ten_min_arg1(ten_t *r, const ten_t t);

void ten_add	(ten_t *r, const ten_t t1, const ten_t t2);
void ten_mul	(ten_t *r, const ten_t t1, const ten_t t2);
void ten_power	(ten_t *r, const ten_t t1, const ten_t t2);
void ten_gcd	(ten_t *r, const ten_t t1, const ten_t t2);
void ten_max	(ten_t *r, const ten_t t1, const ten_t t2);
void ten_rem	(ten_t *r, const ten_t t1, const ten_t t2);
void ten_min	(ten_t *r, const ten_t t1, const ten_t t2);

void ten_apply_add		(ten_t *r, const ten_t t1);
void ten_apply_mul		(ten_t *r, const ten_t t1);
void ten_apply_power	(ten_t *r, const ten_t t1);
void ten_apply_gcd		(ten_t *r, const ten_t t1);


size_t ten_size(const ten_t t); 

void ten_part_scattered	(ten_t *r, const ten_t it, const ten_t st);
void ten_part_single	(num_t *n, const ten_t it, mi_t *index);
void ten_part_structured(ten_t *r, const ten_t it, size_t n, const ten_t *mi);

void ten_set_part_scattered	(ten_t *it, const ten_t st, const ten_t v);
void ten_set_part_single	(ten_t *it, mi_t *index, const num_t v);
void ten_set_part_structured(ten_t *it, size_t n, const ten_t *mi, const ten_t v);

void ten_constantarray	(ten_t *r, const ten_t v, size_t n, const mi_t *index);
void ten_outerlist		(ten_t *r, size_t n, const ten_t *tens);
void ten_range			(ten_t *r, num_t start, num_t stop, num_t step);

#endif

