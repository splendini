#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#include <signal.h>
#include <setjmp.h>

#include "uex.h"
#include "uex_language.h"

/* used in scanner and parser */
/*@-exportlocal@*/
int lineNumber = 1;
bool isEOF = false;
jmp_buf abort_jmp;
/*@=exportlocal@*/

void abort_handler(int dummy) {
	longjmp(abort_jmp, 1);
}


int main(int argc, char *argv[]) {

	ue_p u = sNull, env = sNull;
	FILE *fOut = stdout, *fb = NULL;
	FILE * volatile fIn = stdin;
	volatile bool isInteractive = true;

	if ( argc > 1 ) {
		fIn = fopen( argv[1], "r" );
		if (fIn == NULL) {
			fprintf(stderr, "\n");
			fprintf(stderr, "File: %s can not be opened.\n", argv[1]);
			fprintf(stderr, "\n");
			exit(EXIT_FAILURE);
		}
		isInteractive = false;
	}

start:

	ue_init_lib();
	ue_init_language();

	env = ue_global_env;

#ifdef UE_DEBUG  
	if (isInteractive) {
		fprintf(fOut, "\n\tDebug Build\n");
	}
#endif

	fb = fopen( "bootstrap.spl", "r");
	if (fb != NULL) {
		/* always eat new line */
		u = ue_Parse(fb, true);
		u = ue_Eval(u, env);
	}

	signal(SIGINT, abort_handler);

	while (true) {

		if (isInteractive) {
			fprintf(fOut, "\n%i : ", lineNumber);
		}

		if (setjmp(abort_jmp)) {
			signal(SIGINT, abort_handler);
			u = sFailed;
		} else {
			u = ue_Parse(fIn, !isInteractive);
		}

		u = ue_Eval(u, env);

		if (u == sDone || u == sQuit) break;
		if (isEOF && !isInteractive) break;
		if (u == sNull) {
			lineNumber++;
			continue;
		}
		if (u == sRestart) {
			lineNumber = 1;
			goto start;
		}

		fprintf(fOut, "%i=> ", lineNumber);
		ue_Print(fOut, u);

		lineNumber++;
	}

	if (fIn != stdin) {
		(void) fclose(fIn);
	}

	exit(EXIT_SUCCESS);
}

