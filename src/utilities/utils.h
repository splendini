#ifndef UTILS_N
#define UTILS_N

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <errno.h>

#ifndef NDEBUG
	#define GC_DEBUG
	#define UE_DEBUG
#endif

#include <gc.h>
#include <assert.h>


void	*ue_Malloc			(size_t s);
void	*ue_Realloc			(void *p, size_t s);
void	ue_Free				(void *p); 
size_t	ue_HeapSize			(void); 
size_t	ue_ExpressionSize	(const void * p); 

char	 *ue_strdup	(char const *s); 

#define ue_init_memory_function()	GC_INIT()

#define validateMemory(data, s)		do {\
										if(!(data)) {\
											fprintf(stderr, "\nNo more mem\n");\
											fprintf(stderr, "%s", s);\
											fprintf(stderr, "\n");\
        									fprintf(stderr, "-> line %d, file %s\n", __LINE__, __FILE__);\
											exit(EXIT_FAILURE); }\
									} while (0)



#define noCase(str)		do {\
							printf("\nNo case:\n\t");\
							printf("%s", str);\
							printf("\n\tAt file +line: %s +%d" , __FILE__, __LINE__);\
							printf("\n\n");\
						 } while (0)



#endif
