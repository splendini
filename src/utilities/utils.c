
#include "utils.h"
#include <gc.h>

void *ue_Malloc(size_t s) {
	return (GC_MALLOC(s));
}

void *ue_Realloc(void *p, size_t s) {
	return (GC_REALLOC(p, s));
}

void ue_Free(void *p) {
	GC_FREE(p);
}

size_t ue_HeapSize(void) {
	GC_gcollect();
	return GC_get_heap_size();
}

size_t ue_ExpressionSize(const void * p) {
	return GC_size(p);
}

char *ue_strdup(char const *s) {
	return (GC_STRDUP(s));
}
